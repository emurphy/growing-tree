package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This interface declares the functions for traversing and manipulating trees
 * that are accessible to all operators.
 * 
 * @author Josh Israel
 *
 * @param <P> The <code>NodeProperties</code> class (or subclass) that these
 * nodes are parameterized by.
 * @see NodeProperties
 */

public interface IAlgorithmNode<P extends NodeProperties> extends INode<P> {
	public IAlgorithmNode<P> getParent();
	public IAlgorithmNode<P> getRoot();
	public IAlgorithmNode<P> getLeft();
	public IAlgorithmNode<P> getRight();
	public IAlgorithmNode<P> getSuccessor();
	public IAlgorithmNode<P> getPredecessor();
	
	/**
	 * Freezes the animation briefly and displays the current state of the tree.
	 * This allows states between events like rotations to be displayed. 
	 */
	public void freeze(double delay);
	public void freeze();
	
	/**
	* Rotates the left child up. 
	* @return The new parent of the node (the former left child).
	*/
	public IAlgorithmNode<P> rotateLeft();
	/**
	* Rotates the right child up. 
	* @return The new parent of the node (the former right child).
	*/
	public IAlgorithmNode<P> rotateRight();
}
