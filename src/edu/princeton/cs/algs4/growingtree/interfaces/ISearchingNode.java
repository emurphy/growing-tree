package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This interface is for use by an <code>ISearchOperator</code> to traverse the tree
 * to find the node being sought. Some functions from <code>IAlgorithmNode</code> are 
 * redeclared here with return type <code>ISearchingNode</code> so as to avoid the 
 * need to cast in client code.
 * 
 * @author Josh Israel
 *
 * @param <P>
 * @see ISearchOperator
 */

public interface ISearchingNode<P extends NodeProperties> extends IAlgorithmNode<P> {
	public ISearchingNode<P> getLeft();
	public ISearchingNode<P> getRight();
	public ISearchingNode<P> getParent();
	/**
	 * This must be called on the node being sought in order to trigger the search
	 * animation. 
	 */
	public void markFound();
}
