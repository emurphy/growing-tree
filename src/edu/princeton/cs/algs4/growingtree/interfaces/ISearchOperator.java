package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This must be implemented by classes in order to define a search operator.
 * @author Josh Israel
 *
 * @param <P>
 */

public interface ISearchOperator<P extends NodeProperties> {
	/**
	 * 
	 * @param root The root of the tree
	 * @param keyCompare Comparable representing the key being sought
	 * @return The node being sought
	 */
	public INode<P> doSearch(ISearchingNode<P> root, Comparable<INode<P>> keyCompare);
}
