package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This interface is used by <code>IDeleteOperator</code> to delete a node.
 * 
 * @author Josh Israel
 *
 * @param <P>
 * @see IDeleteOperator
 */


public interface IDeletingNode<P extends NodeProperties> extends IAlgorithmNode<P> {
	/**
	 * This performs Hibbard deletion. There are 3 cases:
	 * If the node is a leaf, it is simply removed.
	 * If the node has 1 child, it is replaced by its child
	 * If the node has 2 children, it is swapped with its successor and then deleted
	 * as a node with 1 child or a leaf.
	 * @return If a leaf, null. If it has 1 child, returns its child. If it has 2 
	 * children, the child of the successor.
	 */
	public IAlgorithmNode<P> successorHibbardDelete();
	
	/**
	 * Identical to successorHibbardDelete, except swaps with predecessor.
	 * @return If a leaf, null. If it has 1 child, returns its child. If it has 2 
	 * children, the child of the predecessor.
	 */
	public IAlgorithmNode<P> predecessorHibbardDelete();
}
