package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This must be implemented by classes in order to define a insert operator.
 * @author Josh Israel
 *
 * @param <P>
 */

public interface IInsertOperator<P extends NodeProperties> {
	/**
	 * 
	 * @param root Root of the tree
	 * @param newNode Node to be inserted into the tree. Null when
	 *  inserting into an empty tree.
	 */
	public void doInsert(IInsertingNode<P> root, INode<P> newNode);
}
