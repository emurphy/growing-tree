package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.experiments.IExperimentLogger;
import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This is the base interface for all the interfaces that client code is exposed to
 * for manipulating the tree. Currently, the only class that implements this interface
 * is <code>ShadowNode</code>, but casting to <code>ShadowNode</code> should never be
 * necessary and is strongly discouraged.
 * 
 * @author Josh Israel
 *
 * @param <P> The <code>NodeProperties</code> class (or subclass) that these
 * nodes are parameterized by.
 * @see NodeProperties
 * @see edu.princeton.cs.algs4.growingtree.framework.ShadowNode
 */

public interface INode<P extends NodeProperties> extends Comparable<INode<P>> {
	public P getNodeProperties();
	public IExperimentLogger<P> getLogger();
}
