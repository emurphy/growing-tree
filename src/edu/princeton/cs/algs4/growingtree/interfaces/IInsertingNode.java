package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This interface is for use by an <code>IInsertOperator</code> to traverse and manipulate
 * the tree. Some functions from <code>IAlgorithmNode</code> are redeclared here with
 * return type <code>IInsertingNode</code> so as to avoid the need to cast in client code.
 * 
 * @author Josh Israel
 *
 * @param <P>
 */

public interface IInsertingNode<P extends NodeProperties> extends IAlgorithmNode<P> {
	/**
	 * This should only be called once per call to <code>IInsertOperator.doInsert</code>
	 * @param newNode Node to be inserted as the left child of this one. It should
	 * not be any node other than the newNode argument to <code>IInsertOperator.doInsert</code>
	 * @return The node just inserted into the tree
	 */
	public IAlgorithmNode<P> insertLeft(INode<P> newNode);
	/**
	 * This should only be called once per call to <code>IInsertOperator.doInsert</code>
	 * @param newNode Node to be inserted as the left child of this one. It should
	 * not be any node other than the newNode argument to <code>IInsertOperator.doInsert</code>
	 * @return The node just inserted into the tree
	 */
	public IAlgorithmNode<P> insertRight(INode<P> newNode);
	public IInsertingNode<P> rotateLeft();
	public IInsertingNode<P> rotateRight();
	public IInsertingNode<P> getLeft();
	public IInsertingNode<P> getRight();
	public IInsertingNode<P> getParent();
}
