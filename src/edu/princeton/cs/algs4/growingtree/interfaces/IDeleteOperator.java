package edu.princeton.cs.algs4.growingtree.interfaces;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

/**
 * This must be implemented by classes in order to define a delete operator.
 * @author Josh Israel
 *
 * @param <P>
 */

public interface IDeleteOperator<P extends NodeProperties> {
	/**
	 * 
	 * @param root The root of the tree
	 * @param node Node to be deleted
	 */
	public void doDelete(IAlgorithmNode<P> root, IDeletingNode<P> node);
}
