package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Standard BST Insertion
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertingNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;

public class BSTInsertion <P extends NodeProperties> implements IInsertOperator<P> {
	
	protected IAlgorithmNode<P> insert(IInsertingNode<P> root, INode<P> newNode) {
		if (newNode == null) { assert(false); }
		IInsertingNode<P> pathNode = root;
		while (true) {
	        int cmp = newNode.compareTo(pathNode);
	        if (cmp < 0) {
	        	if (pathNode.getLeft() == null) {
	        		return pathNode.insertLeft(newNode);
	        	}
	        	else {
	        		pathNode = pathNode.getLeft();
	        	} 
	        }
	        else if (cmp > 0) {
	        	if (pathNode.getRight() == null) {
	        		return pathNode.insertRight(newNode);
	        	}
	        	else {
	        		pathNode = pathNode.getRight();
	        	}
	        }
	        else {
	        	assert(false);
	        	return null;
	        }
		}
	}
	
	public void doInsert(IInsertingNode<P> root, INode<P> newNode) {
		if (newNode == null) return;
		insert(root, newNode);
	}
}
