package edu.princeton.cs.algs4.growingtree.demos;

/**
 * AVL Tree Deletion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeletingNode;

public class AVLDeletion<P extends AVLNodeProperties> extends AVLOperator<P> implements IDeleteOperator<P> {

	public AVLDeletion() {
		super();
	}
	
	public AVLDeletion(int balFactor) {
		super(balFactor);
	}
	
	private IAlgorithmNode<P> deleteMin (IAlgorithmNode<P> t, IDeletingNode<P> x) {
        assert (t != null);
        if (t.getLeft()==null) {
            return x.successorHibbardDelete();
        }
        deleteMin(t.getLeft(), x);
        assert(((ShadowNode<P>)t).isBST());
        return bal(t);
    }
	
	private IAlgorithmNode<P> delete(IAlgorithmNode<P> t, IDeletingNode<P> x) {
        int c = x.compareTo(t);
        if (c==0) {
            if (t.getLeft()==null || t.getRight()==null) return x.successorHibbardDelete();
            else {
            	IAlgorithmNode<P> newT = x.getSuccessor();
            	deleteMin(t.getRight(), x);
            	t = newT;
            }
        } 
        else if (c<0) {
            delete(t.getLeft(), x);
        } 
        else {
            delete(t.getRight(), x);
        }
        return bal(t);
	}
	
	@Override
	public void doDelete(IAlgorithmNode<P> root, IDeletingNode<P> node) {
		delete(root, node);
	}

}
