package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Red Black Tree Deletion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeletingNode;

public class RedBlackDeletion<P extends RBNodeProperties> extends RedBlackOperator<P> implements IDeleteOperator<P> {

    private void fixAfterDeletion(IAlgorithmNode<P> x) {
        while (x.getParent() != null && colorOf(x) == RBNodeProperties.BLACK) {
            if (x == leftOf(parentOf(x))) {
                IAlgorithmNode<P> sib = rightOf(parentOf(x));
                
                if (colorOf(sib) == RBNodeProperties.RED) {
                	x.freeze();
                    setColor(sib, RBNodeProperties.BLACK);
                    setColor(parentOf(x), RBNodeProperties.RED);
                    x.freeze();
                    parentOf(x).rotateLeft();
                    sib = rightOf(parentOf(x));
                }
                
                if (colorOf(leftOf(sib))  == RBNodeProperties.BLACK &&
                    colorOf(rightOf(sib)) == RBNodeProperties.BLACK) {
                    setColor(sib,  RBNodeProperties.RED);
                    x = parentOf(x);
                } else {
                    if (colorOf(rightOf(sib)) == RBNodeProperties.BLACK) {
                    	x.freeze();
                        setColor(leftOf(sib), RBNodeProperties.BLACK);
                        setColor(sib, RBNodeProperties.RED);
                        sib.rotateRight();
                        sib = rightOf(parentOf(x));
                    }
                    x.freeze();
                    setColor(sib, colorOf(parentOf(x)));
                    setColor(parentOf(x), RBNodeProperties.BLACK);
                    setColor(rightOf(sib), RBNodeProperties.BLACK);
                    x.freeze();
                    parentOf(x).rotateLeft();
                    x = x.getRoot();
                }
            } else { // symmetric
                IAlgorithmNode<P> sib = leftOf(parentOf(x));
                
                if (colorOf(sib) == RBNodeProperties.RED) {
                    x.freeze();
                    setColor(sib, RBNodeProperties.BLACK);
                    setColor(parentOf(x), RBNodeProperties.RED);
                    parentOf(x).rotateRight();
                    sib = leftOf(parentOf(x));
                }
                
                if (colorOf(rightOf(sib)) == RBNodeProperties.BLACK &&
                    colorOf(leftOf(sib)) == RBNodeProperties.BLACK) {
                    setColor(sib,  RBNodeProperties.RED);
                    x = parentOf(x);
                } else {
                    if (colorOf(leftOf(sib)) == RBNodeProperties.BLACK) {
                        setColor(rightOf(sib), RBNodeProperties.BLACK);
                        setColor(sib, RBNodeProperties.RED);
                        sib.rotateLeft();
                        sib = leftOf(parentOf(x));
                    }
                    x.freeze();
                    setColor(sib, colorOf(parentOf(x)));
                    setColor(parentOf(x), RBNodeProperties.BLACK);
                    setColor(leftOf(sib), RBNodeProperties.BLACK);
                    x.freeze();
                    parentOf(x).rotateRight();
                    x = x.getRoot();
                }
            }
        }
        
        setColor(x, RBNodeProperties.BLACK);
    }
	
	
	@Override
	public void doDelete(IAlgorithmNode<P> root, IDeletingNode<P> node) {
		assert(isRedBlack(root));
		IAlgorithmNode<P> p = node;
        boolean swapping = false;
        
        if (p.getLeft() != null && p.getRight() != null){
        	swapping = true;
        	p = p.getSuccessor();
        	
        }
        boolean removingBlack = p.getNodeProperties().getColor() == RBNodeProperties.BLACK;
        IAlgorithmNode<P> replacement = (p.getLeft() != null ? p.getLeft() : p.getRight());

        if (replacement != null) {
        	if (swapping) {
	        	boolean temp = colorOf(p);
	        	p.getNodeProperties().setColor(colorOf(node));
	        	node.getNodeProperties().setColor(temp);
        	}
        	node.successorHibbardDelete();
        	
            // Fix replacement
            if (removingBlack)
                fixAfterDeletion(replacement);
        } else if (p.getParent() == null) { // return if we are the only node.
        	node.successorHibbardDelete();
        } else { //  No children. Use self as phantom replacement and unlink.
            if (removingBlack) {
            	fixAfterDeletion(p);
            }
                
            if (swapping) {
	        	boolean temp = colorOf(p);
	        	p.getNodeProperties().setColor(colorOf(node));
	        	node.getNodeProperties().setColor(temp);
        	}
            node.successorHibbardDelete();
        }
        

	}

}
