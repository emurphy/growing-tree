package edu.princeton.cs.algs4.growingtree.demos;

import edu.princeton.cs.algs4.growingtree.experiments.RotationHeightLogger;
import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.framework.TreeVisualization;

/**
 * This is a demo to instantiate each of the trees implemented in this 
 * package.
 * @author Josh Israel
 *
 */

public class GrowingTreeDemo {

	public static void main(String[] args) {
        // If running on Mac, use Mac-style menu bar	
        if (System.getProperty("os.name").equals("Mac OS X"))
        {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
        }

	    TreeVisualization vis = new TreeVisualization();
	    vis.addTree("BST", new NodeProperties(), 
				  	   new BSTInsertion<NodeProperties>(), 
				  	   new BSTSearch<NodeProperties>(), 
				  	   new BSTDeletion<NodeProperties>(), true);
	    vis.addTree("Red-Black", new RBNodeProperties(), 
 		       		   new RedBlackInsertion<RBNodeProperties>(), 
 		               new BSTSearch<RBNodeProperties>(),
 		               new RedBlackDeletion<RBNodeProperties>(),
 		               new RotationHeightLogger<RBNodeProperties>());
    	vis.addTree("LLRB", new RBNodeProperties(), 
					   new LLRBInsertion<RBNodeProperties>(), 
					   new BSTSearch<RBNodeProperties>(), 
					   new LLRBDeletion<RBNodeProperties>());
    	vis.addTree("Splay", new NodeProperties(), 
				   	   (new SplayOperators()).new SplayInsertion<NodeProperties>(), 
				   	   (new SplayOperators()).new SplaySearch<NodeProperties>(),
				   	   (new SplayOperators()).new SplayDeletion<NodeProperties>());
    	vis.addTree("Randomized", new NodeProperties(), 
    				   new RandomizedBSTInsertion<NodeProperties>(), 
    				   new BSTSearch<NodeProperties>(),
    				   new RandomizedBSTDeletion<NodeProperties>());
    	vis.addTree("AVL-1", new AVLNodeProperties(), 
    				   new AVLInsertion<AVLNodeProperties>(1), 
    				   new BSTSearch<AVLNodeProperties>(), 
    				   new AVLDeletion<AVLNodeProperties>(1));
    	vis.addTree("AVL-2", new AVLNodeProperties(), 
				   new AVLInsertion<AVLNodeProperties>(2), 
				   new BSTSearch<AVLNodeProperties>(), 
				   new AVLDeletion<AVLNodeProperties>(2));
	    vis.addTree("RAVL", new AVLNodeProperties(),
	    			   new AVLInsertion<AVLNodeProperties>(),
	    			   new BSTSearch<AVLNodeProperties>(),
	    			   new BSTDeletion<AVLNodeProperties>());
    	vis.addTree("Rank Balanced", new RankNodeProperties(), 
    				   new RankInsertion<RankNodeProperties>(),
    				   new BSTSearch<RankNodeProperties>(), 
    				   new RankDeletion<RankNodeProperties>());
    	vis.start();
	}

}
