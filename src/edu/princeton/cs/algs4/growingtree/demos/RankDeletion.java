package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Rank Balanced Tree Deletion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeletingNode;

public class RankDeletion<P extends RankNodeProperties> implements IDeleteOperator<P> {

	private boolean is22Node(IAlgorithmNode<P> n) {
		if (n == null) return false;
		if ((RankUtils.rank(n) - RankUtils.rank(n.getLeft()) == 2) &&
			(RankUtils.rank(n) - RankUtils.rank(n.getRight()) == 2)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private void bal(IAlgorithmNode<P> q, IAlgorithmNode<P> p) {
		assert (q == null || p==q.getParent());
		if (p == null) {
			return;
		}
		
		// q is not a 3-child and p is not a 2,2-node of rank 1.
		if (RankUtils.rank(q) + 3 != RankUtils.rank(p) &&
			!(is22Node(p) && p.getNodeProperties().getRank() == 1)) {
			RankUtils.diffUpdate(p);
			//if (p.getParent() != null) RankUtils.diffUpdate(p.getParent());
			return;
		}
		
		IAlgorithmNode<P> s;
		if (q == null) {
			if (p.getLeft() == null) s = p.getRight();
			else 					 s = p.getLeft();
		}
		else {
			s = RankUtils.sibling(q);
		}
		
		if (RankUtils.rank(s) + 2 == RankUtils.rank(p)) {
			RankUtils.demote(p);
			bal(p, p.getParent());
		}
		else if (RankUtils.rank(q) + 3 == RankUtils.rank(p) &&
				 s != null &&
				 RankUtils.rank(s) + 1 == RankUtils.rank(p)) {
			assert(s != null);
			IAlgorithmNode<P> t;
			IAlgorithmNode<P> u;
			if (q == p.getRight()) {
				t = s.getRight();
				u = s.getLeft();
			}
			else {
				t = s.getLeft();
				u = s.getRight();
			}
			// Double Demotion
			if(RankUtils.rank(t) + 2 == RankUtils.rank(s) &&
			   RankUtils.rank(u) + 2 == RankUtils.rank(s)) {
				RankUtils.demote(s); 
				RankUtils.demote(p);
				bal(p, p.getParent());
			}
			// Rotation
			else if (RankUtils.rank(u) + 1 == RankUtils.rank(s)) {
				RankUtils.rotateUp(s);
				RankUtils.demote(p);
				if (t == null) RankUtils.demote(p);
				RankUtils.promote(s);
			}
			// Double Rotation
			else if (RankUtils.rank(t) + 1 == RankUtils.rank(s) &&
					 RankUtils.rank(u) + 2 == RankUtils.rank(s)) {
				RankUtils.rotateUp(t); 
				RankUtils.promote(t); 
				RankUtils.rotateUp(t);
				RankUtils.demote(p);
				RankUtils.demote(s);
				RankUtils.demote(p);
				RankUtils.promote(t);
			}
			else {
				assert(false);
			}
			/*if (t != null) RankUtils.diffUpdate(t);
			if (u != null) RankUtils.diffUpdate(u);
			if (s != null) RankUtils.diffUpdate(s);
			if (p != null) RankUtils.diffUpdate(p);
			if (q != null) RankUtils.diffUpdate(q);*/
		}
		else {
			assert(false);
		}
	}

	
	
	@Override
	public void doDelete(IAlgorithmNode<P> root, IDeletingNode<P> node) {
		//double key = ((ShadowNode<P>)node).getKey();
		assert(RankUtils.hasValidRankDiffs(root));
		IAlgorithmNode<P> q;
		IAlgorithmNode<P> p;
		if (node.getLeft() == null) {
			p = node.getParent();
			q = node.getRight();
		}
		else if (node.getRight() == null) {
			p = node.getParent();
			q = node.getLeft();
		}
		else {
			IAlgorithmNode<P> succ = node.getSuccessor();
			int temp = succ.getNodeProperties().getRank();
			succ.getNodeProperties().setRank(node.getNodeProperties().getRank());
			node.getNodeProperties().setRank(temp);
			q = succ.getRight();
			if (succ == node.getRight()) {
				p = succ;
			}
			else {
				p = succ.getParent();
			}
		}
		node.successorHibbardDelete();
		bal(q, p);
		if (q != null) RankUtils.diffUpdate(q);
		if (p != null) RankUtils.diffUpdate(p);
	}
	
	
	
}
