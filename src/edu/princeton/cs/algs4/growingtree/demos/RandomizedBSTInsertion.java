package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Randomized Binary Search Tree Deletion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertingNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;

public class RandomizedBSTInsertion<P extends NodeProperties> implements IInsertOperator<P> {

	public void putRoot(IInsertingNode<P> x, INode<P> newNode) {
		assert(x != null);
		int cmp = newNode.compareTo(x);
		if (cmp == 0) { 
			return; 
		}
		else if (cmp  < 0) {
			if (x.getLeft() == null) {
				x.insertLeft(newNode);
			}
			else {
				putRoot(x.getLeft(), newNode);

			}
			x.rotateRight();
		}
		else { 
			if (x.getRight() == null) {
				x.insertRight(newNode); 
			}
			else {
				putRoot(x.getRight(), newNode);
			}
			x.rotateLeft();
		}
		return;
	}
	
	public void put(IInsertingNode<P> x, INode<P> newNode) {
		assert(x != null);
        int cmp = newNode.compareTo(x);
        if (cmp == 0) { return; }
        if (StdRandom.bernoulli(1.0 / (x.getNodeProperties().getSize() + 1.0))) {
        	putRoot(x, newNode);
        	return;
        }
        if (cmp < 0) {
        	if (x.getLeft() == null) {
        		x.insertLeft(newNode);
        	}
        	else {
        		put(x.getLeft(), newNode);
        	}
        }
        else {
        	if (x.getRight() == null) {
        		x.insertRight(newNode);
        	}
        	else {
        		put(x.getRight(), newNode); 	
        	}
        }
        return;
	}
	
	@Override
	public void doInsert(IInsertingNode<P> root, INode<P> newNode) {
		if (newNode != null) {
			put(root, newNode);
		}
	}

}
