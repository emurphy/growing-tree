package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Utility functions for Red Black Tree Operators
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.experiments.IExperimentLogger;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;

public class RedBlackOperator<P extends RBNodeProperties> {
	protected static <P extends RBNodeProperties> void setColor(IAlgorithmNode<P> n, boolean color) {
		if (n != null) n.getNodeProperties().setColor(color);
	}
	
	protected static <P extends RBNodeProperties> boolean colorOf(IAlgorithmNode<P> n) {
		return (n == null ? RBNodeProperties.BLACK : n.getNodeProperties().getColor());
	}
	
    protected static <P extends RBNodeProperties> IAlgorithmNode<P> parentOf(IAlgorithmNode<P> p) {
        return (p == null ? null: p.getParent());
    }
    
    protected static <P extends RBNodeProperties> IAlgorithmNode<P> leftOf(IAlgorithmNode<P> p) {
        return (p == null ? null: p.getLeft());
    }
    
    protected static <P extends RBNodeProperties> IAlgorithmNode<P> rightOf(IAlgorithmNode<P> p) {
        return (p == null ? null: p.getRight());
    }
	
    public boolean isRed(IAlgorithmNode<P> h) {
		if (h == null) return false;
		return (h.getNodeProperties().getColor() == RBNodeProperties.RED);
	}
    
    protected boolean isRedBlack(IAlgorithmNode<P> x) {
    	assert(is23(x));
    	assert(isBalanced(x));
    	return true;
    }
    
    private boolean is23(IAlgorithmNode<P> x) {
        if (x == null) return true;
        if (x.getParent() != null && isRed(x) && (isRed(x.getLeft()) || isRed(x.getRight())))
            return false;
        return is23(x.getLeft()) && is23(x.getRight());
    }
    
    private boolean isBalanced(IAlgorithmNode<P> root)
    { // Do all paths from root to leaf have same number of black edges?
        int black = 0;     // number of black links on path from root to min
        IAlgorithmNode<P> x = root;
        while (x != null)
        {
            if (!isRed(x)) black++;
            x = x.getLeft();
        }
        //System.out.println("black links: " + black);
        return isBalanced(root, black);
    }
    
    private boolean isBalanced(IAlgorithmNode<P> x, int black)
    { // Does every path from the root to a leaf have the given number 
        // of black links?
        if      (x == null && black == 0) return true;
        else if (x == null && black != 0) return false;
        if (!isRed(x)) black--;
        return isBalanced(x.getLeft(), black) && isBalanced(x.getLeft(), black);
    } 
    
}
