package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Utility functions for Left-Leaning Red Black Tree Operators
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;

public class LLRBOperator<P extends RBNodeProperties> {
	
	protected IAlgorithmNode<P> rotateLeft(IAlgorithmNode<P> h) {
		IAlgorithmNode<P> x = h.getRight();
		h.freeze(.0125);
		x.getNodeProperties().setColor(h.getNodeProperties().getColor());
		h.getNodeProperties().setColor(RBNodeProperties.RED);
		h.rotateLeft();
		h.freeze(.025);
		return x;
	}
	
	protected IAlgorithmNode<P> rotateRight(IAlgorithmNode<P> h) {
		IAlgorithmNode<P> x = h.getLeft();
		h.freeze(.0125);
		x.getNodeProperties().setColor(h.getNodeProperties().getColor());
		h.getNodeProperties().setColor(RBNodeProperties.RED);
		h.rotateRight();
		h.freeze(.025);
		return x;
	}
	
	protected void flipColor(IAlgorithmNode<P> h) {
		h.getNodeProperties().setColor(!h.getNodeProperties().getColor());
	}
	
	protected void flipColors(IAlgorithmNode<P> h) {
		assert (h != null) && (h.getLeft() != null) && (h.getRight() != null);
        assert (!isRed(h) &&  isRed(h.getLeft()) &&  isRed(h.getRight())) ||
                (isRed(h) && !isRed(h.getLeft()) && !isRed(h.getRight()));
		h.freeze(.0125);
        flipColor(h);
		flipColor(h.getLeft());
		flipColor(h.getRight());
		h.freeze(.025);
	}
	
	protected IAlgorithmNode<P> balance(IAlgorithmNode<P> h) {
        assert (h != null);
        assert(((ShadowNode<P>)h).isBST());
        if (isRed(h.getRight()))                      h = rotateLeft(h);
        if (isRed(h.getLeft()) && isRed(h.getLeft().getLeft())) h = rotateRight(h);
        if (isRed(h.getLeft()) && isRed(h.getRight()))     flipColors(h);
        assert(((ShadowNode<P>)h).isBST());
        return h;
    }
	
	public boolean isRed(IAlgorithmNode<P> h) {
		if (h == null) return false;
		return (h.getNodeProperties().getColor() == RBNodeProperties.RED);
	}
}
