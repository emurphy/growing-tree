package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Red Black Tree Node Properties (also used for LLRB)
 * @author Josh Israel
 */

import java.awt.Color;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

public class RBNodeProperties extends NodeProperties {
	public static final boolean RED =true;
	public static final boolean BLACK = false;
	private boolean color;
	
	public RBNodeProperties makeDefaultProperties() {
		RBNodeProperties np = new RBNodeProperties();
		np.setColor(RED);
		return np;
	}
	
	public boolean getColor()       { return color; }
	public void setColor(boolean c) { color = c; }

	// Overrides call-back that determines color
	//   node is drawn
	public Color getNodeColor() { 
		if (color == RED) return Color.red;
		else return Color.black;
	}
	
	public boolean colorParentLink() { return true; }
	public Integer getULIntegerFieldValue() { return getHeight(); }
}
