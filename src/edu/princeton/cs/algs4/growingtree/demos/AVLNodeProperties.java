package edu.princeton.cs.algs4.growingtree.demos;

/**
 * AVL Tree Node Properties
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

public class AVLNodeProperties extends NodeProperties {
	public Integer getULIntegerFieldValue() { return getHeight(); }
	
	public AVLNodeProperties makeDefaultProperties() {
		return new AVLNodeProperties();
	}
}
