package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Standard BST Search
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;
import edu.princeton.cs.algs4.growingtree.interfaces.ISearchOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.ISearchingNode;

public class BSTSearch<P extends NodeProperties> implements ISearchOperator<P> {
	
	public IAlgorithmNode<P> search(ISearchingNode<P> root, Comparable<INode<P>> keyCompare) {
		if (root == null) {
			return null;
		}
		int cmp = keyCompare.compareTo(root);
		if (cmp > 0) {
			return search(root.getRight(), keyCompare);
		}
		else if (cmp < 0) {
			return search(root.getLeft(), keyCompare);
		}
		else {
			root.markFound();
			return root;
		}
	}
	
	public INode<P> doSearch(ISearchingNode<P> root, Comparable<INode<P>> keyCompare) {
		return search(root, keyCompare);
	}
}
