package edu.princeton.cs.algs4.growingtree.demos;

/**
 * AVL Tree Insertion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertingNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;

public class AVLInsertion<P extends AVLNodeProperties> extends AVLOperator<P> implements IInsertOperator<P>{

	public AVLInsertion() {
		super();
	}
	
	public AVLInsertion(int balFactor) {
		super(balFactor);
	}
	
	private void insert(IInsertingNode<P> t, INode<P> x) {
		assert (t != null);
		int c = x.compareTo(t);
		if (c==0) return;
		if (c<0) {
			if (t.getLeft() == null) { t.insertLeft(x); }
			else 					{ insert(t.getLeft(), x); }
		} 
		else {
			if (t.getRight() == null) { t.insertRight(x); }
			else 					{ insert(t.getRight(), x); }
		}
		bal(t);
		return;
	}
	@Override
	public void doInsert(IInsertingNode<P> root, INode<P> newNode) {
		if (newNode == null) return;
		insert(root, newNode);
	}
	
}
