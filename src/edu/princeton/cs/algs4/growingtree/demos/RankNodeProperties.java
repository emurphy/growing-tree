package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Rank Balanced Tree Node Properties
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;

public class RankNodeProperties extends NodeProperties {
	private int LRrankDiff = 1;
	private int LLrankDiff = 1;
	private int rank = NULL_HEIGHT + 1;
	public RankNodeProperties makeDefaultProperties() {
		return new RankNodeProperties();
	}
	
	public Integer getULIntegerFieldValue() { return getRank(); }
	public Integer getLLIntegerFieldValue() { return getLLrankDiff(); }
	public Integer getLRIntegerFieldValue() { return getLRrankDiff(); }

	public void setLRrankDiff(int lRrankDiff) {
		LRrankDiff = lRrankDiff;
	}

	public int getLRrankDiff() {
		return LRrankDiff;
	}

	public void setLLrankDiff(int lLrankDiff) {
		LLrankDiff = lLrankDiff;
	}

	public int getLLrankDiff() {
		return LLrankDiff;
	}

	public void setRank(int r) {
		this.rank = r;
	}

	public int getRank() {
		return rank;
	}
}
