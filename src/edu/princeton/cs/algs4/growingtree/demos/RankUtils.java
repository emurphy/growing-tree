package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Utility functions for Rank Balanced Tree Operators
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.experiments.IExperimentLogger;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;

public class RankUtils<P extends RankNodeProperties> {
	public static <P extends RankNodeProperties> int rank(IAlgorithmNode<P> n) {
		if (n == null) return -1;
		else return n.getNodeProperties().getRank();
	}
	
	public static <P extends RankNodeProperties> void diffUpdate(IAlgorithmNode<P> n) {
		int nRank = n.getNodeProperties().getRank();
		int lRank = rank(n.getLeft());
		int rRank = rank(n.getRight());
		if (n.getParent() != null) RankUtils.diffUpdate(n.getParent());
		if (nRank - lRank != n.getNodeProperties().getLLrankDiff()) {
			n.getNodeProperties().setLLrankDiff(nRank - lRank);
		}
		if (nRank - rRank != n.getNodeProperties().getLRrankDiff()) {
			n.getNodeProperties().setLRrankDiff(nRank - rRank);
		}
	}
	
	public static <P extends RankNodeProperties> IAlgorithmNode<P> sibling(IAlgorithmNode<P> n) {
		assert (n != null);
		IAlgorithmNode<P> p = n.getParent();
		assert (p != null);
		if (n == p.getLeft()) return p.getRight();
		else 				  return p.getLeft();
	}
	
	public static <P extends RankNodeProperties> void promote(IAlgorithmNode<P> q) {
		assert (q != null);
		q.getNodeProperties().setRank(q.getNodeProperties().getRank() + 1);
		diffUpdate(q);
	}
	
	public static <P extends RankNodeProperties> void demote(IAlgorithmNode<P> q) {
		assert (q != null);
		q.getNodeProperties().setRank(q.getNodeProperties().getRank() - 1);
		diffUpdate(q);
	}
	
	// DOES NOT CHANGE RANKS
	public static <P extends RankNodeProperties> void rotateUp(IAlgorithmNode<P> q) {
		assert(q != null);
		IAlgorithmNode<P> p = q.getParent();
		assert(p != null);
		if (q == p.getLeft()) p.rotateRight();
		else 				  p.rotateLeft();
		diffUpdate(q);
	}
	
	public static <P extends RankNodeProperties> boolean hasValidRankDiffs(IAlgorithmNode<P> root) {
		if (root == null) return true;
		int lldiff = root.getNodeProperties().getLLrankDiff();
		int lrdiff = root.getNodeProperties().getLRrankDiff();
		int LLdiff = rank(root) - rank(root.getLeft());
		int LRdiff = rank(root) - rank(root.getRight());
		assert (lldiff == LLdiff);
		assert (lrdiff == LRdiff);
		assert(lldiff == 2 || lldiff == 1);
		assert(lrdiff == 2 || lrdiff == 1);
		assert(RankUtils.hasValidRankDiffs(root.getLeft()));
		assert(RankUtils.hasValidRankDiffs(root.getRight()));
		return true;
	}
}
