package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Left-Leaning Red Black Tree Deletion Operators
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.GrowingTreeNode;
import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeletingNode;

public class LLRBDeletion<P extends RBNodeProperties> extends LLRBOperator<P> implements IDeleteOperator<P> {

	// Assuming that h is red and both h.left and h.left.left
    // are black, make h.left or one of its children red.
    private IAlgorithmNode<P> moveRedLeft(IAlgorithmNode<P> h) {
        assert (h != null);
        assert isRed(h) && !isRed(h.getLeft()) && !isRed(h.getLeft().getLeft());

        flipColors(h);
        if (isRed(h.getRight().getLeft())) { 
            rotateRight(h.getRight());
            h = rotateLeft(h);
            // flipColors(h);
        }
        return h;
    }
 
    // Assuming that h is red and both h.right and h.right.left
    // are black, make h.right or one of its children red.
    private IAlgorithmNode<P> moveRedRight(IAlgorithmNode<P> h) {
        assert (h != null);
        assert isRed(h) && !isRed(h.getRight()) && !isRed(h.getRight().getLeft());
        flipColors(h);
        if (isRed(h.getLeft().getLeft())) { 
            h = rotateRight(h);
            // flipColors(h);
        }
        return h;
    }
    
    private IAlgorithmNode<P> deleteMin(IDeletingNode<P> key, IAlgorithmNode<P> h) {
    	assert(((ShadowNode<P>)h).isBST());
        if (h.getLeft() == null) {
        	h.getNodeProperties().setColor(key.getNodeProperties().getColor());
        	IAlgorithmNode<P> ret = key.successorHibbardDelete();
        	assert(((ShadowNode<P>)h).isBST());
        	return ret;
        }

        if (!isRed(h.getLeft()) && !isRed(h.getLeft().getLeft()))
            h = moveRedLeft(h);

        deleteMin(key, h.getLeft());
        assert(((ShadowNode<P>)h).isBST());
        return balance(h);
    }
    
	public IAlgorithmNode<P> delete(IAlgorithmNode<P> h, IDeletingNode<P> key) {
		assert(((ShadowNode<P>)h).isBST());
		if (key.compareTo(h) < 0)  {
            if (!isRed(h.getLeft()) && !isRed(h.getLeft().getLeft()))
                h = moveRedLeft(h);
            delete(h.getLeft(), key);
        }
        else {
            if (isRed(h.getLeft()))
                h = rotateRight(h);
            if (key.compareTo(h) == 0 && (h.getRight() == null)) {
            	return key.successorHibbardDelete();
            }
            if (!isRed(h.getRight()) && !isRed(h.getRight().getLeft()))
                h = moveRedRight(h);
            if (key.compareTo(h) == 0) {
            	IAlgorithmNode<P> newH = h.getSuccessor();
            	deleteMin(key, h.getRight());
            	h = newH;
            }
            else {
            	delete(h.getRight(), key);
        		assert(((ShadowNode<P>)h).isBST());
            }
        }
        return balance(h);
	}

	public IAlgorithmNode<P> getPostDeletionValidNode(IAlgorithmNode<P> root, IDeletingNode<P> node) {
		IAlgorithmNode<P> validNode = root;
		if (root == node) {
			if (root.getLeft()==null && root.getRight()==null) {
				validNode = null;
			}
			else if (root.getLeft()!=null) {
				validNode = root.getLeft();
			}
			else {
				validNode = root.getRight();
			}
		}
		return validNode;
	}
	
	@Override
	public void doDelete(IAlgorithmNode<P> root, IDeletingNode<P> node) {
	    // if both children of root are black, set root to red
	    if (!isRed(root.getLeft()) && !isRed(root.getRight()))
	        root.getNodeProperties().setColor(RBNodeProperties.RED);
	    IAlgorithmNode<P> validNode = getPostDeletionValidNode(root, node);
	    
	    delete(root, node);
	    //if (validNode == null) {
	    //	ShadowNode<P> newRoot = (ShadowNode<P>) validNode.getRoot();
	    //	return;
	    //}
	    if (validNode != null)
	    	validNode.getRoot().getNodeProperties().setColor(RBNodeProperties.BLACK);
	    //System.out.println("root has node: " + root.)
	    
	}

}
