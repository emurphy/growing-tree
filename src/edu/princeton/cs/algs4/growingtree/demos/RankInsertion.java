package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Rank Balanced Tree Insertion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertingNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;

public class RankInsertion<P extends RankNodeProperties> extends BSTInsertion<P> {
	private <P extends RankNodeProperties> void bal(IAlgorithmNode<P> q) {
		assert(q != null);
		IAlgorithmNode<P> p = q.getParent();
		if (p == null) return;
		if (RankUtils.rank(q) != RankUtils.rank(p)) {
			RankUtils.diffUpdate(p);
			return;
		}
		
		IAlgorithmNode<P> s = RankUtils.sibling(q);
		// Sibling is a 1-child
		if (RankUtils.rank(s) + 1 == RankUtils.rank(p)) {
			RankUtils.promote(p);
			bal(p);
		}
		// Sibling is a 2-child
		else if (RankUtils.rank(s) + 2 == RankUtils.rank(p)) {
			IAlgorithmNode<P> t;
			if (q == p.getLeft()) {
				t = q.getRight();
			}
			else {
				t = q.getLeft();
			}
			if (RankUtils.rank(t) + 2 == RankUtils.rank(q)) {
				RankUtils.rotateUp(q);
				RankUtils.demote(p);
				RankUtils.diffUpdate(q);
				//RankUtils.diffUpdate(p);
			}
			else if (RankUtils.rank(t) + 1 == RankUtils.rank(q)) {
				RankUtils.rotateUp(t); 
				RankUtils.promote(t);
				RankUtils.rotateUp(t);
				RankUtils.demote(p);
				RankUtils.demote(q);
				RankUtils.diffUpdate(p);
			}
		}
		else {
			assert(false);
		}
	}
	
	
	public void doInsert(IInsertingNode<P> root, INode<P> newNode) {
		assert(RankUtils.hasValidRankDiffs(root));
		if (newNode == null) return;
		IAlgorithmNode<P> q = insert(root, newNode);
		bal(q);
		assert(RankUtils.hasValidRankDiffs(root.getRoot()));
	}
}
