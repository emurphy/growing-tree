package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Red Black Tree Insertion Operator
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertingNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;

public class RedBlackInsertion<P extends RBNodeProperties> extends RedBlackOperator<P> implements
																   IInsertOperator<P> {
    
	private void fixAfterInsertion(IAlgorithmNode<P> x) {
        x.getNodeProperties().setColor(RBNodeProperties.RED);
        while (x != null && x.getParent() != null && colorOf(x.getParent()) == RBNodeProperties.RED) {
            if (x.getParent() == leftOf(parentOf(parentOf(x)))) {
                IAlgorithmNode<P> y = rightOf(parentOf(parentOf(x)));
                if (colorOf(y) == RBNodeProperties.RED) {
                	x.freeze();
                    setColor(parentOf(x), RBNodeProperties.BLACK);
                    setColor(y, RBNodeProperties.BLACK);
                    setColor(parentOf(parentOf(x)), RBNodeProperties.RED);
                    x.freeze();
                    x = parentOf(parentOf(x));
                } else {
                    if (x == rightOf(parentOf(x))) {
                        x = parentOf(x);
                        x.rotateLeft();
                    }
                    x.freeze();
                    setColor(parentOf(x), RBNodeProperties.BLACK);
                    setColor(parentOf(parentOf(x)), RBNodeProperties.RED);
                    x.freeze();
                    if (parentOf(parentOf(x)) != null)
                        parentOf(parentOf(x)).rotateRight();
                }
            } else {
                IAlgorithmNode<P> y = leftOf(parentOf(parentOf(x)));
                if (colorOf(y) == RBNodeProperties.RED) {
                	x.freeze();
                    setColor(parentOf(x), RBNodeProperties.BLACK);
                    setColor(y, RBNodeProperties.BLACK);
                    setColor(parentOf(parentOf(x)), RBNodeProperties.RED);
                    x.freeze();
                    x = parentOf(parentOf(x));
                } else {
                    if (x == leftOf(parentOf(x))) {
                        x = parentOf(x);
                        x.rotateRight();
                    }
                    x.freeze();
                    setColor(parentOf(x),  RBNodeProperties.BLACK);
                    setColor(parentOf(parentOf(x)), RBNodeProperties.RED);
                    x.freeze();
                    if (parentOf(parentOf(x)) != null)
                        parentOf(parentOf(x)).rotateLeft();
                }
            }
        }
        setColor(x.getRoot(), RBNodeProperties.BLACK);
	}
	
	@Override
	public void doInsert(IInsertingNode<P> root, INode<P> newNode) {
		assert(isRedBlack(root));
		//System.out.println("Root height: " + root.getNodeProperties().getHeight() + " " + root.getNodeProperties().getSize());
		if (newNode == null) {
			root.getNodeProperties().setColor(RBNodeProperties.BLACK);
			return;
		}
		IInsertingNode<P> t = root;
        while (true) {
            int cmp = newNode.compareTo(t);
            if (cmp == 0) {
                break;
            } 
            else if (cmp < 0) {
                if (t.getLeft() != null) {
                    t = t.getLeft();
                } 
                else {
                	newNode.getNodeProperties().setColor(RBNodeProperties.BLACK);
                	IAlgorithmNode<P> added = t.insertLeft(newNode);
                	newNode.getNodeProperties().setColor(RBNodeProperties.RED);
                    fixAfterInsertion(added);
                    break;
                }
            } 
            else { // cmp > 0
                if (t.getRight() != null) {
                    t = t.getRight();
                } else {
                	newNode.getNodeProperties().setColor(RBNodeProperties.BLACK);
                	IAlgorithmNode<P> added = t.insertRight(newNode);
                	newNode.getNodeProperties().setColor(RBNodeProperties.RED);
                    fixAfterInsertion(added);
                    break;
                }
            }
        }
        assert(isRedBlack(root.getRoot()));
	}

}
