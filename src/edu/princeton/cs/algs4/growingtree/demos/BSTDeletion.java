package edu.princeton.cs.algs4.growingtree.demos;

/**
 * Standard BST Deletion
 * @author Josh Israel
 */

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeletingNode;

public class BSTDeletion<P extends NodeProperties> implements IDeleteOperator<P> {
	public void doDelete(IAlgorithmNode<P> root, IDeletingNode<P> node) {
		node.successorHibbardDelete();
	}
}
