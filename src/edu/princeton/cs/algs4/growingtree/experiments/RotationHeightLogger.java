package edu.princeton.cs.algs4.growingtree.experiments;

import java.util.Arrays;
import java.util.HashMap;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;


public class RotationHeightLogger<P extends NodeProperties> implements IExperimentLogger<P> {
	protected HashMap<Integer, Integer> heightCounts = new HashMap<Integer,Integer>();
	protected int sizeCount = 0;
	private int height(ShadowNode<P> n) {
		if (n == null) return -1;
		return 1 + Math.max(height(n.getLeft()), height(n.getRight()));
	}
	
	public void printCounts() {
		Integer[] heights = new Integer[heightCounts.keySet().size()];
		heightCounts.keySet().toArray(heights);
		Arrays.sort(heights);
		for (int i = 0; i < 50; i++) {
			Integer count = heightCounts.get(i);
			if (count != null) {
				//System.out.print(count + ", ");
			}
			else {
				//System.out.print("0, ");
			}
			//System.out.println(heights[i] + ": " + count);
		}
		System.out.println();
		System.out.println(sizeCount);
	}
	
	@Override
	public void logDeletion(ShadowNode<P> n) {}

	@Override
	public void logInsertion(ShadowNode<P> n) {}

	@Override
	public void logRotation(ShadowNode<P> n) {
		Integer height = n.getNodeProperties().getHeight();
		sizeCount += n.getNodeProperties().getSize();
		//if (height > n.getRoot().getNodeProperties().getHeight()) {
		//	assert(false);
		//}
		//System.out.println("Logging height: " + height);
		Integer count = heightCounts.get(height);
		if (count == null) {
			heightCounts.put(height, 1);
		}
		else {
			heightCounts.put(height, count+1);
		}
	}

	@Override
	public void logSearchHit(ShadowNode<P> n) {}

	@Override
	public void logOther(IAlgorithmNode<P> n, int eventId) {
		// TODO Auto-generated method stub
		
	}

}
