package edu.princeton.cs.algs4.growingtree.experiments;

import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.interfaces.IAlgorithmNode;
import edu.princeton.cs.algs4.growingtree.interfaces.INode;

/**
 * This class provides the ability to log events in a series
 * of operations. When added to a <code>TreeVisualization</code>
 * or <code>TreeExperiment</code>, it receives callbacks for
 * each event.
 * @author Josh Israel
 *
 * @param <P>
 */

public interface IExperimentLogger<P extends NodeProperties> {
	/**
	 * event_id for logOther call for height changing
	 */
	public static final int HEIGHT_UPDATE = 0;
	/**
	 * event_id for logOther call for size changing
	 */
	public static final int SIZE_UPDATE = 1;
	
	/**
	 * Called on <code>IInsertNode.insertLeft</code>
	 * and <code>IInsertNode.insertRight</code>
	 * @param n Node that was just inserted
	 */
	public void logInsertion(ShadowNode<P> n);
	/**
	 * Called on <code>IAlgorithmNode.rotateLeft</code>
	 * and <code>IAlgorithmNode.rotateRight</code> PRIOR
	 * to the actual rotation.
	 * @param n Node is being rotated down the tree
	 */
	public void logRotation(ShadowNode<P> n);
	
	/**
	 * Called on <code>IDeletingNode.predecessorHibbardDelete</code>
	 * and <code>IDeletingNode.successorHibbardDelete</code> PRIOR
	 * to the actual deletion.
	 * @param n Node being deleted
	 */
	public void logDeletion(ShadowNode<P> n);

	/**
	 * Called on <code>ISearchingNode.markFound</code>
	 * @param n Node that has been found
	 */
	public void logSearchHit(ShadowNode<P> n);
	
	/**
	 * Catch-all logging function for anything missed by the others.
	 * Meant in part to be used by an actual operator if necessary.
	 * @param n Node being logged
	 * @param event_id Used to identify the event this call represents
	 */
	public void logOther(IAlgorithmNode<P> n, int event_id);
}
