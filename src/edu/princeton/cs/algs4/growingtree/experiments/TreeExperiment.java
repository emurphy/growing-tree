package edu.princeton.cs.algs4.growingtree.experiments;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import edu.princeton.cs.algs4.growingtree.demos.AVLDeletion;
import edu.princeton.cs.algs4.growingtree.demos.AVLInsertion;
import edu.princeton.cs.algs4.growingtree.demos.AVLNodeProperties;
import edu.princeton.cs.algs4.growingtree.demos.BSTDeletion;
import edu.princeton.cs.algs4.growingtree.demos.BSTInsertion;
import edu.princeton.cs.algs4.growingtree.demos.BSTSearch;
import edu.princeton.cs.algs4.growingtree.demos.LLRBDeletion;
import edu.princeton.cs.algs4.growingtree.demos.LLRBInsertion;
import edu.princeton.cs.algs4.growingtree.demos.RBNodeProperties;
import edu.princeton.cs.algs4.growingtree.demos.RankDeletion;
import edu.princeton.cs.algs4.growingtree.demos.RankInsertion;
import edu.princeton.cs.algs4.growingtree.demos.RankNodeProperties;
import edu.princeton.cs.algs4.growingtree.demos.RedBlackDeletion;
import edu.princeton.cs.algs4.growingtree.demos.RedBlackInsertion;
import edu.princeton.cs.algs4.growingtree.demos.SplayOperators;
import edu.princeton.cs.algs4.growingtree.demos.StdRandom;
import edu.princeton.cs.algs4.growingtree.demos.SplayOperators.SplayDeletion;
import edu.princeton.cs.algs4.growingtree.demos.SplayOperators.SplayInsertion;
import edu.princeton.cs.algs4.growingtree.demos.SplayOperators.SplaySearch;
import edu.princeton.cs.algs4.growingtree.framework.NodeProperties;
import edu.princeton.cs.algs4.growingtree.framework.ShadowNode;
import edu.princeton.cs.algs4.growingtree.framework.TreeVisualization;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.ISearchOperator;

/**
 * This is the class analogous to <code>TreeVisualization</code> for experiments.
 * It stores a set of trees and provides a simple API to perform operations on all
 * of them simultaneously, with callbacks for logging.
 * Included is an example of how it can be used to log the number of rotations at
 * each height of the tree.
 * @author Josh Israel
 *
 */

public class TreeExperiment {

	private List<ExperimentTree<?>> trees;
	
	public TreeExperiment() {
		trees = new LinkedList<ExperimentTree<?>>();
	}
	
	
	/**
	 * Adds a tree to the set of trees to run the experiment on.
	 * @param name Name of the tree to be created
	 * @param p Instance of the proper NodeProperties subclass
	 * @param inserter The IInsertOperator to be used for this tree
	 * @param searcher The ISearchOperator to be used for this tree
	 * @param deleter The IDeleteOperator to be used for this tree
	 * @param logger The IExperimentLogger to be used for this tree. This
	 * will receive callbacks from the tree when operations occur.
	 */
	public <P extends NodeProperties> void addTree(String name, P p,
			   									   IInsertOperator<P> inserter,
			   									   ISearchOperator<P> searcher,
			   									   IDeleteOperator<P> deleter,
			   									   IExperimentLogger<P> logger) {
		ExperimentTree<P> newTree = new ExperimentTree<P>(name, p, inserter, searcher, deleter, logger);
		trees.add(newTree);
 	}
	
	/**
	 * Insert an element into each of the trees
	 * @param d Key of the node to be inserted
	 */
	public void insert(double d) {
		for (ExperimentTree<?> t : trees) {
			t.insert(d);
		}
	}
	
	/**
	 * Perform a search for an element
	 * @param d Key of node to be sought
	 */
	public void search(double d) {
		for (ExperimentTree<?> t : trees) {
			t.search(d);
		}
	}
	
	/**
	 * Deletes a node from the tree
	 * @param d Key of node to be deleted
	 */
	public void delete(double d) {
		for (ExperimentTree<?> t : trees) {
			t.delete(d);
		}
	}
	
	public static void main(String[] args) {
		RotationHeightLogger<RBNodeProperties> LLRBlogger = new RotationHeightLogger<RBNodeProperties>();
		RotationHeightLogger<RBNodeProperties> RedBlacklogger = new RotationHeightLogger<RBNodeProperties>();
		RotationHeightLogger<AVLNodeProperties> AVL1logger = new RotationHeightLogger<AVLNodeProperties>();
		RotationHeightLogger<AVLNodeProperties> AVL2logger = new RotationHeightLogger<AVLNodeProperties>();
		RotationHeightLogger<AVLNodeProperties> RAVLlogger = new RotationHeightLogger<AVLNodeProperties>();
		RotationHeightLogger<RankNodeProperties> Ranklogger = new RotationHeightLogger<RankNodeProperties>();
		TreeExperiment experiment = new TreeExperiment();
		experiment.addTree("LLRB", new RBNodeProperties(), 
				   new LLRBInsertion<RBNodeProperties>(), 
				   new BSTSearch<RBNodeProperties>(), 
				   new LLRBDeletion<RBNodeProperties>(),
				   LLRBlogger);
		experiment.addTree("Red-Black", new RBNodeProperties(), 
				   new RedBlackInsertion<RBNodeProperties>(), 
				   new BSTSearch<RBNodeProperties>(), 
				   new RedBlackDeletion<RBNodeProperties>(),
				   RedBlacklogger);
		experiment.addTree("AVL-1", new AVLNodeProperties(), 
				   new AVLInsertion<AVLNodeProperties>(), 
				   new BSTSearch<AVLNodeProperties>(), 
				   new AVLDeletion<AVLNodeProperties>(),
				   AVL1logger);
		experiment.addTree("AVL-2", new AVLNodeProperties(), 
				   new AVLInsertion<AVLNodeProperties>(2), 
				   new BSTSearch<AVLNodeProperties>(), 
				   new AVLDeletion<AVLNodeProperties>(2),
				   AVL2logger);
		experiment.addTree("RAVL", new AVLNodeProperties(), 
				   new AVLInsertion<AVLNodeProperties>(), 
				   new BSTSearch<AVLNodeProperties>(), 
				   new BSTDeletion<AVLNodeProperties>(),
				   RAVLlogger);
		experiment.addTree("Rank", new RankNodeProperties(),
						   new RankInsertion<RankNodeProperties>(),
						   new BSTSearch<RankNodeProperties>(),
						   new RankDeletion<RankNodeProperties>(),
						   Ranklogger);
		String filename = args[0];
		System.out.println(System.getProperty("user.dir"));
		BufferedInputStream input = null;
		String line = null;
		try {
			input = new BufferedInputStream(new FileInputStream(filename));
			System.out.println("got input");
			String charsetName = "UTF-8";
			Scanner scanner = new Scanner(new BufferedInputStream(input), charsetName);
			System.out.println(scanner.nextLine());
			System.out.println(scanner.nextLine());
			System.out.println(scanner.nextLine());
			//for (int i =0; i < 10000; i++) {
			int count = 0;
			while (scanner.hasNextLine()) {
				if (count % 10000 == 0) System.out.println(count);
				line = scanner.nextLine();
				if (line == null) break;
				String[] tokens = line.split(" ");
				String op = tokens[0];
				String key = tokens[1];
				if (op.equals("ins")) {
					experiment.insert(Double.parseDouble(key));
				}
				else if (op.equals("del")) {
					experiment.delete(Double.parseDouble(key));
				}
				else {
					assert(false);
				}
				count++;
			}
		}
		catch (ArrayIndexOutOfBoundsException ae) {
			System.out.println("Bad line is: " + line);
			ae.printStackTrace();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		finally {
			if (input != null) {
				try {
					input.close();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		System.out.print("LLRB, ");
		LLRBlogger.printCounts();
		System.out.print("Red Black, ");
		RedBlacklogger.printCounts();
		System.out.print("AVL-1, ");
		AVL1logger.printCounts();
		System.out.print("AVL-2, ");
		AVL2logger.printCounts();
		System.out.print("RAVL, ");
		RAVLlogger.printCounts();
		System.out.print("Rank, ");
		Ranklogger.printCounts();

	}
	
}
