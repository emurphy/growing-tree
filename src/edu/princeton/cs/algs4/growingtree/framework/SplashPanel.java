package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)SplashPanel.java
 *
 * Last Modified: 9/01/02
 */

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.net.*;

/** * This class provides the panel for a splash screen.
	*
 	* @author  Corey Sanders
	* @version 3.7 9/01/02
 	*/
public class SplashPanel extends JPanel {


	/**
	 * Sole constructor which sets all of the default values for the Panel. Calls the super
	 * constructor of JPanel.
	 */
    public SplashPanel() {

		super();

    }

	/**
	 * Overides paintComponenet and is called whenever the Panel needs to be painted.
	 *
	 * @param g Graphics to which the component is drawn.
	 */
	public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D)g;

		super.paintComponent(g);

		g2.drawImage(getImage("growingTreeSplash.jpg"), 0, 0, this);

	}


	/**
	 * Gets the icon from the resource (JAR file).
	 */
	public Image getImage(String imageName) {

   		// Get current classloader
   		URL url = this.getClass().getResource(imageName);


   		Image img=Toolkit.getDefaultToolkit().getImage(url);

		return img;

	}




}
