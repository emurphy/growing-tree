package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)TreeMessageListener.java
 *
 *	Last Modified: 9/15/01
 */

import java.awt.*;
import java.util.*;

/**
 * The listener interface for receiving TreeMessage events.
 * The class that is interested in processing a TreeMesasge event
 * implements this interface, and the object created with that
 * class is registered with an TreeMessage Object, using the object's
 * <code>addTreeMessageListener</code> method. When the action event
 * occurs, that object's <code>treeMessageEventPerformed</code> method is
 * invoked.
 *
 * @author Corey Sanders
 * @version 1.2 9/15/01
 */
public interface TreeMessageListener extends EventListener {

	 /**
     * Invoked when an Tree Message action occurs.
     */
	public void treeMessageEventPerformed(TreeMessageEvent e);
}
