package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)WaitingActionList.java
 *
 * Last Modified: 9/15/01
 */

import java.awt.geom.AffineTransform;
import java.util.Collection;
import java.util.LinkedList;


/**
 * Linked list implementation of the <tt>List</tt> interface containing String actions and accompanying Object elements.  Implements all
 * optional list operations, but only permits a <code>String</code> with <code>Object</code> insertion.  In addition to implementing the <tt>List</tt> interface,
 * the <tt>WaitingActionList</tt> class extends the uniformly named methods within LinkedList to
 * <tt>get</tt>, <tt>remove</tt> and <tt>insert</tt> an element at the
 * beginning and end of the list.  These operations allow this list to be
 * used as a stack, queue, or double-ended queue (deque).<p>
 *
 * The primary purpose of the class is for the development of </tt>nextAction</tt>,
 * which calls the <code>waitingAction</code> method for the <code>headelement</code> passed and the
 * next action and accompanying Object element.
 *
 * @author  Corey Sanders
 * @version 1.4 9/15/01
 */

public class WaitingActionList<P extends NodeProperties>  {

	/**
	 * LinkedList holding the <code>String</code> actions for the WaitingActions.
	 */
    private LinkedList<String> actions;
    
	/**
	 * LinkedList holding the <code>Object</code> elements for the WaitingActions.
	 */
	private LinkedList<ActionElementType<P>> elements;

	/**
     * Constructs an empty list.
     */
	public WaitingActionList() {
		actions = new LinkedList<String>();
		elements = new LinkedList<ActionElementType<P>>();
	}

    /**
     * Constructs a list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     *
     * @param actions the collection whose string actions are to be placed into this list as the String Actions.
     * @param elements the collection whose elements are to be places into the list as the Object elements.
     */
    public WaitingActionList(Collection<String> actions, 
        Collection<ActionElementType<P>> elements) {
	 	this.actions =  new LinkedList<String>(actions);
	 	this.elements = new LinkedList<ActionElementType<P>>(elements);
    }

    /**
     * Is the list of actions empty? (Since boths lists have the same number
     * of elements, just check actions list.)
     */
    public boolean isEmpty() {
        return actions.isEmpty();
    }

	/**
     * Appends the given <code>String</code> at the end of this list.
     *
     * @param action the <code>String</code> representing an action, to be inserted at the beginning of this list.
     *
     * @return <tt>true</tt> (as per the general contract of
	 * <tt>Collection.add</tt>).
     */
	public boolean add(String action) {
		return this.add(action, new ActionElementType<P>(new Integer(0)));
	}

	/**
     * Appends the given <code>String</code> and <code>Object</code> at the end of this list.
     *
     * @param action the <code>String</code> representing an action, to be inserted at the beginning of this list.
     * @param element the <code>Object</code> to be inserted in reference to the action.
	 *
     * @return <tt>true</tt> (as per the general contract of
	 * <tt>Collection.add</tt>).
     */
	public boolean add(String action, ActionElementType<P> element) {
		elements.add(element);
		return actions.add(action);
	}

	/**
     * Inserts the specified <code>String</code> and <code>Object</code> at the specified position in this list.
     * Shifts the <code>String</code> and <code>Object</code> currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index the integer index representing the location to add the action and element.
     * @param action the <code>String</code> representing an action, to be inserted at the beginning of this list.
     * @param element the <code>Object</code> to be inserted in reference to the action.
     *
     * @throws IndexOutOfBoundsException if the specified index is out of
     *		  range (<tt>index &lt; 0 || index &gt; size()</tt>).
     */
	public void add(int index, String action, ActionElementType<P> element) throws IndexOutOfBoundsException {
		elements.add(index, element);
		actions.add(index, action);
	}

	/**
     * Inserts the given <code>String</code> and <code>Object</code> at the beggining of this list.
     *
     * @param action the <code>String</code> representing an action, to be inserted at the beginning of this list.
     * @param element the <code>Object</code> to be inserted in reference to the action.
     */
	public void addFirst(String action, ActionElementType<P> element) {
		actions.addFirst(action);
		elements.addFirst(element);
	}

	/**
     * Appends the given <code>String</code> and <code>Object</code> at the end of this list.
     *
     * @param action the <code>String</code> representing an action, to be inserted at the end of this list.
     * @param element the <code>Object</code> to be inserted in reference to the action.
     */
	public void addLast(String action, ActionElementType<P> element) {
		actions.addLast(action);
		elements.addLast(element);
	}


	/**
     * Gets the given <code>String</code> of the action at the beggining of this list.
     *
     * @return action the <code>String</code> representing the first action.
     */
	public String getFirstAction() {
		return actions.getFirst();
	}

	/**
	 * Calls the next action of the <code>ObjectHead</code> headelement, using the waitingAction command.
	 * The call to <code>waitingAction</code> <b>always</b> passes the element, even if the element is unnecssary to
	 * that operations.
	 *
	 * @param headTree the <code>TreeHead</code> element of the <code>Tree</code> to which the action occurs.
	 */
	public void nextAction(TreeHead<P> headTree) {
		String action = actions.removeFirst();
		ActionElementType<P> element = elements.removeFirst();

		headTree.waitingAction(action, element);
	}


}

