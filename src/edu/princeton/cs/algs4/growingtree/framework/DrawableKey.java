package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)DrawableKey.java
 *
 * Last Modified: 9/15/01
 */

 import java.awt.*;
 import java.awt.geom.*;

/**
 * The <code>DrawableKey</code> interface is a drawable key, that must implement the
 * method <tt>drawKey</tt>, <code>fillKey</code>, and settings methods.<p>
 *
 * The drawing method must be one of the two calls. If the parameters include simply a
 * Graphics2D, then the drawing method redraws the key as previously drawn (using drawKey or fillKey) in the Graphics2D passed to it.
 * If the parameters are a Graphics2D and an AffineTransform, then the drawing method draws using the AffineTransform,
 * starting from (0,0) and assuming the removal of any current AffineTransforms. This indicates,
 * that a scaling of 10 will result in a size of 10 and a transpose of (5,10) results in the
 * drawing at (5,10) and so forth.<p>
 *
 * The <code>fillKey</code> fills the Graphics2D passed to it with the key. The method also sets
 * the transform so a call to <code>drawKey</code> without a transform will drawn exactly as drawn
 * in fillKey. <p>
 *
 * The settings methods, <code>setSettings</code> and <code>getSettings</code>, refer to <code>KetSettings</code>
 * generally used to render the key.<p>
 *
 * All of the methods must be present in a DrawableKey. <b>However</b> not all must act. If the
 * DrawableKey has no need for <code>KeySettings</code> than the setting methods can be left
 * empty.
 *
 * @see DrawingKey
 * @author Corey Sanders
 * @version 2.2 8/02/01
 */
public interface DrawableKey extends Cloneable {
	/**
	 * Draws the actual key, first setting all values for the Graphics2D g2.
	 * The drawing is done using the current Shape and transform, defined previously in <code>drawKey</code> or
	 * <code>fillKey</code>.
	 *
	 * @param g2 g2 Graphics2D that the key is painted to.
	 */
	public void drawKey(Graphics2D g2);

	/**
	 * Draws the key in the given Graphics2D, using the AffineTransform passed to it.
	 * The method uses the AffineTransform using no previous transforms.
	 *
	 * @param g2 g2 Graphics2D that this fills with the key.
	 * @param a <code>AffineTransform</code> that transforms the key, using no previous transforms.
	 */
	public void drawKey(Graphics2D g2, AffineTransform a);

	/**
	 * Draws the key filling the given Graphics2D. The method completely fills the Graphics2D passed
	 * to it, setting the transform as the one necessary to fill and center in the Graphics2D.
	 *
	 * @param g2 Graphics2D that fills with the key.
	 */
	public void fillKey(Graphics2D g2);

 	/**
     * Makes public the protected clone method in Object.
     *
     * @return Object that is cloned.
     */
    public Object clone();

	/**
	 * Sets the <code>KeySettings</code> of the <code>DrawableKey</code>.
	 * These settings are used for drawing the key in a given Graphics2D.
	 * <p>Can be left empty
	 *
	 * @param k <code>KeySettings</code> for use in drawing the key.
	 */
	public void setSettings(KeySettings k);

	/**
	 * Sets the <code>KeySettings</code> of the <code>DrawableKey</code>.
	 * These settings are used for drawing the key in a given Graphics2D.
	 *
	 * @param s <code>KeySettings</code> for use in drawing the key.
	 */
	public void setKeySettings(KeySettings s);


	/**
	 * Gets the <code>KeySettings</code> of the key.
	 *<p>Can be left empty
	 *
	 * @return <code>KeySettings</code> for use in drawing the key.
	 */
	public KeySettings getSettings();

	/**
	 * Saves the settings for the key.
	 */
	public void saveSettings();


	/**
	 * Restores the settings for the key.
	 */
	public void restoreSettings();

	/**
	 * Returns true if the <code>KeySettings</code> are currently saved.
	 *
	 * @return true if the settings are saved.
	 */
	public boolean isSettingsSaved();

}



