package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)OptionJPanel.java
 *
 * Last Modified: 9/15/01
 */

import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.net.*;

/**
 * OptionJPanel for use generally to extend to more specific usages. This just implements the adding of
 * and option listener and other useful methods, useful to all extending classes.
 *
 * <p>
 * @author  Corey Sanders
 * @version 1.3 9/15/01
 */
public class OptionJPanel extends JPanel {

	/**
	 * Id for the control panel.
	 */
	protected static int id = OptionEvent.OPTION_PANEL;

	/**
	 * Listeners for the Panel. Thye listen to the optionEvents made.
	 */
	protected LinkedList<OptionListener> listeners;

  /**
	* This sole constructor sets the listeners list and the original border and background.
 	*/
   public OptionJPanel() {
		listeners = new LinkedList<OptionListener>();
		setBorder(BorderFactory.createEmptyBorder());
    }

	/**
	 * Adds the OptionListener as listening to this current panel. It simply adds to listener to
	 * the list of listeners.
	 *
	 * @param optionListener the listener added to listen to the options
	 */
    public void addOptionListener(OptionListener optionListener) {
		listeners.add(optionListener);
	}

	/**
	 * Removes the OptionListener as listening to this current panel. It simply removes to listener to
	 * the list of listeners.
	 *
	 * @param optionListener the listener removed to listen to the options
	 */
    public void removeOptionListener(OptionListener optionListener) {
		listeners.remove(optionListener);
	}

	/**
	 * Passes an optionEvent. If the optionListener parameter is null, the method sends the optionEvent to all listeners. Otherwise, it only
	 * sends the message to the specified optionListener.
	 *
	 * @param optionEvent the event passed on.
	 * @param optionListener the specific listener to the event or null if all listeners should be
	 * called.
	 */
	public void optionAction(OptionEvent optionEvent, OptionListener optionListener) {

		// If listener is null.
		if (optionListener == null) {
			// Goes through entire list.
			ListIterator list = listeners.listIterator(0);

			while (list.hasNext()) {
				((OptionListener)list.next()).optionEventPerformed(optionEvent);
			}
		}
		else {
			optionListener.optionEventPerformed(optionEvent);
		}
	}

	/**
	 * Passes an optionEvent.
	 *
	 * @param optionEvent the event passed on.
	 */
	public void optionAction(OptionEvent optionEvent) {
		optionAction(optionEvent, null);
	}


	/**
	 * Creates an optionEvent based upon the id, event type, and object. If the optionListener
	 * parameter is null, the method sends the optionEvent to all listeners. Otherwise, it only
	 * sends the message to the specified optionListener.
	 *
	 * @param evt the String event passed in the optionEvent.
	 * @param object the object accompanying the String event type.
	 * @param optionListener the specific listener to the event or null if all listeners should be
	 * called.
	 */
	public void optionAction(String evt, Object object, OptionListener optionListener) {
		OptionEvent optionEvent = new OptionEvent(this, id, evt, object);

		optionAction(optionEvent, optionListener);

	}


	/**
	 * Creates an optionEvent based upon the id, event type, and object. All listeners are called.
	 *
	 * @param evt the String event passed in the optionEvent.
	 * @param object the object accompanying the String event type.
	 */
	public void optionAction(String evt, Object object) {
		optionAction(evt, object, null);
	}

	/**
	 * Creates an optionEvent based upon the id and event type. All listeners are called.
	 *
	 * @param evt the String event passed in the optionEvent.
	 */
	public void optionAction(String evt) {
		optionAction(evt, null, null);
	}

}




