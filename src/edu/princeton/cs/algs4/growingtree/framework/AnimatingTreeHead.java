package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)AnimatingTreeHead.java
 *
 * Last Modified: 9/15/01
 */

 import java.util.*;
 import java.lang.*;
 import java.awt.*;
 import java.awt.font.*;
import java.awt.geom.*;


/**
 * The AnimatingTreeHead interface extends DrawingTreeHead.<p>
 * The interface simply defines numerous methods which allow for the animating of
 * the <code>Tree</code> onto a given Graphics2D. The interface also defines many mutator and accesor
 * methods for information concerning the animating of the <code>AnimatingTree</code>
 *
 * @author Corey Sanders
 * @version 1.2 8/02/01
 * @see DrawingTreeHead
 * @see TreeHead
 * @see Tree
 * @see AnimatingTree
 */
public interface AnimatingTreeHead<P extends NodeProperties> extends DrawingTreeHead<P> {

	/**
	 * Adds the <code>Animation</code> to the list of Animations for the Head. The method does
	 * not add the tree as a listener to the <code>Animation</code>. That must be accomplished by
	 * the client.
	 *
	 * @param a <code>Animation</code> added to the Animation list.
	 */
	public void addTreeAnimator(Animation a);

	/**
	 * Gets the first <code>Animation</code> in the list of Animations for the Head and null if no
	 * Animations are present.
	 *
	 * @return first <code>Animation</code> in the Animation list.
	 */
	public Animation getTreeAnimator();

	/**
	 * Returns true if the current AnimatingTreeHead is animating (whether the animating list
	 * is empty. This determines if the entire tree is animating.
	 *
	 * @return true if the current tree is animating and not empty.
	 */
	public boolean isTreeAnimating();

	/**
	 * Animates the entire AnimatingTree. The animation is drawn to the Graphics2D passed.
	 * This method is called repeatedly for each Animation within the list.
	 *
	 * @param g2 Graphics2D to which the <code>Animation</code>s are drawn.
	 */
	public void AnimateTree(Graphics2D g2);

	/**
	 * Quickly removes all Animations within the Tree. The method sets the Status of all the
	 * Animations to <code>Animation.FINISH</code> so that al listeners of the Animations will
	 * receive the AnimationEvent.
	 */
	public void removeTreeAnimation();


	/**
	 * Sets the JumpStep of the current tree to the boolean value. The jumpStep determines whether
	 * the Animation skips through an entire step at each <code>AnimateTree</code> call.
	 *
	 * @param b sets the jumpStep to the value b.
	 */
	public void setJumpStep(boolean b);

	/**
	 * Gets the JumpStep of the current tree.
	 *
	 * @return true if the tree is jumpSteping.
	 */
	public boolean isJumpStep();

	/**
	 * Sets the StepPause of the current tree to the boolean value. The stepPause determines whether
	 * the Animation pauses after each step of the Animation completes.
	 *
	 * @param b sets the stepPause to the value b.
	 */
	public void setStepPause(boolean b);

	/**
	 * Gets the StepPause of the current tree.
	 *
	 * @return true if the tree is pausing at the completion of each step.
	 */
	public boolean isStepPause();

	/**
	 * Sets the status of the entire <code>AnimatingTree</code> to play.
	 */
	public void play();

	/**
	 * Sets the status of the entire <code>AnimatingTree</code> to stop.
	 */
	public void stop();

	/**
	 * Sets the status of the entire <code>AnimatingTree</code> to rewind.
	 */
	public void rewind();

	/**
	 * Sets the status of the entire <code>AnimatingTree</code> to pause.
	 */
	public void pause();

	/**
	 * Sets the step size of the <code>Animation</code>s of the tree. The integer value
	 * is passed to every Animation's <code>setStepSize</code> method.
	 *
	 * @param t integer step size setting.
	 */
	public void setTreeAnimationsStepSize(int t);

	/**
	 * Gets the step size of the <code>Animation</code>s of the tree.
	 *
	 * @return integer step size of the tree.
	 */
	public int getTreeAnimationStepSize();


	/**
	 * Gets the Tree's status, using the String status of <code>Animation</code>.
	 *
	 * @return String status of the Tree, representing the <code>Animation</code> status.
	 */
	public String getTreeStatus();

	/**
	 * Sets the Tree's status, using the String status of <code>Animation</code>.
	 *
	 * @param status String status of the Tree, representing the <code>Animation</code> status.
	 */
	public void setTreeStatus(String status);

	/**
	 * Makes a new tree message. The information presented usually includes :<p>
	 * <ul>
	 *<li>Tree Type</li>
	 *<li>Tree Size</li>
	 *<li>Tree Level</li>
	 *</ul>
	 */
	public String getTreeStatusMessage();

}
