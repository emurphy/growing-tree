package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)BSTTreeJPanel.java
 *
 * Last Modified: 9/15/01
 */

import javax.swing.*;
import javax.swing.event.*;

import edu.princeton.cs.algs4.growingtree.experiments.IExperimentLogger;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.ISearchOperator;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.beans.*;

/** * This class provides the panel for a <code>BSTTree</code>. It keeps the graphics for drawing
	* the tree and the image for redrawing. It also keeps a timer for the animation and all changes
	* to the tree proceed through the panel.
	*
 	* @author  Corey Sanders
	* @version 3.4 9/15/01
 	*/
public class BSTTreeJPanel<P extends NodeProperties> extends TreeJPanel<P> {

	/**
	 * Default color scheme.
	 */
	final public static String DEFAULT = "Default";


	/**
	 * Default color scheme.
	 */
	final public static String PRIMARY = "Primary";


	/**
	 * Princeton color scheme.
	 */
	final public static String PRINCETON = "Princeton";

	/**
	 * Christmas color scheme.
	 */
	final public static String CHRISTMAS = "Christmas";

	/**
	 * Halloween color scheme.
	 */
	final public static String HALLOWEEN = "Halloween";

	/**
	 * The String for the currently selected backgroundColor.
	 */
	protected String backgroundColor;

	/**
	 * The protected String for the currently selected drawing Node Settings.
	 */
	protected String drawingNodeSettings;
	/**
	 * The protected String for the currently selected drawing Key Settings.
	 */
	protected String drawingKeySettings;

	/**
	 * The protected String for the currently selected insertion Left Node Settings.
	 */
	protected String insertNodeLeftSettings;
	/**
	 * The protected String for the currently selected insertion Right Node Settings.
	 */
	protected String insertNodeRightSettings;
	/**
	 * The protected String for the currently selected insertion Animator Node Settings.
	 */
	protected String insertAnimatorNodeSettings;
	/**
	 * The protected String for the currently selected insertion Animator Key Settings.
	 */
	protected String insertAnimatorKeySettings;

	/**
	 * The protected String for the currently selected searching Left Node Settings.
	 */
	protected String searchNodeLeftSettings;
	/**
	 * The protected String for the currently selected searching Right Node Settings.
	 */
	protected String searchNodeRightSettings;
	/**
	 * The protected String for the currently selected searching Animator Node Settings.
	 */
	protected String searchAnimatorNodeSettings;
	/**
	 * The protected String for the currently selected searching Animator Key Settings.
	 */
	protected String searchAnimatorKeySettings;

	/**
	 * The protected String for the currently selected select Left Node Settings.
	 */
	protected String selectNodeLeftSettings;
	/**
	 * The protected String for the currently selected select Right Node Settings.
	 */
	protected String selectNodeRightSettings;
	/**
	 * The protected String for the currently selected select Animator Node Settings.
	 */
	protected String selectAnimatorNodeSettings;
	/**
	 * The protected String for the currently selected select Animator Key Settings.
	 */
	protected String selectAnimatorKeySettings;

	/**
	 * The protected String for the currently selected rotated Child Node Settings.
	 */
	protected String rotateChildNodeSettings;
	/**
	 * The protected String for the currently selected rotated Root Node Settings.
	 */
	protected String rotateRootNodeSettings;
	/**
	 * The protected String for the currently selected rotated Descendant Node Settings.
	 */
	protected String rotateDescendantNodeSettings;
	/**
	 * The protected String for the currently selected rotated Original Node Settings.
	 */
	protected String rotateOriginalNodeSettings;
	/**
	 * The protected String for the currently selected rotated Animator Key Settings.
	 */
	protected String rotateAnimatorKeySettings;
	/**
	 * The protected String for the currently selected rotated Original Key Settings.
	 */
	protected String rotateOriginalKeySettings;

	/**
	 * The protected String for the currently selected left line paint.
	 */
	protected String deleteLeftLinePaintSettings;
	/**
	 * The protected String for the currently selected right line paint.
	 */
	protected String deleteRightLinePaintSettings;

	/**
	 * The protected String for the currently selected traversal animator Node Settings.
	 */
	protected String traverseAnimatorNodeSettings;
	/**
	 * The protected String for the currently selected traversal animator Key Settings.
	 */
	protected String traverseAnimatorKeySettings;

	/**
	 * The animatingPanel for this JPanel.
	 */
	protected NodeAndLinkAnimatingJPanel<P> animatingPanel;

	/**
	 * The drawingPanel for this JPanel.
	 */
	protected NodeAndLinkDrawingJPanel drawingPanel;

	/**
	 * The variable holding the colorSettings. Set initially to DEFAULT.
	 */
	protected String colorSettings = DEFAULT;

	/**
	 * JComboBox that holds all of the functions available for this panel.
	 */
	protected JComboBox functionJComboBox;

	/**
	 * JComboBox that holds all of the settings available for this current function.
	 */
	protected JComboBox colorSettingsJComboBox;

	/**
	 * JComboBox that holds all of the schemes available for this current settings.
	 */
	protected JComboBox colorSchemesJComboBox;


	/**
	 * JComboBox that holds all of the schemes available for this current settings.
	 */
	protected JComboBox colorPresetOptionsJComboBox;

	/**
	 * The colorOptionsPanel held currently within this panel.
	 */
	protected ColorOptionsJPanel colorOptionsPanel;

	/**
	 * Whether or not to allow rotations through the GUI
	 */
	private boolean allowRotations = false;
	
	/**
	 * Sole Constructor. Simply sets the correct type of tree and sets it to animating.
	 */
    public BSTTreeJPanel() {
		super();
		setTree(new GrowingTreeHead<P>(GrowingTreeNode.ANIMATING_BST_TREE_TYPE));
		setAnimating(true);
    }
    
    public BSTTreeJPanel(P np, IInsertOperator<P> inserter, ISearchOperator<P> searcher,
 			  				IDeleteOperator<P> deleter, boolean allowRotations) {
    	super();
		setTree(new GrowingTreeHead<P>(np, inserter, 
										searcher,
										deleter,this));
		setAnimating(true);
		this.allowRotations = allowRotations;
    }

    public BSTTreeJPanel(P np, IInsertOperator<P> inserter, ISearchOperator<P> searcher,
				IDeleteOperator<P> deleter, IExperimentLogger<P> logger, boolean allowRotations) {
		super();
		setTree(new GrowingTreeHead<P>(np, inserter, 
									  searcher,
									  deleter,this, logger));
		setAnimating(true);
		this.allowRotations = allowRotations;
    }	

	/**
	 * Gets the title for the tree.
	 *
	 * @return String defining the title of the tree.
	 */
	public String getTitle() {
		return ((GrowingTreeNode<P>)getTree()).getTreeTypeString();
	}




	/**
	 * Resets the string settings currently selected. This bascially calls the tree and
	 * gets the settings of the tree and sets all currently selected strings to the
	 * settings of the tree.
	 */
	public void resetSettingStrings() {
		backgroundColor = PaintSettings.getString(getBackground());

		drawingNodeSettings = ((GrowingTreeHead<P>)getTree()).getDrawingNodeSettings().getSettingName();
		drawingKeySettings = ((GrowingTreeHead<P>)getTree()).getDrawingKeySettings().getSettingName();

		insertNodeLeftSettings = ((GrowingTreeHead<P>)getTree()).getInsertNodeLeftSettings().getSettingName();
		insertNodeRightSettings = ((GrowingTreeHead<P>)getTree()).getInsertNodeRightSettings().getSettingName();
		insertAnimatorNodeSettings = ((GrowingTreeHead<P>)getTree()).getInsertAnimatorNodeSettings().getSettingName();
		insertAnimatorKeySettings = ((GrowingTreeHead<P>)getTree()).getInsertAnimatorKeySettings().getSettingName();

		searchNodeLeftSettings = ((GrowingTreeHead<P>)getTree()).getSearchNodeLeftSettings().getSettingName();
		searchNodeRightSettings = ((GrowingTreeHead<P>)getTree()).getSearchNodeRightSettings().getSettingName();
		searchAnimatorNodeSettings = ((GrowingTreeHead<P>)getTree()).getSearchAnimatorNodeSettings().getSettingName();
		searchAnimatorKeySettings = ((GrowingTreeHead<P>)getTree()).getSearchAnimatorKeySettings().getSettingName();

		selectNodeLeftSettings = ((GrowingTreeHead<P>)getTree()).getSelectNodeLeftSettings().getSettingName();
		selectNodeRightSettings = ((GrowingTreeHead<P>)getTree()).getSelectNodeRightSettings().getSettingName();
		selectAnimatorNodeSettings = ((GrowingTreeHead<P>)getTree()).getSelectAnimatorNodeSettings().getSettingName();
		selectAnimatorKeySettings = ((GrowingTreeHead<P>)getTree()).getSelectAnimatorKeySettings().getSettingName();

		rotateChildNodeSettings = ((GrowingTreeHead<P>)getTree()).getRotateChildNodeSettings().getSettingName();
		rotateRootNodeSettings = ((GrowingTreeHead<P>)getTree()).getRotateRootNodeSettings().getSettingName();
		rotateDescendantNodeSettings = ((GrowingTreeHead<P>)getTree()).getRotateDescendantNodeSettings().getSettingName();
		rotateOriginalNodeSettings = ((GrowingTreeHead<P>)getTree()).getRotateOriginalNodeSettings().getSettingName();
		rotateAnimatorKeySettings = ((GrowingTreeHead<P>)getTree()).getRotateAnimatorKeySettings().getSettingName();
		rotateOriginalKeySettings = ((GrowingTreeHead<P>)getTree()).getRotateOriginalKeySettings().getSettingName();

		deleteLeftLinePaintSettings = ((GrowingTreeHead<P>)getTree()).getDeleteLeftLinePaintSettings().getSettingName();
		deleteRightLinePaintSettings = ((GrowingTreeHead<P>)getTree()).getDeleteRightLinePaintSettings().getSettingName();

		traverseAnimatorNodeSettings = ((GrowingTreeHead<P>)getTree()).getTraverseAnimatorNodeSettings().getSettingName();
		traverseAnimatorKeySettings = ((GrowingTreeHead<P>)getTree()).getTraverseAnimatorKeySettings().getSettingName();
	}


	/**
	 * Sets the settings for the panel using the currently selected String options.
	 * <p> The Panel used is the current panel.
	 *
	 */
	public void setSettings() {
		setSettings(this);
	}

	/**
	 * Sets the settings for the panel using the currently selected String options.
	 * <p> The Panel used is the passed panel.
	 *
	 * @param panel panel to which the settings are changed according to the currently
	 * selected settings.
	 */
	public void setSettings(TreeJPanel panel) {

		panel.setBackground(PaintSettings.getColor(backgroundColor));

		((GrowingTreeHead<P>)panel.getTree()).setDrawingNodeSettings(NodeSettings.getScheme(drawingNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setDrawingKeySettings(KeySettings.getScheme(drawingKeySettings));


		((GrowingTreeHead<P>)panel.getTree()).setTreeSettings(NodeSettings.getScheme(drawingNodeSettings), KeySettings.getScheme(drawingKeySettings));

		((GrowingTreeHead<P>)panel.getTree()).setInsertNodeLeftSettings(NodeSettings.getScheme(insertNodeLeftSettings));
		((GrowingTreeHead<P>)panel.getTree()).setInsertNodeRightSettings(NodeSettings.getScheme(insertNodeRightSettings));
		((GrowingTreeHead<P>)panel.getTree()).setInsertAnimatorNodeSettings(NodeSettings.getScheme(insertAnimatorNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setInsertAnimatorKeySettings(KeySettings.getScheme(insertAnimatorKeySettings));

		((GrowingTreeHead<P>)panel.getTree()).setSearchNodeLeftSettings(NodeSettings.getScheme(searchNodeLeftSettings));
		((GrowingTreeHead<P>)panel.getTree()).setSearchNodeRightSettings(NodeSettings.getScheme(searchNodeRightSettings));
		((GrowingTreeHead<P>)panel.getTree()).setSearchAnimatorNodeSettings(NodeSettings.getScheme(searchAnimatorNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setSearchAnimatorKeySettings(KeySettings.getScheme(searchAnimatorKeySettings));

		((GrowingTreeHead<P>)panel.getTree()).setSelectNodeLeftSettings(NodeSettings.getScheme(selectNodeLeftSettings));
		((GrowingTreeHead<P>)panel.getTree()).setSelectNodeRightSettings(NodeSettings.getScheme(selectNodeRightSettings));
		((GrowingTreeHead<P>)panel.getTree()).setSelectAnimatorNodeSettings(NodeSettings.getScheme(selectAnimatorNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setSelectAnimatorKeySettings(KeySettings.getScheme(selectAnimatorKeySettings));

		((GrowingTreeHead<P>)panel.getTree()).setRotateChildNodeSettings(NodeSettings.getScheme(rotateChildNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setRotateRootNodeSettings(NodeSettings.getScheme(rotateRootNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setRotateDescendantNodeSettings(NodeSettings.getScheme(rotateDescendantNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setRotateOriginalNodeSettings(NodeSettings.getScheme(rotateOriginalNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setRotateAnimatorKeySettings(KeySettings.getScheme(rotateAnimatorKeySettings));
		((GrowingTreeHead<P>)panel.getTree()).setRotateOriginalKeySettings(KeySettings.getScheme(rotateOriginalKeySettings));


		((GrowingTreeHead<P>)panel.getTree()).setDeleteLeftLinePaintSettings(PaintSettings.getScheme(deleteLeftLinePaintSettings));
		((GrowingTreeHead<P>)panel.getTree()).setDeleteLeftLinePaintSettings(PaintSettings.getScheme(deleteRightLinePaintSettings));

		((GrowingTreeHead<P>)panel.getTree()).setTraverseAnimatorNodeSettings(NodeSettings.getScheme(traverseAnimatorNodeSettings));
		((GrowingTreeHead<P>)panel.getTree()).setTraverseAnimatorKeySettings(KeySettings.getScheme(traverseAnimatorKeySettings));

		panel.setDrawTree(true);
		panel.repaint();

	}



	/**
	 * Sets the color settings string of the current panel to the colorSettings param.
	 *
	 * @param colorSettings the String defining the colorSettings for the current panel.
	 */
	public void setColorSettingsString(String colorSettings) {
		this.colorSettings = colorSettings;
	}

	/**
	 * Gets the color settings string of the current panel to the colorSettings param.
	 *
	 * @return String colorSettings String defining the colorSettings for the current panel.
	 */
	public String getColorSettingsString() {
		return colorSettings;
	}



	/**
	 * Draws the drawing node using the currently selected drawing settings.
	 */
	public void drawDrawingNode() {
		NodeAndLinkDrawingJPanel drawingPanel = colorOptionsPanel.getDrawingPanel();

		drawingPanel.setBackground(PaintSettings.getColor(backgroundColor));

		drawingPanel.getNode().setSettings(NodeSettings.getScheme(drawingNodeSettings));
		((DrawableKey)drawingPanel.getNode().getValue()).setSettings(KeySettings.getScheme(drawingKeySettings));


		drawingPanel.setDrawTree(true);
		drawingPanel.repaint();
	}

	/**
	 * Draws the animating node using the currently selected animating settings.
	 */
	public void drawAnimatingNode() {

		NodeAndLinkAnimatingJPanel animatingPanel = colorOptionsPanel.getAnimatingPanel();

		if (!((String)functionJComboBox.getSelectedItem()).equals(DELETE)) {
			animatingPanel.setDrawingLines(false);
		}
		else {
			animatingPanel.setDrawingLines(true);
		}

		animatingPanel.setBackground(PaintSettings.getColor(backgroundColor));

		((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(drawingKeySettings));

		if (((String)functionJComboBox.getSelectedItem()).equals(SEARCH)) {

			((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(searchAnimatorKeySettings));

			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_LEFT_SETTINGS)) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(searchNodeLeftSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_RIGHT_SETTINGS)) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(searchNodeRightSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_ANIMATOR_SETTINGS)) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(searchAnimatorNodeSettings));
			}

		}
		else if (((String)functionJComboBox.getSelectedItem()).equals(SELECT)) {

			((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(selectAnimatorKeySettings));

			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_LEFT_SETTINGS)) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(selectNodeLeftSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_RIGHT_SETTINGS)) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(selectNodeRightSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_ANIMATOR_SETTINGS)) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(selectAnimatorNodeSettings));
			}

		}
		else if (((String)functionJComboBox.getSelectedItem()).equals(ROTATE)) {

			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_ROOT_SETTINGS)) {
				((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateAnimatorKeySettings));

				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(rotateChildNodeSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_CHILD_SETTINGS)) {
				((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateAnimatorKeySettings));

				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(rotateRootNodeSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_DESCENDANT_SETTINGS)) {
				((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateAnimatorKeySettings));

				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(rotateDescendantNodeSettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_ORIGINAL_SETTINGS)) {
				((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateOriginalKeySettings));

				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(rotateOriginalKeySettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(KEY_ANIMATOR_SETTINGS)) {
				((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateAnimatorKeySettings));
			}
			if (((String)colorSettingsJComboBox.getSelectedItem()).equals(KEY_ORIGINAL_SETTINGS)) {
				((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateOriginalKeySettings));
			}

		}
		else if (((String)functionJComboBox.getSelectedItem()).equals(DELETE)) {
			((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(rotateAnimatorKeySettings));

			animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(rotateRootNodeSettings));

			animatingPanel.setRightLinePaint(PaintSettings.getColor(deleteRightLinePaintSettings));
			animatingPanel.setLeftLinePaint(PaintSettings.getColor(deleteLeftLinePaintSettings));

		}
		else if (((String)functionJComboBox.getSelectedItem()).equals(TRAVERSE)) {

			((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(traverseAnimatorKeySettings));

			animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(traverseAnimatorNodeSettings));

		}
		else { //Insert and default animating node

			((DrawableKey)animatingPanel.getAnimatingNode().getValue()).setSettings(KeySettings.getScheme(insertAnimatorKeySettings));

			if (colorSettingsJComboBox.getSelectedItem() == null) {
				animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(insertNodeLeftSettings));
			}
			else {
				if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_RIGHT_SETTINGS)) {
					animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(insertNodeRightSettings));
				}
				else if (((String)colorSettingsJComboBox.getSelectedItem()).equals(NODE_ANIMATOR_SETTINGS)) {
					animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(insertAnimatorNodeSettings));
				}

				else { // Left settings and default
					animatingPanel.getAnimatingNode().setSettings(NodeSettings.getScheme(insertNodeLeftSettings));
				}
			}
		}
		animatingPanel.setDrawTree(true);
		animatingPanel.repaint();

	}

	/**
	 * Gets the Color Scheme setting for the given function and setting.
	 *
	 * @param function String function like DRAWING or BACKGROUND.
	 * @param setting String setting choice for which the color setting is requested like NODE.
	 *
	 * @return String defining the color scheme setting.
	 */
	public String getColorSchemeSetting(String function, String setting) {


		if (function.equals(DRAWING)) {
			if (setting.equals(NODE)) {
				return drawingNodeSettings;
			}
			else {
				return drawingKeySettings;
			}
		}
		if (function.equals(BACKGROUND)) {
			return backgroundColor;
		}
		if (function.equals(INSERT)) {
			if (setting.equals(NODE_LEFT_SETTINGS)) {
				return insertNodeLeftSettings;
			}
			if (setting.equals(NODE_RIGHT_SETTINGS)) {
				return insertNodeRightSettings;
			}
			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				return insertAnimatorNodeSettings;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				return insertAnimatorKeySettings;
			}
		}

		if (function.equals(SEARCH)) {
			if (setting.equals(NODE_LEFT_SETTINGS)) {
				return searchNodeLeftSettings;
			}
			if (setting.equals(NODE_RIGHT_SETTINGS)) {
				return searchNodeRightSettings;
			}
			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				return searchAnimatorNodeSettings;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				return searchAnimatorKeySettings;
			}
		}
		if (function.equals(SELECT)) {
			if (setting.equals(NODE_LEFT_SETTINGS)) {
				return selectNodeLeftSettings;
			}
			if (setting.equals(NODE_RIGHT_SETTINGS)) {
				return selectNodeRightSettings;
			}
			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				return selectAnimatorNodeSettings;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				return selectAnimatorKeySettings;
			}
		}
		if (function.equals(ROTATE)) {

			if (setting.equals(NODE_ROOT_SETTINGS)) {
				return rotateChildNodeSettings;
			}
			if (setting.equals(NODE_CHILD_SETTINGS)) {
				return rotateRootNodeSettings;
			}
			if (setting.equals(NODE_DESCENDANT_SETTINGS)) {
				return rotateDescendantNodeSettings;
			}
			if (setting.equals(NODE_ORIGINAL_SETTINGS)) {
				return rotateOriginalNodeSettings;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				return rotateAnimatorKeySettings;
			}
			if (setting.equals(KEY_ORIGINAL_SETTINGS)) {
				return rotateOriginalKeySettings;
			}

		}
		if (function.equals(DELETE)) {

			if (setting.equals(LEFT_PAINT)) {
				return deleteLeftLinePaintSettings;
			}
			if (setting.equals(RIGHT_PAINT)) {
				return deleteRightLinePaintSettings;
			}

		}
		if (function.equals(TRAVERSE)) {

			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				return traverseAnimatorNodeSettings;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				return traverseAnimatorKeySettings;
			}

		}

		return null;


	}

	/**
	 * Sets the color scheme using the given function and setting.
	 *
	 * @param function for which the setting is given.
	 * @param setting the setting for which the scheme is set.
	 * @param scheme the color setting used for the given function and setting.
	 */
	public void setColorSchemeSetting(String function, String setting, String scheme) {
		if (function.equals(BACKGROUND)) {
			backgroundColor = scheme;
		}
		if (function.equals(DRAWING)) {
			if (setting.equals(NODE)) {
				drawingNodeSettings = scheme;
			}
			else {
				drawingKeySettings = scheme;
			}
		}
		if (function.equals(INSERT)) {
			if (setting.equals(NODE_LEFT_SETTINGS)) {
				insertNodeLeftSettings = scheme;
			}
			if (setting.equals(NODE_RIGHT_SETTINGS)) {
				insertNodeRightSettings = scheme;
			}
			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				insertAnimatorNodeSettings = scheme;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				insertAnimatorKeySettings = scheme;
			}
		}

		if (function.equals(SEARCH)) {
			if (setting.equals(NODE_LEFT_SETTINGS)) {
				searchNodeLeftSettings = scheme;
			}
			if (setting.equals(NODE_RIGHT_SETTINGS)) {
				searchNodeRightSettings = scheme;
			}
			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				searchAnimatorNodeSettings = scheme;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				searchAnimatorKeySettings = scheme;
			}
		}
		if (function.equals(SELECT)) {
			if (setting.equals(NODE_LEFT_SETTINGS)) {
				selectNodeLeftSettings = scheme;
			}
			if (setting.equals(NODE_RIGHT_SETTINGS)) {
				selectNodeRightSettings = scheme;
			}
			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				selectAnimatorNodeSettings = scheme;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				selectAnimatorKeySettings = scheme;
			}
		}
		if (function.equals(ROTATE)) {

			if (setting.equals(NODE_ROOT_SETTINGS)) {
				rotateChildNodeSettings = scheme;
			}
			if (setting.equals(NODE_CHILD_SETTINGS)) {
				rotateRootNodeSettings = scheme ;
			}
			if (setting.equals(NODE_DESCENDANT_SETTINGS)) {
				rotateDescendantNodeSettings = scheme;
			}
			if (setting.equals(NODE_ORIGINAL_SETTINGS)) {
				rotateOriginalNodeSettings = scheme;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				rotateAnimatorKeySettings = scheme;
			}
			if (setting.equals(KEY_ORIGINAL_SETTINGS)) {
				rotateOriginalKeySettings = scheme;
			}

		}
		if (function.equals(DELETE)) {

			if (setting.equals(LEFT_PAINT)) {
				deleteLeftLinePaintSettings = scheme;
			}
			if (setting.equals(RIGHT_PAINT)) {
				deleteRightLinePaintSettings = scheme;
			}

		}
		if (function.equals(TRAVERSE)) {

			if (setting.equals(NODE_ANIMATOR_SETTINGS)) {
				traverseAnimatorNodeSettings = scheme;
			}
			if (setting.equals(KEY_ANIMATOR_SETTINGS)) {
				traverseAnimatorKeySettings = scheme;
			}

		}



	}


	/**
	 * Sets the color scheme using the given preset scheme string.
	 *
	 * @param presetScheme the preset sceme defined.
	 */
	public void setColorSchemeSetting(String presetScheme) {

		setColorSettingsString(presetScheme);

		if (presetScheme.equals(DEFAULT)) {

			backgroundColor = PaintSettings.WHITE;

			drawingNodeSettings = NodeSettings.DEFAULT_SCHEME;

			drawingKeySettings = KeySettings.DEFAULT_SCHEME;

			insertNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_1;

			insertNodeRightSettings = NodeSettings.ANIMATION_SCHEME_1;

			insertAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_1;

			insertAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_1;

			searchNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_1;

			searchNodeRightSettings = NodeSettings.ANIMATION_SCHEME_1;

			searchAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_1;

			searchAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_1;

			selectNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_1;

			selectNodeRightSettings = NodeSettings.ANIMATION_SCHEME_1;

			selectAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_1;

			selectAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_1;

			rotateChildNodeSettings = NodeSettings.ROTATION_SCHEME_1;

			rotateRootNodeSettings = NodeSettings.ROTATION_SCHEME_1 ;

			rotateDescendantNodeSettings = NodeSettings.ROTATION_SCHEME_1;

			rotateOriginalNodeSettings = NodeSettings.DEFAULT_SCHEME;

			rotateAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_1;

			rotateOriginalKeySettings = KeySettings.DEFAULT_SCHEME;

			deleteLeftLinePaintSettings = PaintSettings.RED;

			deleteRightLinePaintSettings = PaintSettings.BLUE;

			traverseAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_1;

			traverseAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_1;
		}
		if (presetScheme.equals(PRIMARY)) {

			backgroundColor = PaintSettings.BLACK;

			drawingNodeSettings = NodeSettings.DEFAULT_SCHEME_2;

			drawingKeySettings = KeySettings.DEFAULT_SCHEME_2;

			insertNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_5;

			insertNodeRightSettings = NodeSettings.ANIMATION_SCHEME_5;

			insertAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_5;

			insertAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			searchNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_5;

			searchNodeRightSettings = NodeSettings.ANIMATION_SCHEME_5;

			searchAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_5;

			searchAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			selectNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_5;

			selectNodeRightSettings = NodeSettings.ANIMATION_SCHEME_5;

			selectAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_5;

			selectAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			rotateChildNodeSettings = NodeSettings.ROTATION_SCHEME_4;

			rotateRootNodeSettings = NodeSettings.ROTATION_SCHEME_4;

			rotateDescendantNodeSettings = NodeSettings.ROTATION_SCHEME_4;

			rotateOriginalNodeSettings = NodeSettings.DEFAULT_SCHEME_2;

			rotateAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			rotateOriginalKeySettings = KeySettings.DEFAULT_SCHEME_2;

			deleteLeftLinePaintSettings = PaintSettings.RED;

			deleteRightLinePaintSettings = PaintSettings.BLUE;

			traverseAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_5;

			traverseAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;
		}

		if (presetScheme.equals(PRINCETON)) {

			backgroundColor = PaintSettings.BLACK;

			drawingNodeSettings = NodeSettings.PRINCETON_SCHEME;

			drawingKeySettings = KeySettings.ANIMATOR_SCHEME_1;

			insertNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_2;

			insertNodeRightSettings = NodeSettings.ANIMATION_SCHEME_2;

			insertAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_2;

			insertAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_2;

			searchNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_2;

			searchNodeRightSettings = NodeSettings.ANIMATION_SCHEME_2;

			searchAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_2;

			searchAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_2;

			selectNodeLeftSettings = NodeSettings.ANIMATION_SCHEME_2;

			selectNodeRightSettings = NodeSettings.ANIMATION_SCHEME_2;

			selectAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_2;

			selectAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_2;

			rotateChildNodeSettings = NodeSettings.ROTATION_SCHEME_1;

			rotateRootNodeSettings = NodeSettings.ROTATION_SCHEME_1 ;

			rotateDescendantNodeSettings = NodeSettings.ROTATION_SCHEME_1;

			rotateOriginalNodeSettings = NodeSettings.PRINCETON_SCHEME;

			rotateAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_2;

			rotateOriginalKeySettings = KeySettings.ANIMATOR_SCHEME_1;

			deleteLeftLinePaintSettings = PaintSettings.PRINCETON_ORANGE;

			deleteRightLinePaintSettings = PaintSettings.PRINCETON_ORANGE;

			traverseAnimatorNodeSettings = NodeSettings.ANIMATOR_SCHEME_1;

			traverseAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_1;
		}

		if (presetScheme.equals(CHRISTMAS)) {

			backgroundColor = PaintSettings.DARK_GREEN;

			drawingNodeSettings = NodeSettings.CHRISTMAS_SCHEME;

			drawingKeySettings = KeySettings.CHRISTMAS_SCHEME;

			insertNodeLeftSettings = NodeSettings.TREE_SCHEME;

			insertNodeRightSettings = NodeSettings.TREE_SCHEME;

			insertAnimatorNodeSettings = NodeSettings.TREE_SCHEME;

			insertAnimatorKeySettings = KeySettings.CHRISTMAS_SCHEME;

			searchNodeLeftSettings = NodeSettings.TREE_SCHEME;

			searchNodeRightSettings = NodeSettings.TREE_SCHEME;

			searchAnimatorNodeSettings = NodeSettings.TREE_SCHEME;

			searchAnimatorKeySettings = KeySettings.CHRISTMAS_SCHEME;

			selectNodeLeftSettings = NodeSettings.TREE_SCHEME;

			selectNodeRightSettings = NodeSettings.TREE_SCHEME;

			selectAnimatorNodeSettings = NodeSettings.TREE_SCHEME;

			selectAnimatorKeySettings = KeySettings.CHRISTMAS_SCHEME;

			rotateChildNodeSettings = NodeSettings.TREE_SCHEME;

			rotateRootNodeSettings = NodeSettings.TREE_SCHEME ;

			rotateDescendantNodeSettings = NodeSettings.TREE_SCHEME;

			rotateOriginalNodeSettings = NodeSettings.CHRISTMAS_SCHEME;

			rotateAnimatorKeySettings = KeySettings.CHRISTMAS_SCHEME;

			rotateOriginalKeySettings = KeySettings.CHRISTMAS_SCHEME;

			deleteLeftLinePaintSettings = PaintSettings.RED;

			deleteRightLinePaintSettings = PaintSettings.GREEN;

			traverseAnimatorNodeSettings = NodeSettings.TREE_SCHEME;

			traverseAnimatorKeySettings = KeySettings.CHRISTMAS_SCHEME;
		}

		if (presetScheme.equals(HALLOWEEN)) {

			backgroundColor = PaintSettings.BLACK;

			drawingNodeSettings = NodeSettings.HALLOWEEN_SCHEME;

			drawingKeySettings = KeySettings.GHOST_SCHEME;

			insertNodeLeftSettings = NodeSettings.GHOST_SCHEME;

			insertNodeRightSettings = NodeSettings.GHOST_SCHEME;

			insertAnimatorNodeSettings = NodeSettings.GHOST_SCHEME;

			insertAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			searchNodeLeftSettings = NodeSettings.GHOST_SCHEME;

			searchNodeRightSettings = NodeSettings.GHOST_SCHEME;

			searchAnimatorNodeSettings = NodeSettings.GHOST_SCHEME;

			searchAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			selectNodeLeftSettings = NodeSettings.GHOST_SCHEME;

			selectNodeRightSettings = NodeSettings.GHOST_SCHEME;

			selectAnimatorNodeSettings = NodeSettings.GHOST_SCHEME;

			selectAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			rotateChildNodeSettings = NodeSettings.GHOST_SCHEME;

			rotateRootNodeSettings = NodeSettings.GHOST_SCHEME ;

			rotateDescendantNodeSettings = NodeSettings.GHOST_SCHEME;

			rotateOriginalNodeSettings = NodeSettings.HALLOWEEN_SCHEME;

			rotateAnimatorKeySettings = KeySettings.ANIMATOR_SCHEME_4;

			rotateOriginalKeySettings = KeySettings.GHOST_SCHEME;

			deleteLeftLinePaintSettings = PaintSettings.ORANGE;

			deleteRightLinePaintSettings = PaintSettings.ORANGE;

			traverseAnimatorNodeSettings = NodeSettings.GHOST_SCHEME;

			traverseAnimatorKeySettings = KeySettings.GHOST_SCHEME;

		}
	}

	/**
	 * Gets the color scheme aniamtion options for the current tree.
	 *
	 * @return String array of the options for the current tree.
	 */
	public String[] getColorSchemeOptions() {
		String[] options = {DRAWING,BACKGROUND, INSERT, SEARCH, SELECT, ROTATE, DELETE, TRAVERSE};
		return options;
	}

	/**
	 * Gets the color settings options for the current tree.
	 *
	 * @return String array of the color settings.
	 */
	public String[] getColorSettings() {
		String[] options = {DEFAULT, PRIMARY, PRINCETON,CHRISTMAS, HALLOWEEN};
		return options;
	}

	/**
	 * Gets the color scheme options for each type of animation.
	 *
	 * @return String array of the options for the current tree.
	 */
	public String[] getOptionColorSchemeOptions(String option) {

		if (option.equals(DRAWING)) {
			String[] options = {NODE, KEY};
			return options;
		}
		if (option.equals(BACKGROUND)) {
			String[] options = {};
			return options;
		}

		if (option.equals(INSERT)) {
			String[] options = {NODE_LEFT_SETTINGS, NODE_RIGHT_SETTINGS, NODE_ANIMATOR_SETTINGS, KEY_ANIMATOR_SETTINGS};
			return options;
		}
		if (option.equals(SEARCH)) {
			String[] options = {NODE_LEFT_SETTINGS, NODE_RIGHT_SETTINGS, NODE_ANIMATOR_SETTINGS, KEY_ANIMATOR_SETTINGS};
			return options;
		}
		if (option.equals(SELECT)) {
			String[] options = {NODE_LEFT_SETTINGS, NODE_RIGHT_SETTINGS, NODE_ANIMATOR_SETTINGS, KEY_ANIMATOR_SETTINGS};
			return options;
		}
		if (option.equals(ROTATE)) {
			String[] options = {NODE_ROOT_SETTINGS, NODE_CHILD_SETTINGS, NODE_DESCENDANT_SETTINGS, NODE_ORIGINAL_SETTINGS, KEY_ANIMATOR_SETTINGS, KEY_ORIGINAL_SETTINGS};
			return options;
		}
		if (option.equals(DELETE)) {
			String[] options = {LEFT_PAINT, RIGHT_PAINT};
			return options;
		}
		if (option.equals(TRAVERSE)) {
			String[] options = {NODE_ANIMATOR_SETTINGS, KEY_ANIMATOR_SETTINGS};
			return options;
		}


		return null;

	}

	/**
	 * Gets the color scheme settings for each type of settings.
	 *
	 * @return String array of the options for the current type of settings.
	 */
	public String[] getColorSchemesList(String settings) {


		if (settings.startsWith(NODE)) {
			return NodeSettings.getList();
		}
		if (settings.startsWith(KEY)) {
			return KeySettings.getList();
		}
		if (settings.startsWith(PAINT)) {
			return PaintSettings.getList();
		}

		String[] returnString = {" "};
		return returnString;

	}


	/**
	 * Constructs the color options combo box and sets the box for the given colorOptionsPanel.
	 *
	 * @param colorOptionsPanel the colorToolsJPanel for which the JComboBox is set.
	 */
	public void constructColorOptions(ColorOptionsJPanel colorOptionsPanel) {
		this.colorOptionsPanel = colorOptionsPanel;
		resetSettingStrings();

		functionJComboBox = new JComboBox(getColorSchemeOptions());

		functionJComboBox.addActionListener(this);
		functionJComboBox.setActionCommand(OptionEvent.FUNCTION_CHANGE);
		//functionJComboBox.setBackground(Color.white);

		functionJComboBox.setSelectedIndex(0);

		colorOptionsPanel.setFunctionJComboBox(functionJComboBox);
	}

	/**
	 * Constructs the color settings combo box and sets the box for the given colorOptionsPanel.
	 *
	 * @param colorOptionsPanel the colorOptionsPanel for which the JComboBox is set.
	 */
	public void constructPresetColorOptions(ColorOptionsJPanel colorOptionsPanel) {
		this.colorOptionsPanel = colorOptionsPanel;
		resetSettingStrings();

		colorPresetOptionsJComboBox = new JComboBox(getColorSettings());

		colorPresetOptionsJComboBox.addActionListener(this);
		colorPresetOptionsJComboBox.setActionCommand(OptionEvent.COLOR_SCHEME_PRESET_CHANGE);
		//colorPresetOptionsJComboBox.setBackground(Color.white);

		colorPresetOptionsJComboBox.setSelectedItem(getColorSettingsString());

		colorOptionsPanel.setPresetColorOptionsJComboBox(colorPresetOptionsJComboBox);
	}


	/**
	 * Constructs a popupmenu, using the actionlistener passed. The popupMenu
	 * contains all of the options available through the current tree. For this
	 * class, no items are added, making it necessary to overide the method.
	 *
	 * @param actionListener the listener add to the actions of all the items made in the menu.
	 *
	 * @return JPopupMenu which is the menu constructed within the panel.
	 */
	public JPopupMenu makeJPopupMenu(ActionListener actionlistener) {
		JPopupMenu popupMenu = new JPopupMenu();
		if (allowRotations) {
	        JMenuItem rotateItem = new JMenuItem("Rotate Up");
	        rotateItem.addActionListener(actionlistener);
	        rotateItem.setActionCommand(OptionEvent.ROTATE_CLICK);
	        popupMenu.add(rotateItem);
		}
        /*JMenuItem deleteItem = new JMenuItem("Delete Node");
		deleteItem.addActionListener(actionlistener);
        deleteItem.setActionCommand(OptionEvent.DELETE_CLICK);
        JMenuItem rotateTopItem = new JMenuItem("Rotate to Top");
		rotateTopItem.addActionListener(actionlistener);
        rotateTopItem.setActionCommand(OptionEvent.ROTATE_TOP_CLICK);
        JMenuItem rotateItemDouble = new JMenuItem("Double Rotation");
		rotateItemDouble.addActionListener(actionlistener);
        rotateItemDouble.setActionCommand(OptionEvent.ROTATE_DOUBLE_CLICK);
        JMenuItem splayItem = new JMenuItem("Splay");
		splayItem.addActionListener(actionlistener);
        splayItem.setActionCommand(OptionEvent.SPLAY_CLICK);
        JMenuItem partitionItem = new JMenuItem("Partition");
		partitionItem.addActionListener(actionlistener);
        partitionItem.setActionCommand(OptionEvent.PARTITION_CLICK);*/
        JMenuItem selectItem = new JMenuItem("Select");
		selectItem.addActionListener(actionlistener);
        selectItem.setActionCommand(OptionEvent.SELECT_CLICK);
        /*JMenuItem balanceItem = new JMenuItem("Balance");
		balanceItem.addActionListener(actionlistener);
        balanceItem.setActionCommand(OptionEvent.BALANCE_CLICK);*/

        
        /*popupMenu.add(deleteItem);
        popupMenu.add(rotateTopItem);
        popupMenu.add(rotateItemDouble);
        popupMenu.add(splayItem);
        popupMenu.add(partitionItem);*/
        popupMenu.add(selectItem);
        //popupMenu.add(balanceItem);

        return popupMenu;
	}

	/**
	 * Sets whether the tree is animating or not.
	 *
	 * @param animating boolean flag as to whether the tree is animating.
	 */
	public void setAnimating(boolean animating) {
		if (animating && isComponentShown()) {
			((GrowingTreeNode)getTree()).setTreeType(GrowingTreeNode.ANIMATING_BST_TREE_TYPE);
		}
		else if (isComponentShown()) {
			((GrowingTreeNode)getTree()).setTreeType(GrowingTreeNode.DRAWING_BST_TREE_TYPE);
		}
		super.setAnimating(animating);
	}

	/**
	 * Sets whether the tree is shown or not.
	 *
	 * @param componentShown boolean flag as to whether the tree is shown.
	 */
	public void setComponentShown(boolean componentShown) {
		if (!componentShown) {
			((GrowingTreeNode)getTree()).setTreeType(GrowingTreeNode.DRAWING_BST_TREE_TYPE);
		}
		else {
			if (isAnimating()) {
				((GrowingTreeNode<P>)getTree()).setTreeType(GrowingTreeNode.ANIMATING_BST_TREE_TYPE);
			}
			else {
				((GrowingTreeNode<P>)getTree()).setTreeType(GrowingTreeNode.DRAWING_BST_TREE_TYPE);
			}
		}
		((GrowingTreeHead<P>)getTree()).AnimateTree((Graphics2D)getGraphics());

		super.setComponentShown(componentShown);
	}


	/**
	 * Rotates up the given node.
	 *
	 * @param node the node to rotate up.
	 */
	protected void rotateUpNode(GrowingTreeNode<P> node) {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		if (node == null)
			return;
		GrowingTreeNode<P> parent = node.getParentTree();
		if (parent == null || parent == node.getHead()) return;
		if (parent.getLeftNodeInternal() == node) {
			// rotate right
			parent.getShadowNode().rotateRight();
		}
		else if (parent.getRightNodeInternal() == node){
			// rotate left
			parent.getShadowNode().rotateLeft();
		}

		setDrawTree(true);
		repaint();
	}

	/**
	 * Rotates up the given node to the top.
	 *
	 * @param node the node to rotate up.
	 */
	protected void rotateToTopNode(GrowingTreeNode<P> node) {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		if (node == null)
			return;

		((GrowingTreeHead<P>)getTree()).rotateToTop(node);

		setDrawTree(true);
		repaint();
	}

	/**
	 * Doubly Rotates up the given node.
	 *
	 * @param node the node to rotate up.
	 */
	protected void rotateUpDoubleNode(GrowingTreeNode<P> node) {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		if (node == null)
			return;

		((GrowingTreeHead<P>)getTree()).rotateUpDouble(node);

		setDrawTree(true);
		repaint();
	}

	/**
	 * Splays up the given node.
	 *
	 * @param node the node to rotate up.
	 */
	protected void splayNode(GrowingTreeNode<P> node) {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		if (node == null)
			return;

		((GrowingTreeHead<P>)getTree()).splay(node);

		setDrawTree(true);
		repaint();
	}

	/**
	 * Balances the entire tree.
	 */
	protected void balanceTree() {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		((GrowingTreeHead<P>)getTree()).balanceTree();

		setDrawTree(true);
		repaint();
	}

	/**
	 * Balances the given node.
	 *
	 * @param node the node to balance.
	 */
	protected void balanceNode(GrowingTreeNode<P> node) {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		if (node == null)
			return;

		((GrowingTreeHead<P>)getTree()).balance(node);

		setDrawTree(true);
		repaint();
	}



	/**
	 * Changes the display.
	 */
	protected void changeDisplay() {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		((GrowingTreeHead<P>)getTree()).changeDisplay();

		setDrawTree(true);
		repaint();
	}


    public void setSubtreeCountsCommand(boolean visible)
    {
        ((GrowingTreeHead<P>)getTree()).setSubtreeCountsVisible(visible);
        super.setSubtreeCountsCommand(visible);
    }

	/**
	 * RotateUp command.
	 *
	 * @param node Tree to rotateUp.
	 */
	public void rotateUpCommand(Tree<P> node) {
		rotateUpNode((GrowingTreeNode<P>)node);
	}

	/**
	 * RotateToTop command.
	 *
	 * @param node Tree to rotateToTop.
	 */
	public void rotateToTopCommand(Tree<P> node) {
		rotateToTopNode((GrowingTreeNode<P>)node);
	}

	/**
	 * RotateUpDouble command.
	 *
	 * @param node Tree to rotateUpDouble.
	 */
	public void rotateUpDoubleCommand(Tree node) {
		rotateUpDoubleNode((GrowingTreeNode<P>)node);
	}

	/**
	 * Splay command.
	 *
	 * @param node Tree to splay.
	 */
	public void splayCommand(Tree node) {
		splayNode((GrowingTreeNode<P>)node);
	}

	/**
	 * Traverse command.
	 *
	 * @param traverseType int that defines the traverseType.
	 */
	 public void traverseCommand(int traverseType) {
		// Set initial status.
		//getTree().setTreeStatus(Animation.PLAY);

		((GrowingTreeHead)getTree()).traverse(traverseType);
		setDrawTree(true);
		repaint();
	 }



	/**
	 * Balance command.
	 *
	 * @param node Tree to balance.
	 */
	public void balanceCommand(Tree node) {
		if (node == null) {
			balanceTree();
		}
		else {
			balanceNode((GrowingTreeNode<P>)node);
		}
	}

	/**
	 * Change Display Command.
	 */
	public void displayChangeCommand() {
		changeDisplay();
	}

	/**
	 * Recieved an event from an OptionJPanel with one of the given commands. The Panel
	 * responds accordingly. The Methods it calls (for example: insertCommand, rotateCommand...)
	 * should be overiden in extending classes to functionable.
	 *
	 * @param e OptionEvent recieved from the event performed.
	 */
	public void optionEventPerformed(OptionEvent e) {
		
        if (e.getActionCommand().equals(OptionEvent.RESET)) {
			resetSettingStrings();
			functionJComboBox.setSelectedIndex(0);
			colorPresetOptionsJComboBox.setSelectedIndex(0);
		}
		else if (e.getActionCommand().equals(OptionEvent.SAVE_TREE)) {
			setSettings();
		}
		else if (e.getActionCommand().equals(OptionEvent.COLOR_SETTINGS)) {

		}
		else if (e.getActionCommand().equals(OptionEvent.BINARY_DISPLAY)) {
			if ( ((GrowingTreeHead)getTree()).getDisplay() != GrowingTreeHead.BINARY_DISPLAY) {
				displayChangeCommand();
			}
		}
		else if (e.getActionCommand().equals(OptionEvent.SECTIONAL_DISPLAY)) {
			if ( ((GrowingTreeHead)getTree()).getDisplay() != GrowingTreeHead.SECTIONAL_DISPLAY) {
				displayChangeCommand();
			}
		}
		else {
			super.optionEventPerformed(e);
		}
	}
	/************************************************/
	/* Implements Action Listener              */
	/************************************************/

	/**
	 * Listens to action events.
	 *
	 * @param e ActionEvent that contains information about the tree.
	 */
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand() == null) {
			super.actionPerformed(e);
			return;
		}

		String command = e.getActionCommand();

		if (command.equals(OptionEvent.FUNCTION_CHANGE)) {

			if (getOptionColorSchemeOptions((String)functionJComboBox.getSelectedItem()) == null) {
				colorSettingsJComboBox = new JComboBox();
			}
			else {
				colorSettingsJComboBox = new JComboBox(getOptionColorSchemeOptions((String)functionJComboBox.getSelectedItem()));

			}
			colorSettingsJComboBox.addActionListener(this);
			colorSettingsJComboBox.setActionCommand(OptionEvent.COLOR_SETTINGS_CHANGE);

			if(colorSettingsJComboBox.getItemCount() != 0) {
				colorSettingsJComboBox.setSelectedIndex(0);
			}
			else {
				colorSchemesJComboBox = new JComboBox(getColorSchemesList(PAINT));

				colorSchemesJComboBox.addActionListener(this);
				colorSchemesJComboBox.setActionCommand(OptionEvent.COLOR_SCHEME_CHANGE);

				colorSchemesJComboBox.setSelectedItem(getColorSchemeSetting((String)functionJComboBox.getSelectedItem(), (String)colorSettingsJComboBox.getSelectedItem()));

				colorOptionsPanel.setColorSchemesJComboBox(colorSchemesJComboBox);


			}

			colorOptionsPanel.setColorSettingsJComboBox(colorSettingsJComboBox);


		}
		else if (command.equals(OptionEvent.COLOR_SETTINGS_CHANGE)) {

			if (colorSettingsJComboBox.getItemCount() == 0) {
				colorSchemesJComboBox = new JComboBox(getColorSchemesList(PAINT));
			}
			else {
				colorSchemesJComboBox = new JComboBox(getColorSchemesList((String)colorSettingsJComboBox.getSelectedItem()));
			}

			colorSchemesJComboBox.addActionListener(this);
			colorSchemesJComboBox.setActionCommand(OptionEvent.COLOR_SCHEME_CHANGE);

			colorSchemesJComboBox.setSelectedItem(getColorSchemeSetting((String)functionJComboBox.getSelectedItem(), (String)colorSettingsJComboBox.getSelectedItem()));

			colorOptionsPanel.setColorSchemesJComboBox(colorSchemesJComboBox);

		}
		else if (command.equals(OptionEvent.COLOR_SCHEME_CHANGE)) {
			setColorSchemeSetting((String)functionJComboBox.getSelectedItem(), (String)colorSettingsJComboBox.getSelectedItem(), (String)colorSchemesJComboBox.getSelectedItem());

			drawDrawingNode();
			drawAnimatingNode();

			messageAction(TreeMessageEvent.COLOR_CHANGE, null);
		}
		else if (command.equals(OptionEvent.COLOR_SCHEME_PRESET_CHANGE)) {
			if (!(getColorSettingsString().equals((String)colorPresetOptionsJComboBox.getSelectedItem())) ) {

				setColorSchemeSetting((String)colorPresetOptionsJComboBox.getSelectedItem());

				colorSchemesJComboBox.setSelectedItem(getColorSchemeSetting((String)functionJComboBox.getSelectedItem(), (String)colorSettingsJComboBox.getSelectedItem()));

				drawDrawingNode();
				drawAnimatingNode();

				messageAction(TreeMessageEvent.COLOR_CHANGE, null);
			}
		}
		else {
			super.actionPerformed(e);
		}

	}



}
