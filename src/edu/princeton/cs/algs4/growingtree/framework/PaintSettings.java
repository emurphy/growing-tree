package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)PaintSettings.java
 *
 * Last Modified: 9/01/02
 */

 import java.util.*;
 import java.lang.*;
 import java.awt.*;
 import java.awt.font.*;
 import java.awt.geom.*;


/** * The class provides an object that refers to the settings of a paint color.
	* It presents Strings for every color defined in <code>Color</code>. It also adds
	* some new colors and presents Strings for those. Then, the class simply converts between Strings
	* and colors.
	*
 	* @author  Corey Sanders
	* @version 2.0 9/01/02
 	*/
 public class PaintSettings extends Object implements Cloneable {

	/********************/
	/* Static variables */
	/********************/

	/**
	 * Dark Blue. RGB = (13,30,125).
	 */
	final public static Color darkBlue = new Color(13, 30, 125);

	/**
	 * Gray blue. RGB = (72,70,157).
	 */
	final public static Color grayBlue = new Color(72, 70, 157);

	/**
	 * Gray red. RGB = (205, 158, 158).
	 */
	final public static Color grayRed = new Color(205, 158, 158);

	/**
	 * Gray green. RGB = (88, 110, 84).
	 */
	final public static Color grayGreen = new Color(88, 110, 84);

	/**
	 * Light Green. RGB = (90,120,20).
	 */
	final public static Color lightGreen = new Color(90, 120, 20);

	/**
	 * Dark Green. RGB = (22,84,22).
	 */
	final public static Color darkGreen = new Color(22, 84, 22);


	/**
	 * Yellow Green. RGB = (142,190,47).
	 */
	final public static Color yellowGreen = new Color(142,190,47);

	/**
	 * Light white (darker than Color.white). RGB = (231,231,231).
	 */
	final public static Color lightWhite = new Color(231, 231, 231);

	/**
	 * Light Gray (lighter than Color.gray). RGB = (150,150,150).
	 */
	final public static Color lighterGray = new Color(150, 150, 150);

	/**
	 * Princeton Orange. RGB = (215, 71, 33).
	 */
	final public static Color princetonOrange = new Color(215,71,33);

	/**
	 * Ghostly Gray. RGB = (217, 217, 223).
	 */
	final public static Color ghostlyGray = new Color(217,217,223);

	/**
	 * Aqua. RGB = (40,218,174).
	 */
	final public static Color aqua = new Color(40,218,174);

	/**
	 * Dark Orange. RGB = (161,120,41).
	 */
	final public static Color darkOrange = new Color(161,120,41);

	/**
	 * Brown. RGB = (161,120,41).
	 */
	final public static Color brown = new Color(81,60,21);

	/**
	 * Indigo. RGB = (92,40,170).
	 */
	final public static Color indigo = new Color(92,40,170);

    final public static Color bookRed = new Color(173,32,24);

    final public static String BOOK_RED = "Book Red";

	/**
	 * The color dark blue.
	 */
	final public static String DARK_BLUE = "Dark Blue";

	/**
	 * The color dark green.
	 */
	final public static String DARK_GREEN = "Dark Green";

	/**
	 * The color ghostly gray.
	 */
	final public static String GHOSTLY_GRAY = "Ghostly Gray";

	/**
	 * The color gray blue.
	 */
	final public static String GRAY_BLUE = "Gray Blue";

	/**
	 * The color gray green.
	 */
	final public static String GRAY_GREEN = "Gray Green";

	/**
	 * The color gray red.
	 */
	final public static String GRAY_RED = "Gray Red";

	/**
	 * The color indigo.
	 */
	final public static String INDIGO = "Indigo";

	/**
	 * The color aqua.
	 */
	final public static String AQUA = "Aqua";

	/**
	 * The color light green.
	 */
	final public static String LIGHT_GREEN = "Light Green";

	/**
	 * The color light white.
	 */
	final public static String LIGHT_WHITE = "Light White";

	/**
	 * The color lighter gray.
	 */
	final public static String LIGHTER_GRAY = "Lighter Gray";

	/**
	 * The color princeton orange.
	 */
	final public static String PRINCETON_ORANGE = "Princeton Orange";

	/**
	 * The color dark orange.
	 */
	final public static String DARK_ORANGE = "Dark Orange";

	/**
	 * The color yellow green.
	 */
	final public static String YELLOW_GREEN = "Yellow Green";

	/**
	 * The color black.
	 */
	final public static String BLACK = "Black";

	/**
	 * The color blue.
	 */
	final public static String BLUE = "Blue";

	/**
	 * The color cyan.
	 */
	final public static String CYAN = "Cyan";

	/**
	 * The color dark gray.
	 */
	final public static String DARK_GRAY = "Dark Gray";

	/**
	 * The color gray.
	 */
	final public static String GRAY = "Gray";

	/**
	 * The color green.
	 */
	final public static String GREEN = "Green";

	/**
	 * The color light gray.
	 */
	final public static String LIGHT_GRAY = "Light Gray";

	/**
	 * The color magenta.
	 */
	final public static String MAGENTA = "Magenta";

	/**
	 * The color orange.
	 */
	final public static String ORANGE = "Orange";

	/**
	 * The color pink.
	 */
	final public static String PINK = "Pink";

	/**
	 * The color red.
	 */
	final public static String RED = "Red";

	/**
	 * The color white.
	 */
	final public static String WHITE = "White";

	/**
	 * The color yellow.
	 */
	final public static String YELLOW = "Yellow";


	/**************/
	/* Variables  */
	/**************/

	/**
	 * String defining the type of PaintSettings the current node is.
	 */
	private String settingName = WHITE;

	/**
	 * Paint for the settings.
	 */
	private Paint paint;


	/***************/
	/* Constructor */
	/***************/


	/**
	 * Constructor, contructs a default defined <code>NodeSettings</code>.
	 */
	public PaintSettings() {
		this.setScheme(WHITE);

	}

	/**
	 * Constructor, contructs a defined <code>NodeSettings</code> with the given scheme.
	 *
	 * @param int scheme, defining the scheme of this NodeSettings.
	 */
	public PaintSettings(String scheme) {
		this.setScheme(scheme);
	}


	/*******************/
	/* Static Methods  */
	/*******************/

	/**
	 * Gets a <code>PaintSettings</code> that defines the given String scheme.
	 *
	 * @param s defined color scheme within the class.
	 *
	 * @return <code>PaintSettings</code> that defines the given color scheme or default if no
	 * color scheme exists.
	 */
	public static PaintSettings getScheme(String s) {
		PaintSettings settings = new PaintSettings();
		settings.setScheme(s);
		return settings;
	}

	/**
	 * Gets an array of String elements representing every PaintSettings choice available.
	 *
	 * @return String[] that represents the list of PaintSettings strings.
	 */
	 public static String[] getList() {
		String[] list = {BLACK,BLUE, CYAN,DARK_GRAY, GRAY, GREEN,LIGHT_GRAY, MAGENTA, ORANGE,
		PINK, RED, WHITE, YELLOW, DARK_BLUE, LIGHT_GREEN, LIGHT_WHITE, LIGHTER_GRAY,
		PRINCETON_ORANGE, AQUA, GHOSTLY_GRAY, GRAY_GREEN, DARK_GREEN, GRAY_RED, GRAY_BLUE, INDIGO, YELLOW_GREEN};

		return list;

	}



	/**
	 * Gets the paint for the given String s <code>NodeSettings</code> that defines the given int scheme. The scheme must be one
	 * defined within the class, otherwise the default scheme is returned.
	 *
	 * @param s defined color scheme within the class.
	 *
	 * @return <code>NodeSettings</code> that defines the given color scheme or default if no
	 * color scheme exists.
	 */
	public static Paint getPaint(String s) {

        if (s.equals(BOOK_RED))
            return bookRed;
		if (s.equals(DARK_BLUE)) {
			return darkBlue;
		}
		if (s.equals(AQUA)) {
			return aqua;
		}
		if (s.equals(GHOSTLY_GRAY)) {
			return ghostlyGray;
		}
		if (s.equals(GRAY_GREEN)) {
			return grayGreen;
		}
		if (s.equals(GRAY_RED)) {
			return grayRed;
		}
		if (s.equals(GRAY_BLUE)) {
			return grayBlue;
		}
		if (s.equals(INDIGO)) {
			return indigo;
		}
		if (s.equals(YELLOW_GREEN)) {
			return yellowGreen;
		}
		if (s.equals(DARK_GREEN)) {
			return darkGreen;
		}
		if (s.equals(PRINCETON_ORANGE)) {
			return princetonOrange;
		}
		if (s.equals(LIGHTER_GRAY)) {
			return lighterGray;
		}
		if (s.equals(LIGHT_WHITE)) {
			return lightWhite;
		}
		if (s.equals(LIGHT_GREEN)) {
			return lightGreen;
		}


		if (s.equals(BLACK)) {
			return Color.black;
		}
		if (s.equals(BLUE)) {
			return Color.blue;
		}
		if (s.equals(CYAN)) {
			return Color.cyan;
		}
		if (s.equals(DARK_GRAY)) {
			return Color.darkGray;
		}
		if (s.equals(GRAY)) {
			return Color.gray;
		}
		if (s.equals(GREEN)) {
			return Color.green;
		}
		if (s.equals(LIGHT_GRAY)) {
			return Color.lightGray;
		}
		if (s.equals(MAGENTA)) {
			return Color.magenta;
		}
		if (s.equals(ORANGE)) {
			return Color.orange;
		}
		if (s.equals(PINK)) {
			return Color.pink;
		}
		if (s.equals(RED)) {
			return bookRed;
		}
		if (s.equals(WHITE)) {
			return Color.white;
		}
		if (s.equals(YELLOW)) {
			return Color.yellow;
		}

		return Color.white;
	}


	/**
	 * Gets a <code>NodeSettings</code> that defines the given int scheme. The scheme must be one
	 * defined within the class, otherwise the default scheme is returned.
	 *
	 * @param s defined color scheme within the class.
	 *
	 * @return <code>NodeSettings</code> that defines the given color scheme or default if no
	 * color scheme exists.
	 */
	public static Color getColor(String s) {

        if (s.equals(BOOK_RED))
            return bookRed;

		if (s.equals(DARK_BLUE)) {
			return darkBlue;
		}
		if (s.equals(AQUA)) {
			return aqua;
		}
		if (s.equals(GHOSTLY_GRAY)) {
			return ghostlyGray;
		}
		if (s.equals(GRAY_GREEN)) {
			return grayGreen;
		}
		if (s.equals(GRAY_RED)) {
			return grayRed;
		}
		if (s.equals(GRAY_BLUE)) {
			return grayBlue;
		}
		if (s.equals(INDIGO)) {
			return indigo;
		}
		if (s.equals(YELLOW_GREEN)) {
			return yellowGreen;
		}
		if (s.equals(DARK_GREEN)) {
			return darkGreen;
		}
		if (s.equals(PRINCETON_ORANGE)) {
			return princetonOrange;
		}
		if (s.equals(LIGHTER_GRAY)) {
			return lighterGray;
		}
		if (s.equals(LIGHT_WHITE)) {
			return lightWhite;
		}
		if (s.equals(LIGHT_GREEN)) {
			return lightGreen;
		}
		if (s.equals(DARK_ORANGE)) {
			return darkOrange;
		}



		if (s.equals(BLACK)) {
			return Color.black;
		}
		if (s.equals(BLUE)) {
			return Color.blue;
		}
		if (s.equals(CYAN)) {
			return Color.cyan;
		}
		if (s.equals(DARK_GRAY)) {
			return Color.darkGray;
		}
		if (s.equals(GRAY)) {
			return Color.gray;
		}
		if (s.equals(GREEN)) {
			return Color.green;
		}
		if (s.equals(LIGHT_GRAY)) {
			return Color.lightGray;
		}
		if (s.equals(MAGENTA)) {
			return Color.magenta;
		}
		if (s.equals(ORANGE)) {
			return Color.orange;
		}
		if (s.equals(PINK)) {
			return Color.pink;
		}
		if (s.equals(RED)) {
			return bookRed;
		}
		if (s.equals(WHITE)) {
			return Color.white;
		}
		if (s.equals(YELLOW)) {
			return Color.yellow;
		}

		return Color.white;
	}



	/**
	 * Gets a <code>String</code> that defines the given Color. The color is either defined
	 * in this class of
	 *
	 * @param s defined color within the class or Color class.
	 *
	 * @return <code>String</code> that defines the given color or WHITE if no
	 * color is determined.
	 */
	public static String getString(Color s) {

        if (s.equals(bookRed))
            return BOOK_RED;

		if (s.equals(darkBlue)) {
			return DARK_BLUE;
		}
		if (s.equals(aqua)) {
			return AQUA;
		}
		if (s.equals(ghostlyGray)) {
			return GHOSTLY_GRAY;
		}
		if (s.equals(grayGreen)) {
			return GRAY_GREEN;
		}
		if (s.equals(grayRed)) {
			return GRAY_RED;
		}
		if (s.equals(grayBlue)) {
			return GRAY_BLUE;
		}
		if (s.equals(indigo)) {
			return INDIGO;
		}
		if (s.equals(yellowGreen)) {
			return YELLOW_GREEN;
		}
		if (s.equals(princetonOrange )) {
			return PRINCETON_ORANGE;
		}
		if (s.equals(lighterGray )) {
			return LIGHTER_GRAY;
		}
		if (s.equals(lightWhite )) {
			return LIGHT_WHITE;
		}
		if (s.equals(lightGreen )) {
			return LIGHT_GREEN;
		}
		if (s.equals(darkGreen )) {
			return DARK_GREEN;
		}
		if (s.equals(darkOrange)) {
			return DARK_ORANGE;
		}


		if (s.equals(Color.black )) {
			return BLACK;
		}
		if (s.equals(Color.blue )) {
			return BLUE;
		}
		if (s.equals(Color.cyan )) {
			return CYAN;
		}
		if (s.equals(Color.darkGray )) {
			return DARK_GRAY;
		}
		if (s.equals(Color.gray)) {
			return GRAY;
		}
		if (s.equals(Color.green )) {
			return GREEN;
		}
		if (s.equals(Color.lightGray )) {
			return LIGHT_GRAY;
		}
		if (s.equals(Color.magenta )) {
			return MAGENTA;
		}
		if (s.equals(Color.orange )) {
			return ORANGE;
		}
		if (s.equals(Color.pink)) {
			return PINK;
		}
		if (s.equals(Color.red )) {
			return RED;
		}
		if (s.equals(bookRed )) {
			return RED;
		}
		if (s.equals(Color.white )) {
			return WHITE;
		}
		if (s.equals( Color.yellow )) {
			return YELLOW;
		}

		return WHITE;
	}


	/**********************/
	/* Accessor Methods   */
	/**********************/


	/**
	 * Gets the paint.
	 *
	 * @return <code>Paint</code> of the settings.
	 */
	public Paint getPaint() {
		return paint;
	}


	/**
	 * Gets the string name for the current settings.
	 *
	 * @return String defining the settings name.
	 */
	 public String getSettingName() {
		 return settingName;
	 }

	/*******************/
	/* Mutator Methods */
	/*******************/


	/**
	 * Sets the paint.
	 *
	 * @param p <code>Paint</code> of the settings.
	 */
	public void setPaint(Paint p) {
		paint = p;
	}

	/**
	 * Sets the string name for the current settings.
	 *
	 * @param settingName String defining the settings name.
	 */
	 public void setSettingName(String settingName) {
		 this.settingName = settingName;
	 }


	/**
	 * Sets the scheme of the current PaintSettings using the defined String within this class. WHITE is default.
	 *
	 * @param s defined color string within the class to which the PaintSettings are set.
	 */
	public void setScheme(String s) {

		setSettingName(s);

        if (s.equals(BOOK_RED))
            setPaint(bookRed);

		if (s.equals(AQUA)) {
			setPaint(aqua);
		}
		else if (s.equals(GHOSTLY_GRAY)) {
			setPaint(ghostlyGray);
		}
		else if (s.equals(GRAY_GREEN)) {
			setPaint(grayGreen);
		}
		else if (s.equals(GRAY_RED)) {
			setPaint(grayRed);
		}
		else if (s.equals(GRAY_BLUE)) {
			setPaint(grayBlue);
		}
		else if (s.equals(INDIGO)) {
			setPaint(indigo);
		}
		else if (s.equals(YELLOW_GREEN)) {
			setPaint(yellowGreen);
		}
		else if (s.equals(DARK_BLUE)) {
			setPaint(darkBlue);
		}
		else if (s.equals(PRINCETON_ORANGE)) {
			setPaint(princetonOrange);
		}
		else if (s.equals(LIGHTER_GRAY)) {
			setPaint(lighterGray);
		}
		else if (s.equals(LIGHT_WHITE)) {
			setPaint(lightWhite);
		}
		else if (s.equals(LIGHT_GREEN)) {
			setPaint(lightGreen);
		}
		else if (s.equals(DARK_GREEN)) {
			setPaint(darkGreen);
		}
		else if (s.equals(DARK_ORANGE)) {
			setPaint(darkOrange);
		}
		else if (s.equals(BLACK)) {
			setPaint(Color.black);
		}
		else if (s.equals(BLUE)) {
			setPaint(Color.blue);
		}
		else if (s.equals(CYAN)) {
			setPaint(Color.cyan);
		}
		else if (s.equals(DARK_GRAY)) {
			setPaint(Color.darkGray);
		}
		else if (s.equals(GRAY)) {
			setPaint(Color.gray);
		}
		else if (s.equals(GREEN)) {
			setPaint(Color.green);
		}
		else if (s.equals(LIGHT_GRAY)) {
			setPaint(Color.lightGray);
		}
		else if (s.equals(MAGENTA)) {
			setPaint(Color.magenta);
		}
		else if (s.equals(ORANGE)) {
			setPaint(Color.orange);
		}
		else if (s.equals(PINK)) {
			setPaint(Color.pink);
		}
		else if (s.equals(RED)) {
			setPaint(bookRed);
		}
		else if (s.equals(WHITE)) {
			setPaint(Color.white);
		}
		else if (s.equals(YELLOW)) {
			setPaint(Color.yellow);
		}
		else {
			setPaint(Color.white);
		}
	}

	/**
	 * Implements cloneable so this method must be added so the object can be cloned.
	 *
	 * @return Object clone of the current PaintSettings.
	 */
    public Object clone() {

		try {
		    return super.clone();
		}
		catch (CloneNotSupportedException e) {
		    // this shouldn't happen, since we are Cloneable
		    throw new InternalError();
		}
    }


}
