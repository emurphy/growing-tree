package edu.princeton.cs.algs4.growingtree.framework;


public class ActionElementType<P extends NodeProperties>
{
    public static int BSTTREETYPE = 0;
    public static int INTEGERTYPE = 1;
    public static int KEYTYPETYPE = 2;
    public static int NODEKEYTYPE = 3;
    public static int DOUBLETYPE = 4;

    private int type;
    private GrowingTreeNode<P> treeval = null;
    private Integer intval = null;
    private KeyType keyval = null;
    private NodeAndKey<P> nodeval = null;
    private Double doubleval = null;


    public ActionElementType(GrowingTreeNode<P> value)
    {
        treeval = value;
        type = BSTTREETYPE;
    }

    public ActionElementType(Integer value)
    {   
        intval = value;
        type = INTEGERTYPE;
    }

    public ActionElementType(KeyType value)
    {   
        keyval = value;
        type = KEYTYPETYPE;
    }


    public ActionElementType(NodeAndKey<P> value)
    {   
        nodeval = value;
        type = NODEKEYTYPE;
    }

    public ActionElementType(Double value) {
    	doubleval = value;
    	type = DOUBLETYPE;
    }
    
    public boolean isBSTTree()
    {
        return type == BSTTREETYPE;
    }

    public boolean isInteger()
    {
        return type == INTEGERTYPE;
    }

    public boolean isKeyType()
    {
        return type == KEYTYPETYPE;
    }

    public boolean isNodeAndKeyType()
    {
        return type == NODEKEYTYPE;
    }

    public GrowingTreeNode<P> getBSTTreeValue()
    {
        return treeval;
    }

    public Integer getIntegerValue()
    {
        return intval;
    }

    public KeyType getKeyTypeValue()
    {
        return keyval;
    }

    public NodeAndKey<P> getNodeAndKeyValue()
    {
        return nodeval;
    }
    
    public Double getDoubleValue()
    {
    	return doubleval;
    }
}
