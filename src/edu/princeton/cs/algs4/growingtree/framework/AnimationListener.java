package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)AnimationListener.java
 *
 * Last Modified: 9/15/01
 */

import java.awt.*;
import java.util.*;

/**
 * The listener interface for receiving Animation events.
 * The class that is interested in processing an Animation event
 * implements this interface, and the object created with that
 * class is registered with an Animation Object, using the object's
 * <code>addAnimationListener</code> method. When the action event
 * occurs, that object's <code>animationEventPerformed</code> method is
 * invoked.
 *
 * @see AnimationEvent
 *
 * @author Corey Sanders
 * @version 1.3 9/15/01
 */
public interface AnimationListener extends EventListener {

	 /**
     * Invoked when an animation action occurs.
     */
	public void animationEventPerformed(AnimationEvent e);
}
