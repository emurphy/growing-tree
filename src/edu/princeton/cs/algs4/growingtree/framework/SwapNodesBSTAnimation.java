package edu.princeton.cs.algs4.growingtree.framework;
import java.awt.Graphics2D;



public class SwapNodesBSTAnimation<P extends NodeProperties> extends
		SelectionBSTAnimation<P> {

	public SwapNodesBSTAnimation(int elementCount,
			NodeSettings AnimationSchemeLeft,
			NodeSettings AnimationSchemeRight, NodeSettings AnimatorScheme,
			KeySettings KeyAnimatorScheme, String startingCmd, int stepTime) {
		super(elementCount, AnimationSchemeLeft, AnimationSchemeRight,
				AnimatorScheme, KeyAnimatorScheme, startingCmd, stepTime);
	}

	public SwapNodesBSTAnimation(int elementCount) {
		super(elementCount);
	}
	
	public void drawAnimation(Graphics2D g2, String startingStatus) {
		if (getStatus().equals(Animation.PLAY)) {
			if (currentLocation + getStepSize() >= END) {
				GrowingTreeNode<P> first = nodes.getFirst();
				GrowingTreeNode<P> last = nodes.getLast();
				KeyType tempKey = last.getKey();
				Object tempVal = last.getValue();
				last.setNode(first.getKey(), first.getValue());
				first.setNode(tempKey, tempVal);
			}
		}
		super.drawAnimation(g2, startingStatus);	
	}

}
