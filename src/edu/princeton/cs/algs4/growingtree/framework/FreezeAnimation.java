package edu.princeton.cs.algs4.growingtree.framework;

import java.awt.Graphics2D;
import java.text.NumberFormat;

/**
 * The Animation object that defines a "Freeze" of the tree. This is used to show the state
 * of the tree between other animations, like rotations.
 * @author Josh Israel
 */


public class FreezeAnimation<P extends NodeProperties> extends AbstractAnimation {
	
	private double MAX_LOCATION = 15;
	
	private double lengthMult;
	
	/**
	 * Private double used to hold the current location steps.
	 */
	private double currentLocation = 0.0;
	
	/**
	 * Head of the tree associated with this animation
	 */
	private GrowingTreeHead<P> treeHead;
	
	public FreezeAnimation(GrowingTreeHead<P> tree, int stepTime, double lengthMult) {
		// Set the drawing node.
		super();
		setStartingCommand(Animation.PLAY);
		setStepTime(stepTime);
		setTreeHead(tree);
		setLengthMult(lengthMult);
	}
	
	/**
	 * Draws the animation of the next step, using the status of the animation (Animation.PLAY, Animation.PAUSE and so forth).
	 * After completing the drawing, the Animation sends an AnimationEvent to all its listeners, indicating
	 * any information that the listerners may wish to use.
	 *
	 * @param g2 the graphics to which the animation step should be drawn.
	 * @param startingStatus the status used as the starting command of animation, if needed.
	 */
	public void drawAnimation(Graphics2D g2, String startingStatus) {

		setStartingCommand(startingStatus);

		// BEGIN status
		if (getStatus().equals(Animation.BEGIN)) {
			currentLocation = 0;
			animationAction();
			
			// set starting status
			setStatus(getStartingCommand());

			return;

		}
		// Currently on a step and no changes have occured. Return to starting command
		if (getStatus().equals(Animation.STEP)) {
			setStatus(getStartingCommand());
		}

		// PLAY status
		if (getStatus().equals(Animation.PLAY)) {

			messageAction(Animation.PLAY);
			getTreeHead().MakeTree(g2);
			if(getStep()) { // Skip middle animation steps.
				currentLocation = Math.ceil(currentLocation) + getStepSize();
			}
			else { // Normal step
				currentLocation += getStepSize();
			}

			// Completed Animation
			if (currentLocation >= MAX_LOCATION * lengthMult) {
				setStatus(Animation.FINISH);
				drawAnimation(g2, getStartingCommand());
				return;
			}

		}

		// REWIND status
		if (getStatus().equals(Animation.REWIND)) {

			messageAction(Animation.REWIND);

			if(getStep()) { // Skip middle Animation Steps
				currentLocation = Math.floor(currentLocation) - getStepSize();
			}
			else { // Normal Step
				currentLocation -= getStepSize();
			}

			// Beginning of Animation
			if (currentLocation <= 0) {
				setStatus(Animation.PAUSE);
				currentLocation = 0;
				animationAction();
				return;
			}

		}

		// PAUSE status
		if (getStatus().equals(Animation.PAUSE)) {
			messageAction(Animation.PAUSE);
		}

		// STOP status
		if (getStatus().equals(Animation.STOP)) {
			messageAction(Animation.STOP);
			return;
		}

		// FINISH status
		if (getStatus().equals(Animation.FINISH)) {
			animationAction();
			getTreeHead().popTreeProperties();
			return;
		}

		animationAction();

	}

	private void setTreeHead(GrowingTreeHead<P> treeHead) {
		this.treeHead = treeHead;
	}

	private GrowingTreeHead<P> getTreeHead() {
		return treeHead;
	}

	private void setLengthMult(double lengthMult) {
		this.lengthMult = lengthMult;
	}

	private double getLengthMult() {
		return lengthMult;
	}

}
