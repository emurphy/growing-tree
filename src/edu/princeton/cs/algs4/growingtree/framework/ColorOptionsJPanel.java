package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)OptionColorToolsJPanel.java
 *
 * Last Modified: 9/01/02
 */

import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.net.*;



/**
 * OptionColorToolsJPanel for use with the the OptionJPanel. Within this JPanel, the options and toolbars are
 * made and pictured simply for the controls of the colors.
 *
 *
 * <p>
 * @author  Corey Sanders
 * @version 3.0 9/01/02
 * @see TreeJApplet
 * @see TreeJPanel
 */
public class ColorOptionsJPanel extends OptionJPanel implements ActionListener, TreeMessageListener {


	/**
	 * Id for the control panel.
	 */
	protected static int id = OptionEvent.COLOR_TOOLS_OPTION_PANEL;

   /**
	* JButton used for saving to the specific tree.
	*/
	private JButton resetButton;
   /**
	* JButton used for saving to the specific tree.
	*/
	private JButton saveTreeButton;
   /**
	* JButton used for saving to all trees.
	*/
	private JButton saveAllButton;


	/**
	 * JComboBox for the preset Colors options.
	 */
	private JComboBox presetColorsJComboBox;

	/**
	 * JComboBox for the functions.
	 */
	private JComboBox functionJComboBox;



	/**
	 * JComboBox for the color settings.
	 */
	private JComboBox colorSettingsJComboBox;
	/**
	 * JComboBox for the color schemes.
	 */
	private JComboBox colorSchemesJComboBox;


	/**
	 * Panel used to display the customizable options and the border.
	 */
	private JPanel customizablePanel;

	/**
	 * Title around the customizablePanel.
	 */
	private TitledBorder customizablePanelTitleBorder;

	/**
	 * Panel used to display the presets and the border.
	 */
	private JPanel presetsPanel;

	/**
	 * Title around the presetsPanel.
	 */
	private TitledBorder presetsPanelTitleBorder;

	/**
	 * Panel used to display the drawing node and the border.
	 */
	private JPanel drawingNodePanel;

	/**
	 * Title around the drawingNodePanel.
	 */
	private TitledBorder drawingNodeTitleBorder;

	/**
	 * NodeAndLinkDrawingJPanel used as the panel for the displaying the drawing node.
	 */
	private	NodeAndLinkDrawingJPanel nodeAndLinkDrawingPanel;

	/**
	 * Panel used to display the animating node and the border.
	 */
	private JPanel animatingNodePanel;

	/**
	 * Title around the animatingNodePanel.
	 */
	private TitledBorder animatingNodeTitleBorder;

	/**
	 * NodeAndLinkAnimatingJPanel used as the panel for the displaying the animating node.
	 */
	private	NodeAndLinkAnimatingJPanel nodeAndLinkAnimatingPanel;

	/**
	 * The drawingNode for the panel.
	 */
	private GrowingTreeNode drawingNode;
	/**
	 * The animatingNode for the panel.
	 */
	private GrowingTreeNode animatingNode;
	/**
	 * The currently selected panel.
	 */
	private TreeJPanel selectedPanel;

	/**
	 * GridBagConstraints of the gridbaglayout.
	 */
	private GridBagConstraints c;
	/**
	 * GridBagLayout of the gridbaglayout.
	 */
	private GridBagLayout gridbag;

  /**
	* This constructor makes the OptionColorToolsJPanel for usage with <code>OptionMainJPanel</code>. It presents the
	* options available for manipulation of the colors.
 	*/
   public ColorOptionsJPanel() {
	   super();

		KeyType keyValue = new KeyType((int)(Math.random() * 9));

		// Set Key for drawingNode and animatingNode
		setDrawingNode(new GrowingTreeNode(GrowingTreeNode.ANIMATING_BST_TREE_TYPE));
		setAnimatingNode(new GrowingTreeNode(GrowingTreeNode.ANIMATING_BST_TREE_TYPE));

		getDrawingNode().setNode(keyValue, new DrawingKey(keyValue));
		getAnimatingNode().setNode(keyValue, new DrawingKey(keyValue));

		getDrawingNode().setSettings(new NodeSettings());
		((DrawableKey)getDrawingNode().getValue()).setSettings(new KeySettings());

		getAnimatingNode().setSettings(new NodeSettings());
		((DrawableKey)getAnimatingNode().getValue()).setSettings(new KeySettings());

		createPresetColorsComboBox();
		createFunctionComboBox();
		createColorSettingsComboBox();
		createColorSchemesComboBox();

		createDrawingNodePanel();

		createAnimatingNodePanel();

		createCustomizablePanel();

		createPresetPanel();

		createColorToolsPanel();



    }

	/**
	 * Creates the color tools panel.
	 */
    protected void createColorToolsPanel() {

		gridbag = new GridBagLayout();
		c = new GridBagConstraints();
        this.setLayout(gridbag);


		c.anchor = GridBagConstraints.CENTER;

		c.weighty = 1;
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;
        c.insets = new Insets(0,0,0,0);
       	c.gridx = 0;
		c.gridy = 0;
		gridbag.setConstraints(drawingNodePanel, c);
        this.add(drawingNodePanel);

		c.weighty = 1;
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.insets = new Insets(0,0,0,0);
       	c.gridx = 1;
		c.gridy = 0;
		gridbag.setConstraints(animatingNodePanel, c);
        this.add(animatingNodePanel);



		c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0,0,0,0);
		c.weightx = 0;
        c.weighty = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
		c.gridx = 0;
        c.gridy = 1;
        gridbag.setConstraints(customizablePanel, c);
        this.add(customizablePanel);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0,0,0,0);
		c.weighty = 0;
		c.weightx = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
       	c.gridx = 1;
		c.gridy = 1;
		gridbag.setConstraints(presetsPanel, c);
        this.add(presetsPanel);


	}



	/**
	 * Creates the preset colors combo box.
	 */
	protected void createPresetColorsComboBox() {
		presetColorsJComboBox = new JComboBox();
	}

	/**
	 * Creates the function combo box.
	 */
	protected void createFunctionComboBox() {
		functionJComboBox = new JComboBox();
	}

	/**
	 * Creates the color settings combo box.
	 */
	protected void createColorSettingsComboBox() {
		colorSettingsJComboBox = new JComboBox();
	}

	/**
	 * Creates the color Schemes combo box.
	 */
	protected void createColorSchemesComboBox() {
		colorSchemesJComboBox = new JComboBox();
	}



	/**
	 * Creates the drawing Node Panel.
	 */
	protected void createPresetPanel() {
		// Create display panel.
		presetsPanel = new JPanel();

		presetsPanel.setLayout(new BorderLayout());
		presetsPanel.setMaximumSize(new Dimension(100, 80));
		presetsPanel.setPreferredSize(new Dimension(100, 80));

		// Create Border around display panel.
		presetsPanelTitleBorder = new TitledBorder(BorderFactory.createLineBorder(Color.blue, 1));
		presetsPanelTitleBorder.setTitlePosition(TitledBorder.TOP);
		presetsPanelTitleBorder.setTitleJustification(TitledBorder.CENTER);

		presetsPanelTitleBorder.setTitle("Preset Options");
		presetsPanel.setBorder(presetsPanelTitleBorder);

		presetsPanel.setLayout(new GridLayout(1, 1));

		presetsPanel.add(presetColorsJComboBox);

	}


	/**
	 * Creates the drawing Node Panel.
	 */
	protected void createCustomizablePanel() {
		// Create display panel.
		customizablePanel = new JPanel();

		customizablePanel.setLayout(new BorderLayout());
		customizablePanel.setMaximumSize(new Dimension(100, 200));
		customizablePanel.setPreferredSize(new Dimension(100, 200));

		// Create Border around display panel.
		customizablePanelTitleBorder = new TitledBorder(BorderFactory.createLineBorder(Color.blue, 1));
		customizablePanelTitleBorder.setTitlePosition(TitledBorder.TOP);
		customizablePanelTitleBorder.setTitleJustification(TitledBorder.CENTER);

		customizablePanelTitleBorder.setTitle("Customizable Options");
		customizablePanel.setBorder(customizablePanelTitleBorder);

		customizablePanel.setLayout(new GridLayout(3, 1));

		customizablePanel.add(functionJComboBox);

		customizablePanel.add(colorSettingsJComboBox);

		customizablePanel.add(colorSchemesJComboBox);

	}


	/**
	 * Creates the drawing Node Panel.
	 */
	protected void createDrawingNodePanel() {
		// Create display panel.
		drawingNodePanel = new JPanel();

		drawingNodePanel.setLayout(new BorderLayout());
		drawingNodePanel.setMaximumSize(new Dimension(200, 200));
		drawingNodePanel.setPreferredSize(new Dimension(200, 200));

		// Create Node Draw Panel.
		setDrawingPanel(new NodeAndLinkDrawingJPanel());
		getDrawingPanel().setNode(getDrawingNode());

		// Create Border around display panel.
		drawingNodeTitleBorder = new TitledBorder(BorderFactory.createLineBorder(Color.blue, 1));
		drawingNodeTitleBorder.setTitlePosition(TitledBorder.TOP);
		drawingNodeTitleBorder.setTitleJustification(TitledBorder.CENTER);

		drawingNodeTitleBorder.setTitle("Drawing Node Scheme");
		drawingNodePanel.setBorder(drawingNodeTitleBorder);

		drawingNodePanel.add(nodeAndLinkDrawingPanel, BorderLayout.CENTER);

	}
	/**
	 * Creates the animating Node Panel.
	 */
	protected void createAnimatingNodePanel() {
		// Create display panel.
		animatingNodePanel = new JPanel();

		animatingNodePanel.setLayout(new BorderLayout());
		animatingNodePanel.setMaximumSize(new Dimension(200, 200));
		animatingNodePanel.setPreferredSize(new Dimension(200, 200));

		// Create Node Draw Panel.
		setAnimatingPanel(new NodeAndLinkAnimatingJPanel());
		getAnimatingPanel().setAnimatingNode(getAnimatingNode());

		// Create Border around display panel.
		animatingNodeTitleBorder = new TitledBorder(BorderFactory.createLineBorder(Color.blue, 1));
		animatingNodeTitleBorder.setTitlePosition(TitledBorder.TOP);
		animatingNodeTitleBorder.setTitleJustification(TitledBorder.CENTER);

		animatingNodeTitleBorder.setTitle("Animating Node Scheme");
		animatingNodePanel.setBorder(animatingNodeTitleBorder);

		animatingNodePanel.add(nodeAndLinkAnimatingPanel, BorderLayout.CENTER);


	}

 	 /**
	  * Gets the currently selected panel for the color schemes.
	  *
	  * @return TreeJPanel currently selected for the color schemes.
	  */
	  public TreeJPanel getSelectedPanel() {
		  return selectedPanel;
	  }


 	 /**
	  * Sets the currently selected panel for the color schemes.
	  *
	  * @param selectedPanel currently selected for schemes.
	  */
	  public void setSelectedPanel(TreeJPanel selectedPanel) {
		  this.selectedPanel = selectedPanel;
	  }

 	 /**
	  * Gets the animating node in the panel.
	  *
	  * @return BSTTree animating node.
	  */
	  public GrowingTreeNode getAnimatingNode() {
		  return animatingNode;
	  }

 	 /**
	  * Sets the animating node in the panel.
	  *
	  * @param animatingNode BSTTree animating node.
	  */
	  public void setAnimatingNode(GrowingTreeNode animatingNode) {
		  this.animatingNode = animatingNode;
	  }

 	 /**
	  * Gets the drawing node in the panel.
	  *
	  * @return BSTTree drawing node.
	  */
	  public GrowingTreeNode getDrawingNode() {
		  return drawingNode;
	  }

 	 /**
	  * Sets the drawing node in the panel.
	  *
	  * @param drawingNode BSTTree drawing node.
	  */
	  public void setDrawingNode(GrowingTreeNode drawingNode) {
		  this.drawingNode = drawingNode;
	  }


 	/**
	  * Gets the animating panel.
	  *
	  * @return NodeAndLinkAnimatingJPanel animating panel.
	  */
	  public NodeAndLinkAnimatingJPanel getAnimatingPanel() {
		  return nodeAndLinkAnimatingPanel;
	  }

 	 /**
	  * Sets the animating panel.
	  *
	  * @param nodeAndLinkAnimatingPanel animating panel.
	  */
	  public void setAnimatingPanel(NodeAndLinkAnimatingJPanel nodeAndLinkAnimatingPanel) {
		  this.nodeAndLinkAnimatingPanel = nodeAndLinkAnimatingPanel;
	  }

 	 /**
	  * Gets the drawing panel.
	  *
	  * @return NodeAndLinkDrawingJPanel drawing panel.
	  */
	  public NodeAndLinkDrawingJPanel getDrawingPanel() {
		  return nodeAndLinkDrawingPanel;
	  }

 	 /**
	  * Sets the drawing panel.
	  *
	  * @param NodeAndLinkDrawingJPanel drawing panel.
	  */
	  public void setDrawingPanel(NodeAndLinkDrawingJPanel nodeAndLinkDrawingPanel) {
		  this.nodeAndLinkDrawingPanel = nodeAndLinkDrawingPanel;
	  }

 	 /**
	  * Gets the preset color options JCombo box.
	  *
	  * @return JComboBox that contains the preset color options.
	  */
	  public JComboBox getPresetColorsJComboBox() {
		  return presetColorsJComboBox;
	  }

 	 /**
	  * Sets the function JCombo box.
	  *
	  * @param functionJComboBox JComboBox that contains the function options.
	  */
	  public void setPresetColorOptionsJComboBox(JComboBox presetColorsJComboBox ) {
		remove(getPresetColorsJComboBox());

		this.presetColorsJComboBox = presetColorsJComboBox;

		presetsPanel.removeAll();
		presetsPanel.add(presetColorsJComboBox);

        this.revalidate();

	  }

 	 /**
	  * Gets the function JCombo box.
	  *
	  * @return JComboBox that contains the function options.
	  */
	  public JComboBox getFunctionJComboBox() {
		  return functionJComboBox;
	  }

 	 /**
	  * Sets the function JCombo box.
	  *
	  * @param functionJComboBox JComboBox that contains the function options.
	  */
	  public void setFunctionJComboBox(JComboBox functionJComboBox ) {

		this.functionJComboBox = functionJComboBox;

		customizablePanel.removeAll();

		customizablePanel.add(functionJComboBox);

		customizablePanel.add(colorSettingsJComboBox);

		customizablePanel.add(colorSchemesJComboBox);

	  }

 	 /**
	  * Gets the color settings JCombo box.
	  *
	  * @return JComboBox that contains the color settings options.
	  */
	  public JComboBox getColorSettingsJComboBox() {
		  return colorSettingsJComboBox;

	  }

 	 /**
	  * Sets the color settings JCombo box.
	  *
	  * @param colorSettingsJComboBox JComboBox that contains the color settings options.
	  */
	  public void setColorSettingsJComboBox(JComboBox colorSettingsJComboBox ) {
		this.colorSettingsJComboBox = colorSettingsJComboBox;

		customizablePanel.removeAll();


		customizablePanel.add(functionJComboBox);

		customizablePanel.add(colorSettingsJComboBox);

		customizablePanel.add(colorSchemesJComboBox);

	  }

 	 /**
	  * Gets the color schemes JCombo box.
	  *
	  * @return JComboBox that contains the color schemes options.
	  */
	  public JComboBox getColorSchemesJComboBox() {
		  return colorSchemesJComboBox;
	  }

 	 /**
	  * Sets the color schemes JCombo box.
	  *
	  * @param colorSchemesJComboBox JComboBox that contains the color schemes options.
	  */
	  public void setColorSchemesJComboBox(JComboBox colorSchemesJComboBox ) {


		this.colorSchemesJComboBox = colorSchemesJComboBox;


		customizablePanel.removeAll();


		customizablePanel.add(functionJComboBox);

		customizablePanel.add(colorSettingsJComboBox);

		customizablePanel.add(colorSchemesJComboBox);

	  }


	/**********************/
	/* Commands for tree  */
	/**********************/

	/**
	 * If an action is performed through one of the buttons.
	 *
	 * @param e ActionEvent that contains information about the action performed.
	 */
	public void actionPerformed(ActionEvent e) {
		// Command of action
		String command = e.getActionCommand();

		if (command.equals(OptionEvent.RESET)) {
			optionAction(OptionEvent.RESET, null, getSelectedPanel());
		}
		if (command.equals(OptionEvent.SAVE_TREE)) {
			optionAction(OptionEvent.SAVE_TREE, null, getSelectedPanel());
		}
		else if (command.equals(OptionEvent.SAVE_ALL)) {
			optionAction(OptionEvent.SAVE_TREE, null, getSelectedPanel());
			optionAction(OptionEvent.SAVE_ALL, getSelectedPanel());
		}

	}

	/**
	 * Tree Message Event performed.
	 *
	 * @param e TreeMessageEvent.
	 */
	public void treeMessageEventPerformed(TreeMessageEvent e) {

		String msg = e.getMessage();

		if (msg.equals(TreeMessageEvent.COLOR_PANEL)) {
			setSelectedPanel((TreeJPanel)e.getMessageObject());
			getSelectedPanel().constructColorOptions(this);
		}

		if (msg.equals(TreeMessageEvent.SET_PRESET_COLOR_OPTIONS)) {
			setSelectedPanel((TreeJPanel)e.getMessageObject());
			getSelectedPanel().constructPresetColorOptions(this);
		}



	}



}
