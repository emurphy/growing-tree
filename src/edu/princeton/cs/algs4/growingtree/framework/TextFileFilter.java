package edu.princeton.cs.algs4.growingtree.framework;
import javax.swing.filechooser.FileFilter;
import java.io.File;

public class TextFileFilter extends FileFilter
{
    public boolean accept(File pathname)
    {
        return pathname.toString().endsWith("txt");
    }

    public String getDescription()
    {
        return "Just text files";
    }
}
