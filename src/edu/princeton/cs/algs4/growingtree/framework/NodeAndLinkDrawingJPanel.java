package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)NodeAndLinkDrawingJPanel.java
 *
 * Last Modified: 9/15/01
 */
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.beans.*;

/**
 * A class that extends <code>NodeDrawingJPanel</code> to draw the node and the link within
 * the specific Graphics2D within the panel. The difference between this and its super class.
 * <p>
 *
 * @author  Corey Sanders
 * @version 1.3 9/15/01
 */
public class NodeAndLinkDrawingJPanel extends NodeDrawingJPanel{

	/**
	 * Sole Constructor for the JPanel that paints the node using the AnimatingTree drawNode method.
	 * The JPanel only makes a new image on a resize and otherwise, double-buffers the graphics by
	 * leaving the image unmodified.
	 */
	public NodeAndLinkDrawingJPanel() {
		super();
    }

	/**
 	 *******************
	 * Drawing methods *
	 *******************
	 */

	/**
	 * Method actually called to complete the drawing of the panel. The node is drawn to fill the
	 * entire graphics given within the panel to also include the the links of the node.
	 */
	protected void draw() {
		if (getNode() != null) {

			AffineTransform scaleTransform = AffineTransform.getScaleInstance((getDrawTreeGraphics().getClipBounds().getWidth() / (1.2) ), (getDrawTreeGraphics().getClipBounds().getHeight() / (1.2)) );
			AffineTransform translateTransform = AffineTransform.getTranslateInstance(getDrawTreeGraphics().getClipBounds().getX() + getDrawTreeGraphics().getClipBounds().getWidth()/2.0 - scaleTransform.getScaleX()/2.0, getDrawTreeGraphics().getClipBounds().getY());

			translateTransform.concatenate(scaleTransform);

			getNode().drawNodeAndLink(getDrawTreeGraphics(), getDrawTreeGraphics().getClipBounds().getHeight(), translateTransform, 1.0, 2.0);
		}
	}



}
