package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)NodeSettings.java
 *
 * Last Modified: 9/15/01
 */

 import java.util.*;
 import java.lang.*;
 import java.awt.*;
 import java.awt.font.*;
 import java.awt.geom.*;


/** * The class provides an object that refers to the settings of a node in a <code>Tree</code>.
 	* Many static variables are available for use as defaults and different settings. Additionally,
 	* a static method <code>getScheme</code> becomes very useful in acquiring the object with a
 	* given color scheme.
 	*
 	* <p> The class implements cloneable, making copies of the object possible. The copies are shallow copies
 	* which means changes to the specific elements could be dangerous when numerous instances refer
 	* to the same elements. For example, modifying the rule of the given AlphaComposite, modifies the rule
 	* for all <code>NodeSettings</code> that refer to that AlphaComposite. Therefore, it is suggested
 	* that no modifications to the specific elements be made. Instead, simply use a different element.
 	*
 	* @author  Corey Sanders
	* @version 1.5 9/15/01
 	*/
 public class NodeSettings extends Object implements Cloneable {

	/********************/
	/* Static variables */
	/********************/

	/**
	* Default <code>AlphaComposite</code> rule for node drawing (SRC_OVER).
	*/
	final public static int nodeRuleDefault = AlphaComposite.SRC_OVER;

	/**
	* Default <code>AlphaComposite</code> rule for link drawing (SRC_IN).
	*/
	final public static int linkRuleDefault = AlphaComposite.SRC_IN;

	/**
	* Default <code>AlphaComposite</code> alpha for node drawing (1.0).
	*/
	final public static float nodeAlphaDefault = 1.0F;

	/**
	* Default <code>AlphaComposite</code> alpha for node animating (0.8).
	*/
	final public static float nodeAlphaAnimateDefault = .8F;

	/**
	* Default <code>AlphaComposite</code> alpha for link drawing (0.8).
	*/
	final public static float linkAlphaDefault = .8F;

	/**
	* Default <code>AlphaComposite</code> alpha for link animating (0.6).
	*/
	final public static float linkAlphaAnimateDefault = .6F;


	/**
	 * Default <code>Stroke</code> for link drawing (BasicStroke(1)).
	 */
	final public static BasicStroke linkDefaultStroke = new BasicStroke(3.0f);

    final public static BasicStroke redLinkDefaultStroke = 
        new BasicStroke(6.0f);

	/**
	 * Default <code>Paint</code> for link drawing (PaintSettings.lightWhite)
	 */
	final public static Color linkDefaultColor = PaintSettings.lightWhite;

	/**
	 * Default <code>NodeShape</code> for node drawing (Ellipse2DNode)
	 */
	final public static NodeShape nodeDefaultShape = new Ellipse2DNode(0.0, 0.0, 1.0, 1.0);

	/**
	 * Default <code>Paint</code> for node drawing (PaintSettings.darkBlue)
	 */
	final public static Color nodeDefaultFillColor = PaintSettings.darkBlue;

	/**
	 * Default <code>Stroke</code> for node drawing (BasicStroke(1.5)).
	 */
	final public static BasicStroke nodeOutlineDefaultStroke = new BasicStroke(1.5f);

	/**
	 * Default <code>Paint</code> for node outline drawing (PaintSettings.darkBlue)
	 */
	final public static Color nodeOutlineDefaultColor = PaintSettings.darkBlue;

	/**
	 * Default <code>Composite</code> for node drawing (AlphaComposite(nodeRuleDefault, nodeAlphaDefault)).
	 */
	final public static AlphaComposite nodeCompositeDefault = AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault);

	/**
	 * Default <code>Composite</code> for node animating (AlphaComposite(nodeRuleDefault, nodeAlphaAnimateDefault)).
	 */
	final public static AlphaComposite nodeCompositeAnimateDefault = AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaAnimateDefault);

	/**
	 * Default <code>Composite</code> for link drawing (AlphaComposite(linkRuleDefault, linkAlphaDefault)).
	 */
	final public static AlphaComposite linkCompositeDefault = AlphaComposite.getInstance(linkRuleDefault, linkAlphaDefault);

	/**
	 * Default <code>Composite</code> for link animating (AlphaComposite(linkRuleDefault, linkAlphaAnimateDefault)).
	 */
	final public static AlphaComposite linkCompositeAnimateDefault = AlphaComposite.getInstance(linkRuleDefault, linkAlphaAnimateDefault);

	/**
	 * Default color scheme. (Uses the defaults of above).
	 */
	final public static String DEFAULT_SCHEME = "Default";

	/**
	 * Old Default color scheme.
	 */
	final public static String DEFAULT_SCHEME_2 = "Default 2";

	/**
	 * Erasing color scheme. (Background color and larger stroke widths).
	 */
	final public static String ERASE = "Erase";

	/**
	 * Princeton color scheme.
	 */
	final public static String PRINCETON_SCHEME = "Princeton";

	/**
	 * Black color scheme.
	 */
	final public static String BLACK_SCHEME = "Black";

	/**
	 * Ghost color scheme.
	 */
	final public static String GHOST_SCHEME = "Ghost";

	/**
	 * White color scheme.
	 */
	final public static String WHITE_SCHEME = "White";

	/**
	 * Christmas color scheme.
	 */
	final public static String CHRISTMAS_SCHEME = "Christmas";

	/**
	 * Tree color scheme.
	 */
	final public static String TREE_SCHEME = "Tree";

	/**
	 * Halloween color scheme.
	 */
	final public static String HALLOWEEN_SCHEME = "Halloween";


	/**
	 * Default Animating color scheme for the animator. (Uses the animation defaults of above).
	 */
	final public static String ANIMATION_SCHEME_1 = "Animation 1";

	/**
	 * Animating color scheme 2.
	 */
	final public static String ANIMATION_SCHEME_2 = "Animation 2";

	/**
	 * Animating color scheme 3.
	 */
	final public static String ANIMATION_SCHEME_3 = "Animation 3";

	/**
	 * Animating color scheme 4.
	 */
	final public static String ANIMATION_SCHEME_4 = "Animation 4";

	/**
	 * Animating color scheme 5.
	 */
	final public static String ANIMATION_SCHEME_5 = "Animation 5";

	/**
	 * Animator color scheme 1.
	 */
	final public static String ANIMATOR_SCHEME_1 = "Animator 1";


	/**
	 * Animator color scheme 2.
	 */
	final public static String ANIMATOR_SCHEME_2 = "Animator 2";

	/**
	 * Animator color scheme 3.
	 */
	final public static String ANIMATOR_SCHEME_3 = "Animator 3";

	/**
	 * Animator color scheme 4.
	 */
	final public static String ANIMATOR_SCHEME_4 = "Animator 4";

	/**
	 * Animator color scheme 5.
	 */
	final public static String ANIMATOR_SCHEME_5 = "Animator 5";



	/**
	 * Rotation Scheme 1.
	 */
	final public static String ROTATION_SCHEME_1 = "Rotation 1";

	/**
	 * Rotation Scheme 2.
	 */
	final public static String ROTATION_SCHEME_2 = "Rotation 2";

	/**
	 * Rotation Scheme 3.
	 */
	final public static String ROTATION_SCHEME_3 = "Rotation 3";

	/**
	 * Rotation Scheme 4.
	 */
	final public static String ROTATION_SCHEME_4 = "Rotation 4";

	/**
	 * String used in <code>getSettingName</code> to define an undefined settings.
	 */
	final public static String original = "Original";

	/**************/
	/* Variables  */
	/**************/

	/**
	 * String defining the type of NodeSettings the current node is.
	 */
	private String settingName = original;

	/**
	 * Right link stroke.
	 */
	private Stroke rightLinkStroke = linkDefaultStroke;

	/**
	 * Right link stroke width.
	 */
	private float rightLinkStrokeWidth = linkDefaultStroke.getLineWidth();

	/**
	 * Right link paint.
	 */
	private Paint rightLinkPaint = linkDefaultColor;

	/**
	 * left link stroke.
	 */
	private Stroke leftLinkStroke = linkDefaultStroke;

	/**
	 * left link stroke width.
	 */
	private float leftLinkStrokeWidth = linkDefaultStroke.getLineWidth();

	/**
	 * left link paint.
	 */
	private Paint leftLinkPaint = linkDefaultColor;

	/**
	 * Shape of the node.
	 */
	private NodeShape nodeShape	= nodeDefaultShape;

	/**
	 * Fill paint of the node.
	 */
	private Paint nodeFillPaint = nodeDefaultFillColor;

	/**
	 * Outline Stroke of the node.
	 */
	private Stroke nodeOutlineStroke = nodeOutlineDefaultStroke;

	/**
	 * Outline Stroke width of the node.
	 */
	private float nodeOutlineStrokeWidth = nodeOutlineDefaultStroke.getLineWidth();

	/**
	 * Outline Paint of the node.
	 */
	private Paint nodeOutlinePaint = nodeOutlineDefaultColor;


	/**
	 * Composite for node drawing.
     */
	private Composite nodeComposite = nodeCompositeDefault;

	/**
	 * Composite for right link drawing.
     */
	private Composite rightLinkComposite = nodeCompositeDefault;

	/**
	 * Composite for left link drawing.
     */
	private Composite leftLinkComposite = nodeCompositeDefault;


	/***************/
	/* Constructor */
	/***************/


	/**
	 * Constructor, contructs a default defined <code>NodeSettings</code>.
	 */
	public NodeSettings() {
		this.setScheme(DEFAULT_SCHEME);

	}

	/**
	 * Constructor, contructs a defined <code>NodeSettings</code> with the given scheme.
	 *
	 * @param int scheme, defining the scheme of this NodeSettings.
	 */
	public NodeSettings(String scheme) {
		this.setScheme(scheme);

	}

	/*******************/
	/* Static Methods  */
	/*******************/

	/**
	 * Gets a <code>NodeSettings</code> that defines the given int scheme. The scheme must be one
	 * defined within the class, otherwise the default scheme is returned.
	 *
	 * @param s defined color scheme within the class.
	 *
	 * @return <code>NodeSettings</code> that defines the given color scheme or default if no
	 * color scheme exists.
	 */
	public static NodeSettings getScheme(String s) {
		NodeSettings settings = new NodeSettings();
		settings.setScheme(s);
		return settings;
	}

	/**
	 * Gets a <code>NodeSettings</code> that defines the given int scheme. The scheme must be one
	 * defined within the class, otherwise the default scheme is returned.
	 *
	 * @param s defined color scheme within the class.
	 * @param background background color if needed.
	 *
	 * @return <code>NodeSettings</code> that defines the given color scheme or default if no
	 * color scheme exists.
	 */
	public static NodeSettings getScheme(String s, Color background) {
		NodeSettings settings = new NodeSettings();
		settings.setScheme(s, background);
		return settings;
	}

	/**
	 * Gets an array of String elements representing every NodeSettings choice available.
	 */
	 public static String[] getList() {
		String[] list = {DEFAULT_SCHEME, DEFAULT_SCHEME_2,BLACK_SCHEME, WHITE_SCHEME, TREE_SCHEME, CHRISTMAS_SCHEME, HALLOWEEN_SCHEME, GHOST_SCHEME, PRINCETON_SCHEME, ANIMATION_SCHEME_1,ANIMATION_SCHEME_2, ANIMATION_SCHEME_3, ANIMATION_SCHEME_4,
		ANIMATION_SCHEME_5, ANIMATOR_SCHEME_1, ANIMATOR_SCHEME_2, ANIMATOR_SCHEME_3, ANIMATOR_SCHEME_4, ANIMATOR_SCHEME_5, ROTATION_SCHEME_1, ROTATION_SCHEME_2, ROTATION_SCHEME_3, ROTATION_SCHEME_4};

		return list;

	}



	/************/
	/* Methods  */
	/************/



	/**
	 * Gets the string name for the current settings.
	 *
	 * @return String defining the settings name.
	 */
	 public String getSettingName() {
		 return settingName;
	 }

	/**
	 * Sets the string name for the current settings.
	 *
	 * @param settingName String defining the settings name.
	 */
	 public void setSettingName(String settingName) {
		 this.settingName = settingName;
	 }

	/**
	 * Sets the scheme of the settings, using Color.white as the default background color, if
	 * the background color is indeed needed.
	 *
	 * @param s defined color scheme within the class.
	 */
	public void setScheme(String s) {
		// Call to method.
		setScheme(s, Color.white);
	}

	/**
	 * Sets the scheme of the settings, using the background color if needed.
	 *
	 * @param s defined color scheme within the class.
	 * @param background background color if needed.
	 */
	public void setScheme(String s, Color background) {

		setSettingName(s);

		// Drawing Schemes
		if (s.equals(DEFAULT_SCHEME)) {
			setRightLinkStroke(rightLinkStroke);
			setRightLinkPaint(Color.black);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(leftLinkStroke);
			setLeftLinkPaint(Color.black);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.black);
			setNodeOutlineStroke(new BasicStroke(.7f));
			setNodeOutlinePaint(Color.darkGray);
			setNodeComposite(nodeCompositeDefault);

		}
		if (s.equals(DEFAULT_SCHEME_2)) {
			setRightLinkStroke(rightLinkStroke);
			setRightLinkPaint(PaintSettings.lightWhite);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(leftLinkStroke);
			setLeftLinkPaint(PaintSettings.lightWhite);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(PaintSettings.darkBlue);
			setNodeOutlineStroke(new BasicStroke(.5f));
			setNodeOutlinePaint(PaintSettings.darkBlue);
			setNodeComposite(nodeCompositeDefault);
		}


		if (s.equals(BLACK_SCHEME)) {
			setRightLinkStroke(rightLinkStroke);
			setRightLinkPaint(Color.darkGray);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(leftLinkStroke);
			setLeftLinkPaint(Color.darkGray);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.black);
			setNodeOutlineStroke(new BasicStroke(.5f));
			setNodeOutlinePaint(Color.darkGray);
			setNodeComposite(nodeCompositeDefault);
		}

		if (s.equals(BLACK_SCHEME)) {
			setRightLinkStroke(rightLinkStroke);
			setRightLinkPaint(Color.lightGray);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(leftLinkStroke);
			setLeftLinkPaint(Color.lightGray);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.white);
			setNodeOutlineStroke(new BasicStroke(.5f));
			setNodeOutlinePaint(Color.lightGray);
			setNodeComposite(nodeCompositeDefault);
		}

		if (s.equals(HALLOWEEN_SCHEME)) {
			setRightLinkStroke(new BasicStroke(1.6f));
			setRightLinkPaint(PaintSettings.darkOrange);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(new BasicStroke(1.6f));
			setLeftLinkPaint(PaintSettings.darkOrange);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.black);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.darkOrange);
			setNodeComposite(nodeCompositeDefault);
		}

		if (s.equals(TREE_SCHEME)) {
			setRightLinkStroke(new BasicStroke(1.6f));
			setRightLinkPaint(PaintSettings.brown);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(new BasicStroke(1.6f));
			setLeftLinkPaint(PaintSettings.brown);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.green);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.grayGreen);
			setNodeComposite(nodeCompositeDefault);
		}

		if (s.equals(CHRISTMAS_SCHEME)) {
			setRightLinkStroke(new BasicStroke(3f));
			setRightLinkPaint(Color.red);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkStroke(new BasicStroke(3f));
			setLeftLinkPaint(Color.red);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.red);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(Color.green);
			setNodeComposite(nodeCompositeDefault);
		}

		if (s.equals(PRINCETON_SCHEME)) {
			setRightLinkStroke(rightLinkStroke);
			setRightLinkPaint(PaintSettings.princetonOrange);
			setRightLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setLeftLinkStroke(leftLinkStroke);
			setLeftLinkPaint(PaintSettings.princetonOrange);
			setLeftLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(PaintSettings.princetonOrange);
			setNodeOutlineStroke(new BasicStroke(1.5f));
			setNodeOutlinePaint(PaintSettings.princetonOrange);
			setNodeComposite((Composite)AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
		}

		if (s.equals(GHOST_SCHEME)) {
			setRightLinkStroke(new BasicStroke(3f));
			setRightLinkPaint(PaintSettings.ghostlyGray);
			setRightLinkComposite(nodeCompositeAnimateDefault);
			setLeftLinkStroke(new BasicStroke(3f));
			setLeftLinkPaint(PaintSettings.ghostlyGray);
			setLeftLinkComposite(nodeCompositeAnimateDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(PaintSettings.ghostlyGray);
			setNodeOutlineStroke(new BasicStroke(1.5f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setNodeComposite(nodeCompositeAnimateDefault);
		}

		if (s.equals(ERASE)) {
			setRightLinkStroke(new BasicStroke(3f));
			setLeftLinkStroke(new BasicStroke(3f));
			setNodeOutlineStroke(new BasicStroke(3f));
			setRightLinkPaint(background);
			setRightLinkComposite(linkCompositeDefault);
			setLeftLinkPaint(background);
			setLeftLinkComposite(linkCompositeDefault);
			setNodeFillPaint(background);
			setNodeOutlinePaint(background);
			setNodeComposite(nodeCompositeDefault);

		}


		// Rotation Schemes


		if (s.equals(ROTATION_SCHEME_1)) {
			setRightLinkStroke(new BasicStroke(2f));
			//setRightLinkPaint(Color.darkGray);
			setRightLinkPaint(Color.pink);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setLeftLinkStroke(new BasicStroke(2f));
			//setLeftLinkPaint(Color.darkGray);
			setLeftLinkPaint(Color.pink);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.darkGray);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setNodeComposite(nodeCompositeAnimateDefault);
		}

		if (s.equals(ROTATION_SCHEME_2)) {
			setRightLinkStroke(new BasicStroke(2f));
			setRightLinkPaint(PaintSettings.lightGreen);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setLeftLinkStroke(new BasicStroke(2f));
			setLeftLinkPaint(PaintSettings.lightGreen);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(PaintSettings.grayBlue);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lightGreen);
			setNodeComposite(nodeCompositeAnimateDefault);
		}

		if (s.equals(ROTATION_SCHEME_3)) {
			setRightLinkStroke(new BasicStroke(2f));
			setRightLinkPaint(PaintSettings.grayRed);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setLeftLinkStroke(new BasicStroke(2f));
			setLeftLinkPaint(PaintSettings.grayRed);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(PaintSettings.indigo);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.grayRed);
			setNodeComposite(nodeCompositeAnimateDefault);
		}

		if (s.equals(ROTATION_SCHEME_4)) {
			setRightLinkStroke(new BasicStroke(2f));
			setRightLinkPaint(PaintSettings.lighterGray);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setLeftLinkStroke(new BasicStroke(2f));
			setLeftLinkPaint(PaintSettings.lighterGray);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setNodeShape(new Ellipse2DNode(0.0, 0.0, 1.0, 1.0));
			setNodeFillPaint(Color.blue);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setNodeComposite(nodeCompositeAnimateDefault);
		}

		// Animation Schemes
		if (s.equals(ANIMATION_SCHEME_1)) {
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(Color.black);
			setLeftLinkStroke(new BasicStroke(2.5f));
			//setLeftLinkPaint(Color.black);
			setLeftLinkPaint(Color.orange);
			setLeftLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setRightLinkStroke(new BasicStroke(2.5f));
			//setRightLinkPaint(Color.black);
			setRightLinkPaint(Color.orange);
			setRightLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));

		}

		if (s.equals(ANIMATION_SCHEME_2)) {
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.yellowGreen);
			setLeftLinkStroke(new BasicStroke(2.6f));
			setLeftLinkPaint(PaintSettings.lightGreen);
			setLeftLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setRightLinkStroke(new BasicStroke(2.6f));
			setRightLinkPaint(Color.pink);
			setRightLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));

		}

		if (s.equals(ANIMATION_SCHEME_3)) {
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.aqua);
			setLeftLinkStroke(new BasicStroke(1.5f));
			setLeftLinkPaint(PaintSettings.grayGreen);
			setLeftLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setRightLinkStroke(new BasicStroke(1.5f));
			setRightLinkPaint(PaintSettings.grayBlue);
			setRightLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));

		}

		if (s.equals(ANIMATION_SCHEME_4)) {
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.darkBlue);
			setLeftLinkStroke(new BasicStroke(2.3f));
			setLeftLinkPaint(PaintSettings.indigo);
			setLeftLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setRightLinkStroke(new BasicStroke(2.3f));
			setRightLinkPaint(Color.red);
			setRightLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));

		}
		if (s.equals(ANIMATION_SCHEME_5)) {
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setLeftLinkStroke(new BasicStroke(2.3f));
			setLeftLinkPaint(Color.blue);
			setLeftLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setRightLinkStroke(new BasicStroke(2.3f));
			setRightLinkPaint(Color.blue);
			setRightLinkComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaDefault));
			setNodeFillPaint(PaintSettings.darkBlue);
		}

		if (s.equals(ANIMATOR_SCHEME_1)) {
			setNodeFillPaint(Color.darkGray);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setNodeComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaAnimateDefault));
		}
		if (s.equals(ANIMATOR_SCHEME_2)) {
			setLeftLinkStroke(new BasicStroke(2.3f));
			setLeftLinkPaint(Color.blue);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setRightLinkStroke(new BasicStroke(2.3f));
			setRightLinkPaint(Color.red);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setNodeFillPaint(PaintSettings.lightGreen);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setNodeComposite(nodeCompositeAnimateDefault);
		}
		if (s.equals(ANIMATOR_SCHEME_3)) {
			setLeftLinkStroke(new BasicStroke(2.3f));
			setLeftLinkPaint(PaintSettings.darkBlue);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setRightLinkStroke(new BasicStroke(2.3f));
			setRightLinkPaint(PaintSettings.darkBlue);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setNodeFillPaint(PaintSettings.grayRed);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lightWhite);
			setNodeComposite(nodeCompositeAnimateDefault);
		}
		if (s.equals(ANIMATOR_SCHEME_4)) {
			setLeftLinkStroke(new BasicStroke(1.4f));
			setLeftLinkPaint(Color.black);
			setLeftLinkComposite(linkCompositeAnimateDefault);
			setRightLinkStroke(new BasicStroke(1.4f));
			setRightLinkPaint(Color.black);
			setRightLinkComposite(linkCompositeAnimateDefault);
			setNodeFillPaint(Color.darkGray);
			setNodeOutlineStroke(new BasicStroke(1f));
			setNodeOutlinePaint(Color.black);
			setNodeComposite(nodeCompositeAnimateDefault);
		}
		if (s.equals(ANIMATOR_SCHEME_5)) {
			setNodeFillPaint(PaintSettings.lightGreen);
			setNodeOutlineStroke(new BasicStroke(2f));
			setNodeOutlinePaint(PaintSettings.lighterGray);
			setNodeComposite(AlphaComposite.getInstance(nodeRuleDefault, nodeAlphaAnimateDefault));
		}

	}

	/**
	 * Sets the left settings of the NodeSettings, using the NodeSettings passed. The right
	 * settings remain untouched. Only affects the links.
	 *
	 * @param s NodeSettings to which the left settings are set, using the left settings.
	 */
	public void setLeftSettings(NodeSettings s) {
		setLeftLinkStroke(s.getLeftLinkStroke(), s.getLeftLinkStrokeWidth());
		setLeftLinkComposite(s.getLeftLinkComposite());
		setLeftLinkPaint(s.getLeftLinkPaint());
	}

	/**
	 * Sets the right settings of the NodeSettings, using the NodeSettings passed. The left
	 * settings remain untouched. Only affects the links.
	 *
	 * @param s NodeSettings to which the right settings are set, using the right settings.
	 */
	public void setRightSettings(NodeSettings s) {
		setRightLinkStroke(s.getRightLinkStroke(), s.getRightLinkStrokeWidth());
		setRightLinkComposite(s.getRightLinkComposite());
		setRightLinkPaint(s.getRightLinkPaint());
	}


	/**
	 * Sets the settings of just the node of the NodeSettings, using the NodeSettings passed. The link
	 * settings remain untouched. Only affects the node.
	 *
	 * @param s NodeSettings to which the node settings are set.
	 */
	public void setNodeSettings(NodeSettings s) {
		setNodeFillPaint(s.getNodeFillPaint());
		setNodeOutlineStroke(s.getNodeOutlineStroke(), s.getNodeOutlineStrokeWidth());
		setNodeOutlinePaint(s.getNodeOutlinePaint());
		setNodeComposite(s.getNodeComposite());
	}

	/**
	 * Sets the settings of just the node of the NodeSettings, using the NodeSettings passed. The link
	 * settings remain untouched. Only affects the node.
	 *
	 * @param s NodeSettings to which the node settings are set.
	 */
	public void setAllSettings(NodeSettings s) {
		setNodeFillPaint(s.getNodeFillPaint());
		setNodeOutlineStroke(s.getNodeOutlineStroke(), s.getNodeOutlineStrokeWidth());
		setNodeOutlinePaint(s.getNodeOutlinePaint());
		setNodeComposite(s.getNodeComposite());
		setRightLinkStroke(s.getRightLinkStroke(), s.getRightLinkStrokeWidth());
		setRightLinkComposite(s.getRightLinkComposite());
		setRightLinkPaint(s.getRightLinkPaint());
		setLeftLinkStroke(s.getLeftLinkStroke(), s.getLeftLinkStrokeWidth());
		setLeftLinkComposite(s.getLeftLinkComposite());
		setLeftLinkPaint(s.getLeftLinkPaint());
	}


 	/**
     * Returns a copy of this <code>NodeSettings</code> object.
     * @return an <code>Object</code> that is a copy of this
     * <code>NodeSettings</code> object.
     */
    public Object clone() {

		try {
		    return super.clone();
		}
		catch (CloneNotSupportedException e) {
		    // this shouldn't happen, since we are Cloneable
		    throw new InternalError();
		}
    }


	/**********************/
	/* Mutator Methods    */
	/**********************/

	/**
	 * Sets the right link stroke.
	 *
	 * @param s <code>Stroke</code> that sets the right link.
	 * @param w Width of the <code>Stroke</code> for drawing corrections.
	 */
	public void setRightLinkStroke(Stroke s, float w) {
		rightLinkStroke = s;
		rightLinkStrokeWidth = w;
	}

	/**
	 * Sets the right link stroke. No width is necessary, becuase
	 * BasicStroke defines a <code>getLineWidth</code> method.
	 *
	 * @param s <code>BasicStroke</code> that sets the right link.
	 */
	public void setRightLinkStroke(Stroke s) {
		rightLinkStroke = s;
		rightLinkStrokeWidth = ((BasicStroke)s).getLineWidth();
	}

	/**
	 * Sets the right link paint.
	 *
	 * @param p <code>Paint</code> for the right link.
	 */
	public void setRightLinkPaint(Paint p) {
		rightLinkPaint = p;
	}

	/**
	 * Sets the right composite. Generally an AlphaComposite is used.
	 *
	 * @param c <code>Composite</code> for drawing the right link.
	 */
	public void setRightLinkComposite(Composite c) {
		rightLinkComposite = c;
	}

	/**
	 * Sets the left link stroke.
	 *
	 * @param s <code>Stroke</code> that sets the left link.
	 * @param w Width of the <code>Stroke</code> for drawing corrections.
	 */
	public void setLeftLinkStroke(Stroke s, float w) {
		leftLinkStroke = s;
		leftLinkStrokeWidth = w;
	}

	/**
	 * Sets the left link stroke. No width is necessary, becuase
	 * BasicStroke defines a <code>getLineWidth</code> method.
	 *
	 * @param s <code>BasicStroke</code> that sets the left link.
	 */
	public void setLeftLinkStroke(Stroke s) {
		leftLinkStroke = s;
		leftLinkStrokeWidth = ((BasicStroke)s).getLineWidth();
	}

	/**
	 * Sets the left link paint.
	 *
	 * @param p <code>Paint</code> for the left link.
	 */
	public void setLeftLinkPaint(Paint p) {
		leftLinkPaint = p;
	}

	/**
	 * Sets the left composite. Generally an AlphaComposite is used.
	 *
	 * @param c <code>Composite</code> for drawing the left link.
	 */
	public void setLeftLinkComposite(Composite c) {
		leftLinkComposite = c;
	}

	/**
	 * Sets the <code>NodeShape</code> for the settings.
	 *
	 * @param s <code>NodeShape</code> for drawing the node.
	 */
	public void setNodeShape(NodeShape s) {
		nodeShape = s;
	}

	/**
	 * Sets the paint for filling the node.
	 *
	 * @param p <code>Paint</code> for filling the node.
	 */
	public void setNodeFillPaint(Paint p) {
		nodeFillPaint = p;
	}

	/**
	 * Sets the stroke for the node.
	 *
	 * @param s <code>Stroke</code> that sets the node stroke.
	 * @param w Width of the <code>Stroke</code> for drawing corrections.
	 */
	public void setNodeOutlineStroke(Stroke s, float w) {
		nodeOutlineStroke = s;
		nodeOutlineStrokeWidth = w;
	}

	/**
	 * Sets the node stroke. No width is necessary, becuase
	 * BasicStroke defines a <code>getLineWidth</code> method.
	 *
	 * @param s <code>BasicStroke</code> for drawing the key.
	 */
	public void setNodeOutlineStroke(BasicStroke s) {
		nodeOutlineStroke = s;
		nodeOutlineStrokeWidth = s.getLineWidth();
	}

	/**
	 * Sets the paint for the outline of the node.
	 *
	 * @param p <code>Paint</code> for drawing the outline of the node.
	 */
	public void setNodeOutlinePaint(Paint p) {
		nodeOutlinePaint = p;
	}

	/**
	 * Sets the node composite. Generally an AlphaComposite is used.
	 *
	 * @param c <code>Composite</code> for drawing the node.
	 */
	public void setNodeComposite(Composite c) {
		nodeComposite = c;
	}

	/**********************/
	/* Accessor Methods   */
	/**********************/


	/**
	 * Gets the left link stroke.
	 *
	 * @return <code>Stroke</code> of the left link.
	 */
	public Stroke getLeftLinkStroke() {
		return leftLinkStroke;
	}

	/**
	 * Gets the left link stroke width.
	 *
	 * @return double width of the left link.
	 */
	public float getLeftLinkStrokeWidth() {
		return leftLinkStrokeWidth;
	}

	/**
	 * Gets the left link paint.
	 *
	 * @return <code>Paint</code> of the left link.
	 */
	public Paint getLeftLinkPaint() {
		return leftLinkPaint;
	}

	/**
	 * Gets the left link composite.
	 *
	 * @return <code>Composite</code> of the left link.
	 */
	public Composite getLeftLinkComposite() {
		return leftLinkComposite;
	}

	/**
	 * Gets the right link stroke.
	 *
	 * @return <code>Stroke</code> of the right link.
	 */
	public Stroke getRightLinkStroke() {
		return rightLinkStroke;
	}

	/**
	 * Gets the right link stroke width.
	 *
	 * @return double width of the right link.
	 */
	public float getRightLinkStrokeWidth() {
		return rightLinkStrokeWidth;
	}

	/**
	 * Gets the right link paint.
	 *
	 * @return <code>Paint</code> of the right link.
	 */
	public Paint getRightLinkPaint() {
		return rightLinkPaint;
	}

	/**
	 * Gets the right link composite.
	 *
	 * @return <code>Composite</code> of the right link.
	 */
	public Composite getRightLinkComposite() {
		return rightLinkComposite;
	}

	/**
	 * Gets the <code>NodeShape</code> of the settings.
	 *
	 * @return <code>NodeShape</code> for the node.
	 */
	public NodeShape getNodeShape() {
		return nodeShape;
	}

	/**
	 * Gets the node fill paint.
	 *
	 * @return fill <code>Paint</code> of the node.
	 */
	public Paint getNodeFillPaint() {
		return nodeFillPaint;
	}

	/**
	 * Gets the outline stroke of the node.
	 *
	 * @return <code>Stroke</code> outline of the node.
	 */
	public Stroke getNodeOutlineStroke() {
		return nodeOutlineStroke;
	}

	/**
	 * Gets the outline stroke width of the node.
	 *
	 * @return double width of the outline of the node.
	 */
	public float getNodeOutlineStrokeWidth() {
		return nodeOutlineStrokeWidth;
	}

	/**
	 * Gets the node outline paint.
	 *
	 * @return outline <code>Paint</code> of the node.
	 */
	public Paint getNodeOutlinePaint() {
		return nodeOutlinePaint;
	}

	/**
	 * Gets the node composite.
	 *
	 * @return <code>Composite</code> of the node.
	 */
	public Composite getNodeComposite() {
		return nodeComposite;
	}



}
