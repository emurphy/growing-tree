package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)AffineTransformList.java
 *
 * NOTE: Could use more commenting, if you get more time.
 *
 * Last Modified: 9/01/02
 */

import java.beans.*; //Property change stuff
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Simply builds a JDialog to present the color options.
 *
 * @author  Corey Sanders
 * @version 1.0 9/01/02
 */

class ColorPreferencesDialog extends JDialog implements PropertyChangeListener {
	private boolean applied = true;
    private JOptionPane optionPane;

        final String applyAll = "Apply All";
        final String apply = "Apply";
        final String reset = "Reset";
        final String close = "Close";

	public boolean getApplied() {
		return applied;
	}

	public void setApplied(boolean applied) {
		this.applied = applied;
	}

	ColorOptionsJPanel panel = null;


    public ColorPreferencesDialog(Frame aFrame, ColorOptionsJPanel panel) {
        super(aFrame, true);

		this.panel = panel;

        setTitle("Color Preferences");


        Object[] options = {applyAll, apply, reset, close};

        optionPane = new JOptionPane(panel,
                                   JOptionPane.PLAIN_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null,
                                    options,
                                    options[3]);
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                 optionPane.setValue(close);
            }
        });


        optionPane.addPropertyChangeListener(this);
	}

    public void propertyChange(PropertyChangeEvent e) {
    	String prop = e.getPropertyName();


        if
        (isVisible()
        && (e.getSource() == optionPane)
        && (prop.equals(JOptionPane.VALUE_PROPERTY) ||
            prop.equals(JOptionPane.INPUT_VALUE_PROPERTY)))
        {

            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE) {
            //ignore reset
           		return;
            }

            // Reset the JOptionPane's value.
            // If you don't do this, then if the user
            // presses the same button next time, no
            // property change event will be fired.
            optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

            if (value.equals(applyAll)) {
				panel.optionAction(OptionEvent.SAVE_ALL);
				setApplied(true);
            }
            else if (value.equals(apply)) {
				panel.optionAction(OptionEvent.SAVE_TREE);
				setApplied(true);
			}
            else if (value.equals(reset)) {
				panel.optionAction(OptionEvent.RESET);
				setApplied(true);
            }
            else if (value.equals(close))  { // user closed dialog or clicked close
				if (getApplied() != true) {
					int returnValue = JOptionPane.showConfirmDialog(null, "Changes made. Apply them?", "Unsaved Changes", JOptionPane.YES_NO_OPTION);

            		if (returnValue == JOptionPane.YES_OPTION) {
						panel.optionAction(OptionEvent.SAVE_TREE);
					}
					setVisible(false);

				}
				else {
					setVisible(false);
				}

				panel.optionAction(OptionEvent.RESET);
            }
		}
	}

}
