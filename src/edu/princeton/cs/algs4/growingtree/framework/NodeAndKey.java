package edu.princeton.cs.algs4.growingtree.framework;


/**
 * An object which keeps both a BSTTree node and an integer key. Useful when dealing with
 * waitingActions that must involve both a node and key. This class makes that possible
 * within one object.
 */
public class NodeAndKey<P extends NodeProperties> {
    /**
     * BSTTree node
     */
    GrowingTreeNode<P> node;
    /**
     * key
     */
    int key;

    /**
     * Constructor, making an empty NodeAndKey Object. Using the set methods, it can be
     * set.
     */
    public NodeAndKey() {
        this(null, 0);
    }
    /**
     * Constructor, making a NodeAndKey Object with the speficied node and key.
     *
     * @param node BSTTree node for the current NodeAndKey.
     * @param key int for the NodeAndKey.
     */
    public NodeAndKey(GrowingTreeNode<P> node, int key) {
        setNode(node);
        setKey(key);
    }

    /**
     * Sets the node for the object.
     *
     * @param node BSTTree node for the object.
     */
    protected void setNode(GrowingTreeNode<P> node) {
        this.node = node;
    }

    /**
     * Sets the key for the object.
     *
     * @param key int for the object.
     */
    protected void setKey(int key) {
        this.key = key;
    }

    /**
     * Gets the node for the object.
     *
     * @return BSTTree node for the object.
     */
    public GrowingTreeNode<P> getNode() {
        return node;
    }

    /**
     * Gets the key for the object.
     *
     * @return int key for the object.
     */
    public int getKey() {
        return key;
    }
}




