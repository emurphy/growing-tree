package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)DrawingTreeHead.java
 *
 * Last Modified: 9/15/01
 */

 import java.util.*;
 import java.lang.*;
 import java.awt.*;
 import java.awt.font.*;
import java.awt.geom.*;


/**
 * The interface defines  numerous methods which allow for the drawing of
 * the entire <code>DrawingTree</code> onto a given Graphics2D using the head methods. The interface also defines many mutator and accesor
 * methods for information concerning the drawing of the entire <code>DrawingTree</code>
 *
 * @author Corey Sanders
 * @version 1.2 9/15/01
 */
public interface DrawingTreeHead<P extends NodeProperties> extends TreeHead<P> {

	/**
	 * Sets the <code>NodeSettings</code> for the entire tree from the head down.
	 * These settings are used for drawing the node and the links of each given tree.
	 *
	 * @param s <code>NodeSettings</code> for use in drawing the entire tree.
	 * @param k <code>KeySettings</code> for use in drawing the keys of the entire tree.
	 */
	public void setTreeSettings(NodeSettings s, KeySettings k);

	/**
	 * Gets the <code>NodeSettings</code> for the entire tree.
  	 *
	 * @return <code>NodeSettings</code> for defined for the entire tree.
	 */
	public NodeSettings getDrawingNodeSettings();


	/**
	 * Gets the <code>KeySettings</code> for the entire tree.
  	 *
	 * @return <code>KeySettings</code> for defined for the entire tree.
	 */
	 public KeySettings getDrawingKeySettings();

	/**
	 * Gets the width of the standard node within the tree. Every node is not necessarily the same
	 * width, but the standard is set within the Head.
	 *
	 * @return width of the standard node.
	 */
	public double getNodeWidth();

	/**
	 * Gets the height of the standard node within the tree. Every node is not necessarily the same
	 * height, but the standard is set within the Head.
	 *
	 * @return height of the standard node.
	 */
	public double getNodeHeight();


	/**
	 * Gets the bounds of the screen to which the tree is drawing. The bounds generally is simply
	 * the rectangle enclosing the Graphics2D passed.
	 *
	 * @return the rectangle representing the bounds of the screen.
	 */
	public Rectangle2D getScreenBounds();


	/**
	 * Sets the bounds of the screen to which the tree is drawing. Generally, these bounds
	 * are set using <code>getClipBounds</code> on the Graphics2D passed, however, the bounds
	 * can be set in any way.
	 *
	 * @param bounds the rectangle representing the bounds of the screen.
	 */
	public void setScreenBounds(Rectangle2D screen);

	/**
	 * Makes the entire tree from the null head down. The tree is made using the Graphics2D,
	 * and generally fills the entire Graphics2D parameter. This method allows the tree's
	 * drawing information to be defined without having to render the entire tree.
	 *
	 * @param g2 Graphics2D which the tree is made to fit unto.
	 */
	public void MakeTree(Graphics2D g2);

	/**
	 * Draws the entire tree from the null head down. The tree is drawn onto the Graphics2D,
	 * and uses the information defined in the previous call to <code>MakeTree</code>.
	 *
	 * @param g2 Graphics2D which the tree is drawn onto.
	 */
	public void DrawTree(Graphics2D g2);

	/**
	 * Finds the node represented by the x-y coordinates given. The (x,y) location represents a location
	 * within the Graphics2D to which the node appears. The recursive method progresses through the entire tree
	 * looking for the proper node.
	 *
	 * @param x x-coordinate to find the node.
	 * @param y y-coordinate to find the node.
	 *
	 * @return DrawingTree representing the x-y coordinates passed or null if no node is found.
	 */
	public DrawingTree<P> findNode(double x, double y);






}










