package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)DrawingTree.java
 *
 * Last Modified: 9/15/01
 */

 import java.util.*;
 import java.lang.*;
 import java.awt.*;
 import java.awt.font.*;
import java.awt.geom.*;


/**
 * The DrawingTree interface extends Tree because all Drawing Trees must also be Trees.<p>
 * The interface simply inforces the defining of numerous methods which allow for the drawing of
 * the <code>Tree</code> onto a given Graphics2D. The interface also defines many mutator and accesor
 * methods for information concerning the drawing of the <code>DrawingTree</code>
 *
 * @author Corey Sanders
 * @version 1.2 9/15/01
 */
public interface DrawingTree<P extends NodeProperties> extends Tree<P>  {

	/**
	 * Gets the drawing level for the current <code>DrawingTree</code>. This is a necessary method for allowing
	 * intermidiary drawing between two levels, because the param is a double, not an integer.
	 *
	 * @return the double level for the drawing node.
	 */
	public double getDrawingLevel();


	/**
	 * Gets the height of the section for the <code>DrawingTree</code>. The section height is generally
	 * used to determine link length and node height when drawing the tree.
	 *
	 * @return double height of the drawing section for the node.
	 */
	public double getSectionHeight();

	/**
	 * Gets the <code>AffineTransform</code> defined within the tree. The current transform
	 * is used when calling <code>draw</code> without the <code>AffineTransform</code> value.
	 *
	 * @return transform set for the current drawing of the tree.
	 */
	public AffineTransform getCurrentTransform();

	/**
	 * Gets the bounds of the screen to which the tree is drawing. The bounds generally is simply
	 * the rectangle enclosing the Graphics2D passed.
	 *
	 * @return the rectangle representing the bounds of the screen.
	 */
	public Rectangle2D getScreenBounds();

	/**
	 * Sets the bounds of the screen to which the tree is drawing. Generally, these bounds
	 * are set using <code>getClipBounds</code> on the Graphics2D passed, however, the bounds
	 * can be set in any way.
	 *
	 * @param bounds the rectangle representing the bounds of the screen.
	 */
	public void setScreenBounds(Rectangle2D bounds);

	/**
	 * Sets the <code>NodeSettings</code> of the tree.
	 * These settings are used for drawing the node and the links of the given tree.
	 *
	 * @param s <code>NodeSettings</code> for use in drawing the tree.
	 */
	public void setSettings(NodeSettings s);

	/**
	 * Gets the <code>NodeSettings</code> of the tree.
	 * These settings are used for drawing the node and the links of the given tree.
	 *
	 * @return <code>NodeSettings</code> for use in drawing the tree.
	 */
	public NodeSettings getSettings();



	/**
	 * Draws the node of the tree to the previously defined Graphics2D. The drawing uses
	 * the previously defined <code>AffineTransform</code>.
	 */
	public void drawNode();


	/**
	 * Draws the node of the tree to the given Graphics2D. The drawing uses
	 * the previously defined <code>AffineTransform</code>.
	 *
	 * @param g2 graphics to which the node is drawn.
	 */
	public void drawNode(Graphics2D g2);

	/**
	 * Draws the node of the tree to the given Graphics2D, using the <code>AffineTransform</code>.
	 * The drawing uses the <code>AffineTransform</code> to determine the exact way to draw the
	 * node.
	 *
	 * @param g2 graphics to which the node is drawn.
	 * @param a transform to draw the node.
	 */
	public void drawNode(Graphics2D g2, AffineTransform a);

	/**
	 * Draws the node and link of the tree to the previously defined Graphics2D.
	 * The drawing also uses the previously defined <code>AffineTransform</code>,
	 * section height, drawing level, and power level.
	 */
	public void drawNodeAndLink();


	/**
	 * Draws the node and link of the tree to the given Graphics2D.
	 * The drawing uses the previously defined <code>AffineTransform</code>,
	 * section height, drawing level, and power level.
	 *
	 * @param g2 graphics to which the node is drawn.
	 */
	public void drawNodeAndLink(Graphics2D g2);

	/**
	 * Draws the node and link of the tree to the given Graphics2D.
	 * The drawing generally uses the <code>AffineTransform</code>, section height,
	 * drawing level, and power level to render the node and its links.
	 *
	 * @param g2 graphics to which the node and links are drawn.
	 * @param sectionHieght the height of the tree' section, to draw the correct lengths for the links.
	 * @param a transfrom to draw the node and links.
	 * @param drawingLevel the level in the tree to which the node is currently being drawn.
	 * @param powerLevel the power to which the links extend, depending on how many links are present.
	 */
	public void drawNodeAndLink(Graphics2D g2, double sectionHeight, AffineTransform a, double drawingLevel, double powerLevel);


}
