package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)Ellipse2DNode.java
 *
 * Last Modified: 9/15/01
 */

 import java.awt.*;
 import java.awt.geom.*;

/**
 * <code>Ellipse2DNode</code> class implements NodeShape for it is an Ellipse NodeShape
 * that can be used as a node in a tree. <p>
 *
 * Because it is simply a Shape that also implements <tt>getInscribedRectangle</tt>, it therefore can
 * use all methods defined within Ellipse2DNode.<p>
 *
 * @see java.awt.geom.Ellipse2D
 * @see NodeShape
 *
 * @author Corey Sanders
 * @version 1.2 9/15/01
 */
 public class Ellipse2DNode extends Ellipse2D.Double implements NodeShape {

	/**
	 * Constructs a new <code>Ellipse2D</code>, initialized to
     * location (0,&nbsp;0) and size (0,&nbsp;0).
	 */
	public Ellipse2DNode() {
		super();
	}

	/**
	 * Constructs and initializes an <code>Ellipse2D</code> from the
     * specified coordinates.
	 * @param x,&nbsp;y the coordinates of the bounding rectangle
	 * @param w the width of the rectangle
	 * @param h the height of the rectangle
	 */
	public Ellipse2DNode(double x, double y, double w, double h) {
		super(x,y,w,h);
	}


	/**
	 * Get the Inscribed Rectangle of the current <code>Ellipse2DNode</code> according to the
	 * specifications of the <code>NodeShape</code> Interface.
	 *
	 * @return Rectangle2D that represents the inscribedRectangle of the <code>Ellipse2DNode</code>.
	 */
	public Rectangle2D getInscribedRectangle() {
		// x and y start at 1/4 the width in.
		double x = getX() + getWidth()/4.0;
		double y = getY() + getHeight()/4.0;

		// Width and Height are half of the node.
		double width = getWidth()/2.0;
		double height = getHeight()/2.0;

		return new Rectangle2D.Double(x,y,width,height);

	}

}