package edu.princeton.cs.algs4.growingtree.framework;
import java.awt.Color;
import java.awt.Stroke;

/**
 * 
 * This class hold the local state for a node. It can be extended by client code 
 * to include more properties. It also provides callbacks to allow for limited
 * control over the animation.
 * 
 * @author Josh Israel
 *
 */

public class NodeProperties implements Cloneable {
	public static final int NULL_HEIGHT = -1;
	private int size = 1;
	private int height = NULL_HEIGHT + 1;
	
	/**
     * Returns a copy of this <code>NodeProperties</code> object.
     * @return an <code>Object</code> that is a copy of this
     * <code>NodeProperties</code> object.
     */
    public Object clone() {

		try {
		    return super.clone();
		}
		catch (CloneNotSupportedException e) {
		    // this shouldn't happen, since we are Cloneable
		    throw new InternalError();
		}
    }
	
	
	/**
	 * This functions as a factory for NodeProperty objects. It is
	 * called whenever a new node is created (for insertion into the tree).
	 * If extended, this MUST be overridden to return an instance of the subclass 
	 * as a result of some of the quirks of Java generics.
	 * 
	 * @return An instance of this class. Subclasses should return an instance
	 * of the subclass.
	 */
	public NodeProperties makeDefaultProperties() {
		return new NodeProperties();
	}
	
	public NodeProperties() { }
	
	
	public int getHeight() { return height; }
	/**
	 * The height field of NodeProperties is maintained
	 * by the framework, so this should not be called
	 * by the client
	 * @param i The new height of the tree
	 */
	public void setHeight(int i) { height = i; }
	public int getSize() { return size; }
	/**
	 * The size field of NodeProperties is maintained
	 * by the framework, so this should not be called
	 * by the client
	 * @param i The new height of the tree
	 */
	public void setSize(int i) { size = i; }
	
	/**
	 * @return The color this node should be drawn. If colorParentLink
	 * returns true, then the parent link will be drawn this color and 
	 * the node will be black.
	 */
	public Color getNodeColor() { return Color.black; }
	
	/**
	 * Determines whether the node itself is colored or its parent link
	 * @return True if getNodeColor should be used for the parent link
	 */
	public boolean colorParentLink() { return false; }
	/**
	 * @return The color the left link should be drawn. Ignored if
	 * colorParentLink returns true.
	 */
	public Color getLeftLinkColor() { return Color.black; }
	/**
	 * @return The color the right link should be drawn. Ignored if
	 * colorParentLink returns true;
	 */
	public Color getRightLinkColor() { return Color.black; }
	/**
	 * @return The value the upper right integer field should contain, or null
	 * if nothing should be drawn. This is not to be confused with the actual 
	 * key value of the node. It is to be used for values like height or rank.
	 */
	public Integer getULIntegerFieldValue() { return getSize(); }
	/**
	 * @return The value the upper right integer field should contain, or null
	 * if nothing should be drawn. This is not to be confused with the actual 
	 * key value of the node. It is to be used for values like height or rank.
	 */
	public Integer getURIntegerFieldValue() { return null; }
	/**
	 * @return The value the lower left integer field should contain, or null
	 * if nothing should be drawn. This is not to be confused with the actual 
	 * key value of the node. It is to be used for values like height or rank.
	 */
	public Integer getLLIntegerFieldValue() { return null; }
	/**
	 * @return The value the lower right integer field should contain, or null
	 * if nothing should be drawn. This is not to be confused with the actual 
	 * key value of the node. It is to be used for values like height or rank.
	 */
	public Integer getLRIntegerFieldValue() { return null; }
	/**
	 * @return The color the integer field should be drawn; 
	 */
	public Color getIntegerFieldColor() { return PaintSettings.aqua; }
	
}
