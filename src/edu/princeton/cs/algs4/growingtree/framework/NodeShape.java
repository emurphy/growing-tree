package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)NodeShape.java
 *
 * Last Modified: 9/15/01
 */

 import java.awt.*;
 import java.awt.geom.*;

/**
 * The NodeShape interface extends Shape for it is a specific type of
 * shape that can be used as a node in addition to the regular shape methods. <p>
 *
 * For usage as a node in a tree, the <code>NodeShape</code> must implement the ability to get an Inscribed
 * Rectangle, which is the only additional method this interface requires. <p>
 *
 * The interface is only valid for Binary trees, where only a single inscribedRectangle is
 * required.
 *
 * @see Ellipse2DNode
 *
 * @author Corey Sanders
 * @version 1.1 9/15/01
 */
public interface NodeShape extends Shape {
	/**
	 * Gets the Inscribed Rectangle of the current <code>NodeShape</code>. The Rectangle is
	 * for a single key.
	 *
	 * @return Rectangle2D that represents the inscribed rectangle.
	 */
	public Rectangle2D getInscribedRectangle();
}

