package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)NodeDrawingJPanel.java
 *
 * Last Modified: 9/15/01
 */
import javax.swing.*;
import javax.swing.event.*;


import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.beans.*;

/**
 * A simple class that extends JPanel and draws the given node within the Panel. The only methods
 * used are to set the DrawingTree for the panel and to draw the tree so it fits perfectly within the panel.
 * <p>
 * @author  Corey Sanders
 * @version 1.4 9/15/01
 */
public class NodeDrawingJPanel<P extends NodeProperties> extends DrawingJPanel {

	/**
	 * TreeHead kept for the current Panel.
	 */
	private DrawingTree<P> node;


	/**
	 * Sole Constructor for the JPanel that paints the node using the AnimatingTree drawNode method.
	 * The JPanel only makes a new image on a resize and otherwise, double-buffers the graphics by
	 * leaving the image unmodified.
	 */
	public NodeDrawingJPanel() {
		super();

    }


	/**
 	 ********************
	 * Accesssor methods*
	 ********************
	 */

    /**
     * Gets the head of the tree currently drawn in the Panel.
     *
     * @return TreeHead the tree head.
     */
	public DrawingTree<P> getNode() {
		return node;
	}


	/**
 	 *******************
	 * Mutator methods *
	 *******************
	 */

	/**
	 * Set the node for drawing. This method should be called for each change in node.
	 * @param node node used for drawing within the panel.
	 */
    public void setNode(DrawingTree<P> node) {
		this.node = node;
		setDrawTree(true);
		repaint();
	}


	/**
 	 *******************
	 * Drawing methods *
	 *******************
	 */

	/**
	 * Method actually called to complete the drawing of the panel. The node is drawn to fill the
	 * entire graphics given within the panel.
	 */
	protected void draw() {
		if (getNode() != null) {
			AffineTransform scaleTransform = AffineTransform.getScaleInstance(getDrawTreeGraphics().getClipBounds().getWidth() , getDrawTreeGraphics().getClipBounds().getHeight());

			getNode().drawNode(getDrawTreeGraphics(), scaleTransform);
		}
	}

}
