package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)OptionListener.java
 *
 * Last Modified: 9/15/01
 */

import java.awt.*;
import java.util.*;

/**
 * The listener interface for receiving Option events.
 * The class that is interested in processing an Option event
 * implements this interface, and the object created with that
 * class is registered with a Component, using the object's
 * <code>addOptionListener</code> method. When the action event
 * occurs, that object's <code>optionEventPerformed</code> method is
 * invoked.
 *
 * @see OptionEvent
 *
 * @author Corey Sanders
 * @version 1.1 9/15/01
 */
public interface OptionListener extends EventListener {
	/**
     * Invoked when an Option event occurs.
     */
	public void optionEventPerformed(OptionEvent e);
}