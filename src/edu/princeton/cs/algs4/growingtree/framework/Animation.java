package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)Animation.java
 *
 * Last Modified: 9/15/01
 */

 import java.awt.*;


/** *
 	* The Animation interface that must be used for all Animations. All methods must be
 	* implemented to allow the Animation Object to be used in all animation applications. <p>
 	*
 	* Status fields are provided for usage in Animation objects involving status and mesages. Others may be added if necessary.
    *
 	* @author  Corey Sanders
	* @version 1.5 9/15/01
 	*/


public interface Animation {


	/**
     * Constant for the BEGIN status.
     */
	public static final String BEGIN = "Begin";

	/**
	 * Constant for the STOP status.
     */
	public static final String STOP = "Stop";
	/**
     * Constant for the PLAY status.
     */
	public static final String PLAY = "Play";
	/**
     * Constant for the REVERSE status.
     */
	public static final String REWIND = "Rewind";
	/**
     * Constant for the PAUSE status.
     */
	public static final String PAUSE = "Pause";
	/**
     * Constant for the FINISH status.
     */
	public static final String FINISH = "Finish";
	/**
     * Constant for the REDRAW status.
     */
	public static final String REDRAW = "Redraw";
	/**
     * Constant for the STEP status.
     */
	public static final String STEP = "Step";
	/**
     * Constant for the MESSAGE status.
     */
	public static final String ANIMATION_MESSAGE = "Message";



	/**
	 * Sets the step size for the animation.
	 *
	 * @param t the step size
	 */
	public void setStepTime(int t);

	/**
	 * Gets the step size for the animation.
	 *
	 * @return int step size
	 */
	public int getStepTime();

	/**
	 * Adds an animationListener that recieves meaningful events from the animation, according to
	 * the Animation interface and the <code>AnimationEvent</code>.
	 *
	 * @param l the listener for the AnimationEvents occuring within this Animation.
	 */
	public void addAnimationListener(AnimationListener l);

	/**
	 * Removes an animationListener from the animation, according to
	 * the Animation interface and the <code>AnimationEvent</code>.
	 *
	 * @param l the listener removed from recieving the AnimationEvents occuring within this Animation.
	 */
	public void removeAnimationListener(AnimationListener l);

	/**
	 * Adds a description that may be used to describe to the listener the type of event occuring.
	 * The value of teh description may be retrieved through <code>getDescription</code>.
 	 *
 	 * @param d the string defining the description.
	 */
	public void addDescription(String d);

	/**
	 * Gets the description added with <code>addDescription</code> and should be accessed through the listener.
 	 *
 	 * @return the string defining the description.
	 */
	public String getDescription();

	/**
	 * Sets whether the current animation is in stepping mode or not. Step mode indicates
	 * skipping the intermediary drawings in the animation and going instantly from one step
	 * to the next. Generally useful for fast-forward.
 	 * @param b boolean defining whether it is skipping.
	 */
	public void setStep(boolean b);

	/**
	 * Gets whether the current animation is in stepping mode or not. Step mode indicates
	 * skipping the intermediary drawings in the animation and going instantly from one step
	 * to the next. Generally useful for fast-forward.
	 *
 	 * @return boolean defining whether it is skipping.
	 */
	public boolean getStep();

	/**
	 * Sets the status of the Animation using a command within <code>Animation</code> interface.
	 *
	 * @param cmd cmd that the Animation's status is set to.
	 */
	public void setStatus(String cmd);


	/**
	 * Gets the status of the Animation using a command within <code>Animation</code> interface.
	 *
	 * @return the Animation's status.
	 */
	public String getStatus();


	/**
     * Draws the animation of the next step, using the status of the animation (Animation.PLAY, Animation.PAUSE and so forth).
	 * After completing the drawing, the Animation sends an AnimationEvent to all its listeners, indicating
	 * any information that the listerners may wish to use. <p>
	 * The starting status of the animation is defined as <code>Animation.PLAY</code>
 	 *
 	 * @param b boolean defining whether it is skipping.
	 */
	public void drawAnimation(Graphics2D g2);

	/**
	 * Draws the animation of the next step, using the status of the animation (Animation.PLAY, Animation.PAUSE and so forth).
	 * After completing the drawing, the Animation sends an AnimationEvent to all its listeners, indicating
	 * any information that the listerners may wish to use.
	 *
	 * @param g2 the graphics to which the animation step should be drawn.
	 * @param status the starting status of the animation (what the animation is set to, if in a starting phase like BEGIN or STEP).
	 */
	public void drawAnimation(Graphics2D g2, String status);


}