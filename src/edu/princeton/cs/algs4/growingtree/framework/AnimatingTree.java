package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)AnimatingTree.java
 *
 * Last Modified: 9/15/01
 */

 import java.util.*;
 import java.awt.*;
import java.awt.geom.*;



/**
 * The AnimatingTree interface extends DrawingTree because all AnimatingTrees must be drawable.<p>
 * The interface simply inforces the defining of numerous methods which allow for the animating of
 * the <code>Tree</code> onto a given Graphics2D. The interface also defines many mutator and accesor
 * methods for information concerning the animating of the <code>AnimatingTree</code>
 *
 * @author Corey Sanders
 * @version 1.3 9/15/01
 */
public interface AnimatingTree<P extends NodeProperties> extends DrawingTree<P> {


	/**
	 * Returns true if the node is animating. It simply determines if the list of animators is empty.
	 *
	 * @return true if the node is currently animating.
	 */
	public boolean isNodeAnimating();

	/**
	 * Adds an animation to the current node. This method does not add the node to listen for
	 * AnimationEvents. That must be performed by the user.
	 *
	 * @param a the <code>Animation</code> being added to the node.
	 */
	public void addAnimator(Animation a);

	/**
	 * Gets the first <code>Animation</code> of the node.
	 *
	 * @return Animation which is the first Animation of the node.
	 */
	public Animation getAnimator();

}