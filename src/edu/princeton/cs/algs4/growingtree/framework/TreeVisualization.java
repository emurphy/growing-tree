package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)TreeJApplication.java
 *
 * Last Modified: 9/01/02
 */

import javax.swing.*;
import javax.swing.border.*;

import edu.princeton.cs.algs4.growingtree.demos.BSTDeletion;
import edu.princeton.cs.algs4.growingtree.demos.BSTInsertion;
import edu.princeton.cs.algs4.growingtree.demos.BSTSearch;
import edu.princeton.cs.algs4.growingtree.demos.LLRBDeletion;
import edu.princeton.cs.algs4.growingtree.demos.LLRBInsertion;
import edu.princeton.cs.algs4.growingtree.demos.RBNodeProperties;
import edu.princeton.cs.algs4.growingtree.demos.SplayOperators;
import edu.princeton.cs.algs4.growingtree.experiments.IExperimentLogger;
import edu.princeton.cs.algs4.growingtree.interfaces.IDeleteOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.IInsertOperator;
import edu.princeton.cs.algs4.growingtree.interfaces.ISearchOperator;

import java.awt.*;
import java.awt.event.*;
import java.net.*;

/**
 * The TreeJApplet presents an applet that allows for the drawing
 * of different forms of tress.
 * <p>
 * @author  Corey Sanders
 * @version 2.5 9/01/02
 */

public class TreeVisualization implements WindowListener {


    public static final String VERSION = "3.0";

	public static final String PROGRAM_INFORMATION =
	"Growing Tree!\n"+
    "Version " + VERSION + "\n\n" + 
	"An educational applet that presents different types of searching trees\n"+
	"and animates the numerous manipulating methods available for the specific\n"+
	"type of tree.\n\n"+
	"The program currently supports Binary Search Trees including Red-Black Trees,\n"+
	"Splay Trees, Root-Insertion Trees, Balanced Trees, and Left-Leaning Red Black Trees.\n" +   
    "It supports insertion, deletion, searching, and selection. Furthermore, clicking\n" + 
    "upon nodes supports individualized functions.\n\n\n"+
	"Questions or Modification ideas, please notify the creators.\n";

	public static final String LICENSE_INFORMATION =
    "Version " + VERSION + "\n" +
	"Currently Unlicensed\n\n"+
	"Enjoy free use of this program.\n\n\n";


	public static final String CREATOR_INFORMATION =
	"Authors : Corey Michael Sanders (csanders@princeton.edu) and Jeffrey Hodes (jhodes@princeton.edu)\n"+
	"Supervisors : Professor Robert Sedgewick (rs@cs.Princeton.edu) of Princeton University\n"+
	"and Professor Kevin Wayne (wayne@cs.Princeton.edu) of Princeton University \n\n"+
	"Final Modification Date : 8/6/10 by Jeffrey Hodes\n\n";

	//protected JApplet current = this;

    /**
     * Creates new Applet
     */
    public static void main(String[] args) {


    }


	JWindow splashScreen;
	CenterJFrame centerFrame;
    public TreeVisualization() {
    	centerFrame = new CenterJFrame();
    }
    
    public <P extends NodeProperties> void addTree(String name, P p,
			   IInsertOperator<P> inserter,
			   ISearchOperator<P> searcher,
			   IDeleteOperator<P> deleter, boolean allowRotations) {
    	centerFrame.treePane.addTree(name, p, inserter, searcher, deleter, allowRotations);
    }
    
    public <P extends NodeProperties> void addTree(String name, P p,
			   IInsertOperator<P> inserter,
			   ISearchOperator<P> searcher,
			   IDeleteOperator<P> deleter) {
    	addTree(name, p, inserter, searcher, deleter, false);
    }
    
    public <P extends NodeProperties> void addTree(String name, P p,
			   IInsertOperator<P> inserter,
			   ISearchOperator<P> searcher,
			   IDeleteOperator<P> deleter,
			   IExperimentLogger<P> logger,
			   boolean allowRotations) {
    	centerFrame.treePane.addTree(name, p, inserter, searcher, deleter, logger, allowRotations);
    }
    
    public <P extends NodeProperties> void addTree(String name, P p,
			   IInsertOperator<P> inserter,
			   ISearchOperator<P> searcher,
			   IDeleteOperator<P> deleter,
			   IExperimentLogger<P> logger) {
    	addTree(name, p, inserter, searcher, deleter, logger, false);
    }
    	
    public void start() {
		//Construct centerFrame.
		//CenterJFrame centerFrame = new CenterJFrame();
    	centerFrame.start();
		centerFrame.setSize(900,700);
		centerFrame.setLocation(60,0);
		centerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // equivalent to deprecated method show()
		centerFrame.setVisible(true);

		splashScreen = new JWindow(centerFrame);

		SplashPanel splashPanel = new SplashPanel();

		splashPanel.setBorder(BorderFactory.createLineBorder(Color.black));

		splashScreen.setSize(648,432);
		splashScreen.setLocation(200,170);

		splashScreen.getContentPane().add(splashPanel, BorderLayout.CENTER);

        // equivalent to deprecated method show()
		splashScreen.setVisible(true);

		try {
		    Thread.sleep(2000);
        }
        catch (InterruptedException e){

        }

		splashScreen.dispose();

	}




 	////////////////////////////
	// Implements Window Listener
	////////////////////////////


	/**
	 * Invoked when the Window is set to be the active Window.
	 */
	 public void  windowActivated(WindowEvent e) {
	 }

	/**
	 * Invoked when a window has been closed as the result of calling dispose on the window.
	 */
	 public void  windowClosed(WindowEvent e) {
		 System.exit(0);
	 }


	 /**
	  *  Invoked when the user attempts to close the window from the window's system menu.
	  */
	 public void windowClosing(WindowEvent e) {
		System.exit(0);
	 }


	 /**
	  * Invoked when a Window is no longer the active Window.
	  */
	 public void windowDeactivated(WindowEvent e) {
	 }


	 /**
	  *  Invoked when a window is changed from a minimized to a normal state.
	  */
	 public void windowDeiconified(WindowEvent e) {
	 }


	 /**
	  *  Invoked when a window is changed from a normal to a minimized state.
	  */
	 public void windowIconified(WindowEvent e) {
	 }


	 /**
	  * Invoked the first time a window is made visible.
	  */
	 public void windowOpened(WindowEvent e)  {
	 }

}
