package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)CenterJFrame.java
 *
 * Last Modified: 9/02/01
 *
 *
 */


import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.net.*;
import java.io.File;
import java.util.*;

/**
 * Center JPanel for use with the <code>TreeApplet</code>. Within this JPanel, the two children panels,
 * <code>TreeJPanel</code> and <code>OptionJPanel</code> are made and all data is stored. Methods called from those
 * two children operate through this JPanel.
 *
 * <p> Synchronization occurs within this JPanel to ensure no access and modification errors
 * occur.
 *
 * <p> Each Tree object is maintained in this JPanel.
 *
 * <p>
 * @author  Corey Sanders
 * @version 1.2	9/02/01
 * @see TreeJApplet
 * @see TreeJPanel
 * @see OptionJPanel
 */

public class CenterJFrame extends JFrame implements ActionListener, ItemListener, TreeMessageListener {

    // list of options for Load menu
    public static final String[] loadKeyList = 
        {    
            // "9 5 1 0 8 7",
            "S E A R C H X M P L",
            "L P M X H C R A E S",
            "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30",
            "30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0",
            "0.0 1.0 0.5 0.25 0.75",
            "0.0 0.1 0.2 0.3 0.4 0.5 0.55 0.45 0.35 0.25 0.15 0.05"
        };

	/**
	 * Id for the control panel.
	 */
	protected static int id = OptionEvent.CENTER_FRAME;

	/**
	 * Listeners for the Panel. Thye listen to the optionEvents made.
	 */
	protected LinkedList<OptionListener> listeners;

	/**
	 * Tree Panel which stores the data for the each tree.
	 */
	TreeJDesktop treePane;

   	/**
   	 * Option Pane, representing the options for the tree.
   	 */
    OptionMainJPanel optionPane;

    /**
     * Menu Bar, representing the menu of the Application.
     */
     JMenuBar menuBar;

 	/**
 	 * Zoom count, how far the zoom has gone.
 	 */
	private int zoomCount = 0;

	/**
	 * The menus used in this Frame.
	 */
	private JMenu fileMenu, viewMenu, toolMenu, helpMenu;
	/**
	 * The radio menus used in this Frame.
	 */
	private JRadioButtonMenuItem integerTypeRadioItem, characterTypeRadioItem, doubleTypeRadioItem;
	/**
	 * The check box menus used in this Frame.
	 */
	private JCheckBoxMenuItem subtreeCountCheckItem, animationMenuCheckItem, toolBarPanelCheckItem, nodePanelMenuCheckItem;


    /**
	 * Constructs a new <code>CenterJFrame</code>. This constructor
	 * calls upon the TreePanel class and OptionPanel class to construct
	 * the separate parts of the CenterJFrame.
     */
    public CenterJFrame() {
    	treePane = new TreeJDesktop();
    }
    
    public void start() {
		setTitle("Growing Tree");

		setIconImage(getImage("GTIcon.jpg"));

		listeners = new LinkedList<OptionListener>();

		Container contents = getContentPane();

		contents.setBackground(Color.white);

		contents.setLayout(new BorderLayout());

		treePane.init();

		optionPane = new OptionMainJPanel();

		contents.add(treePane, BorderLayout.CENTER);
		contents.add(optionPane, BorderLayout.NORTH);

		optionPane.addOptionListener(treePane);
		this.addOptionListener(treePane);

		treePane.addMouseListener(optionPane);
		treePane.addMouseMotionListener(optionPane);

		treePane.addTreeMessageListener(optionPane);
		treePane.addTreeMessageListener(this);

		treePane.initialize();

		constructMenu();
	}

	/**
	 * Constructs the tool menu for the given CenterJFrame.
	 */
	public void constructToolMenu() {
		JMenu traversalMenu, balanceMenu;
		JMenuItem preorderItem,inorderItem, postorderItem, levelorderItem;
		JMenuItem balanceTreeItem,balanceAllItem;
		JMenuItem constructTreeItem;

		//Build the tools menu.
		toolMenu = new JMenu("Tools");
		toolMenu.setMnemonic(KeyEvent.VK_T);
		toolMenu.getAccessibleContext().setAccessibleDescription("Tools Menu");

		//Build the traversal menu.
		traversalMenu = new JMenu("Construct Traversal");
		traversalMenu.setMnemonic(KeyEvent.VK_C);
		traversalMenu.getAccessibleContext().setAccessibleDescription("Construct Traversal");

		//Build the preorder item.
		preorderItem = new JMenuItem("Preorder", KeyEvent.VK_P);
		preorderItem.getAccessibleContext().setAccessibleDescription("Preorder");
		preorderItem.addActionListener(this);
		preorderItem.setActionCommand(OptionEvent.PREORDER_TRAVERSAL);

		//Build the inorder item.
		inorderItem = new JMenuItem("Inorder", KeyEvent.VK_I);
		inorderItem.getAccessibleContext().setAccessibleDescription("Inorder");
		inorderItem.addActionListener(this);
		inorderItem.setActionCommand(OptionEvent.INORDER_TRAVERSAL);

		//Build the postorder item.
		postorderItem = new JMenuItem("Postorder", KeyEvent.VK_S);
		postorderItem.getAccessibleContext().setAccessibleDescription("Postorder");
		postorderItem.addActionListener(this);
		postorderItem.setActionCommand(OptionEvent.POSTORDER_TRAVERSAL);

		//Build the levelorder item.
		levelorderItem = new JMenuItem("Level-order", KeyEvent.VK_L);
		levelorderItem.getAccessibleContext().setAccessibleDescription("Level-order");
		levelorderItem.addActionListener(this);
		levelorderItem.setActionCommand(OptionEvent.LEVELORDER_TRAVERSAL);

		traversalMenu.add(preorderItem);
		traversalMenu.add(inorderItem);
		traversalMenu.add(postorderItem);
		traversalMenu.add(levelorderItem);


		//Build the traversal menu.
		balanceMenu = new JMenu("Balance");
		balanceMenu.setMnemonic(KeyEvent.VK_B);
		balanceMenu.getAccessibleContext().setAccessibleDescription("Balance Menu");

		//Build the balanceTree item.
		balanceTreeItem = new JMenuItem("Balance Selected", KeyEvent.VK_S);
		balanceTreeItem.getAccessibleContext().setAccessibleDescription("Balance Selected");
		balanceTreeItem.addActionListener(this);
		balanceTreeItem.setActionCommand(OptionEvent.BALANCE);

		//Build the balanceTree item.
		balanceAllItem = new JMenuItem("Balance All", KeyEvent.VK_A);
		balanceAllItem.getAccessibleContext().setAccessibleDescription("Balance All");
		balanceAllItem.addActionListener(this);
		balanceAllItem.setActionCommand(OptionEvent.BALANCE_ALL);

		balanceMenu.add(balanceTreeItem);
		balanceMenu.add(balanceAllItem);


		//Build the preorder item.
		constructTreeItem = new JMenuItem("Construct Tree", KeyEvent.VK_T);
		constructTreeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.ALT_MASK));
		constructTreeItem.getAccessibleContext().setAccessibleDescription("Randomly Construct Tree");
		constructTreeItem.addActionListener(this);
		constructTreeItem.setActionCommand(OptionEvent.RANDOM_TREE);

		toolMenu.add(traversalMenu);
		toolMenu.add(balanceMenu);
		toolMenu.addSeparator();
		toolMenu.add(constructTreeItem);

	}


	/**
	 * Constructs the help menu for the given CenterJFrame.
	 */
	public void constructHelpMenu() {
		JMenuItem treeInformationItem,programInformationItem, aboutItem;

		//Build the help menu.
		helpMenu = new JMenu("Help");
		helpMenu.setMnemonic(KeyEvent.VK_H);
		helpMenu.getAccessibleContext().setAccessibleDescription("Help Menu");


		//Build the TreeInfo item.
		treeInformationItem = new JMenuItem("Tree Information", KeyEvent.VK_T);
		treeInformationItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.ALT_MASK));
		treeInformationItem.getAccessibleContext().setAccessibleDescription("Tree Information");
		treeInformationItem.addActionListener(this);
		treeInformationItem.setActionCommand(OptionEvent.TREE_INFORMATION);

		//Build the TreeInfo item.
		programInformationItem = new JMenuItem("Program Information", KeyEvent.VK_P);
		programInformationItem.getAccessibleContext().setAccessibleDescription("Program Information");
		programInformationItem.addActionListener(this);
		programInformationItem.setActionCommand(OptionEvent.PROGRAM_INFORMATION);


		//Build the TreeInfo item.
		aboutItem = new JMenuItem("About Growing Tree...", KeyEvent.VK_A);
		aboutItem.getAccessibleContext().setAccessibleDescription("About Growing Tree...");
		aboutItem.addActionListener(this);
		aboutItem.setActionCommand(OptionEvent.ABOUT);

		helpMenu.add(treeInformationItem);
		helpMenu.add(programInformationItem);
		helpMenu.addSeparator();
		helpMenu.add(aboutItem);




	}



	/**
	 * Constructs the file menu for the given CenterJFrame.
	 */
	public void constructFileMenu() {

		JMenu statusMenu, keyTypeMenu, loadMenu;
		JMenuItem newItem, clearItem, allStatusItem, treeStatusItem, colorPreferencesItem, exitItem;
         
        //JCheckBoxMenuItem cbMenuItem;
		ButtonGroup keyTypeGroup;

		//Build the file menu.
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.getAccessibleContext().setAccessibleDescription("File Menu");


		//Build the new item.
		newItem = new JMenuItem("New", KeyEvent.VK_N);
		newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		newItem.getAccessibleContext().setAccessibleDescription("New Option");
		newItem.addActionListener(this);
		newItem.setActionCommand(OptionEvent.CLEAR_ALL);

        //Build the load item.
        /*loadMenu = new JMenu("Load");
		loadMenu.getAccessibleContext().setAccessibleDescription("Load from List");

        //Build loadable items.
        for (int i = 0; i < loadKeyList.length; i++) 
        {
            JMenuItem loadItem = new JMenuItem(loadKeyList[i]);
            loadItem.addActionListener(this);
            loadItem.setActionCommand(OptionEvent.getLoadCommand(i));
            loadMenu.add(loadItem);
        }*/
        
        //Build the clear item.
		clearItem = new JMenuItem("Clear Tree", KeyEvent.VK_C);
		clearItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
		clearItem.getAccessibleContext().setAccessibleDescription("Clear Tree");
		clearItem.addActionListener(this);
		clearItem.setActionCommand(OptionEvent.CLEAR);

		//Build the status menu.
		statusMenu = new JMenu("Status");
		statusMenu.setMnemonic(KeyEvent.VK_S);
		statusMenu.getAccessibleContext().setAccessibleDescription("Status Menu");

		//Build the all status item.
		allStatusItem = new JMenuItem("All Tree Status", KeyEvent.VK_A);
		allStatusItem.getAccessibleContext().setAccessibleDescription("All Tree Status");
		allStatusItem.addActionListener(this);
		allStatusItem.setActionCommand(OptionEvent.GET_STATUS_ALL);

		//Build the all status item.
		treeStatusItem = new JMenuItem("Current Tree Status", KeyEvent.VK_C);
		treeStatusItem.getAccessibleContext().setAccessibleDescription("Current Tree Status");
		treeStatusItem.addActionListener(this);
		treeStatusItem.setActionCommand(OptionEvent.GET_STATUS);

		statusMenu.add(allStatusItem);
		statusMenu.add(treeStatusItem);

		//Build the keyType menu.
		keyTypeMenu = new JMenu("Key Type");
		keyTypeMenu.setMnemonic(KeyEvent.VK_K);
		keyTypeMenu.getAccessibleContext().setAccessibleDescription("Key Type Menu");

		keyTypeGroup = new ButtonGroup();

		integerTypeRadioItem = new JRadioButtonMenuItem("Integer");
		integerTypeRadioItem.setSelected(true);
		integerTypeRadioItem.setMnemonic(KeyEvent.VK_I);
		integerTypeRadioItem.addActionListener(this);
		integerTypeRadioItem.setActionCommand(OptionEvent.INTEGER);

		characterTypeRadioItem = new JRadioButtonMenuItem("Character");
		characterTypeRadioItem.setMnemonic(KeyEvent.VK_C);
		characterTypeRadioItem.addActionListener(this);
		characterTypeRadioItem.setActionCommand(OptionEvent.CHARACTER);

		doubleTypeRadioItem = new JRadioButtonMenuItem("Double");
		doubleTypeRadioItem.setMnemonic(KeyEvent.VK_D);
		doubleTypeRadioItem.addActionListener(this);
		doubleTypeRadioItem.setActionCommand(OptionEvent.DOUBLE);


		keyTypeGroup.add(integerTypeRadioItem);
		keyTypeGroup.add(characterTypeRadioItem);
		keyTypeGroup.add(doubleTypeRadioItem);

		keyTypeMenu.add(integerTypeRadioItem);
		keyTypeMenu.add(characterTypeRadioItem);
		keyTypeMenu.add(doubleTypeRadioItem);

		//Build the clear item.
		colorPreferencesItem = new JMenuItem("Color Preferences", KeyEvent.VK_P);
		colorPreferencesItem.getAccessibleContext().setAccessibleDescription("Color Preferences");
		colorPreferencesItem.addActionListener(this);
		colorPreferencesItem.setActionCommand(OptionEvent.COLOR_OPTIONS);

		//Build the clear item.
		exitItem = new JMenuItem("Exit", KeyEvent.VK_X);
		exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK));
		exitItem.getAccessibleContext().setAccessibleDescription("Exit");
		exitItem.addActionListener(this);
		exitItem.setActionCommand(OptionEvent.EXIT);


		fileMenu.add(newItem);
		fileMenu.add(clearItem);
        //fileMenu.add(loadMenu);
		fileMenu.addSeparator();
		fileMenu.add(statusMenu);
		fileMenu.add(keyTypeMenu);
		//fileMenu.addSeparator();
		//fileMenu.add(colorPreferencesItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);

	}


	/**
	 * Constructs the view menu for the given CenterJFrame.
	 */
	public void constructViewMenu() {

		JMenu messagePanelMenu, displayTypeMenu, viewTypeMenu;

		JRadioButtonMenuItem messagePanelMenuAllItem, messagePanelMenuFinalOnlyItem, messagePanelMenuOffItem;
		JRadioButtonMenuItem displayMenuBinaryItem, displayMenuSectionalItem;
		JRadioButtonMenuItem viewTypeMenuWindowsItem, viewTypeMenuTabbedItem;

		//JCheckBoxMenuItem cbMenuItem;
		ButtonGroup messagePanelGroup, displayTypeGroup, viewTypeGroup;


		//Build the view menu.
		viewMenu = new JMenu("View");
		viewMenu.setMnemonic(KeyEvent.VK_V);
		viewMenu.getAccessibleContext().setAccessibleDescription("View Menu");

        subtreeCountCheckItem = new JCheckBoxMenuItem("Integer Fields");
        subtreeCountCheckItem.setSelected(false);
        subtreeCountCheckItem.getAccessibleContext().setAccessibleDescription("Display Integer Fields");
        subtreeCountCheckItem.addItemListener(this);

		animationMenuCheckItem = new JCheckBoxMenuItem("Animation");
		animationMenuCheckItem.setMnemonic(KeyEvent.VK_A);
		animationMenuCheckItem.setSelected(true);
		animationMenuCheckItem.getAccessibleContext().setAccessibleDescription("Animation On");
		animationMenuCheckItem.addItemListener(this);

		toolBarPanelCheckItem = new JCheckBoxMenuItem("Tool Bar");
		toolBarPanelCheckItem.setMnemonic(KeyEvent.VK_T);
		toolBarPanelCheckItem.setSelected(true);
		toolBarPanelCheckItem.getAccessibleContext().setAccessibleDescription("Tool Bar");
		toolBarPanelCheckItem.addItemListener(this);

		//Build the message menu.
		messagePanelMenu = new JMenu("Message");
		messagePanelMenu.setMnemonic(KeyEvent.VK_M);
		messagePanelMenu.getAccessibleContext().setAccessibleDescription("Message All/Final/Off");

		messagePanelGroup = new ButtonGroup();

		messagePanelMenuAllItem = new JRadioButtonMenuItem("All Messages");
		messagePanelMenuAllItem.setSelected(true);
		messagePanelMenuAllItem.getAccessibleContext().setAccessibleDescription("All Messages");
		messagePanelMenuAllItem.addActionListener(this);
		messagePanelMenuAllItem.setActionCommand(OptionEvent.MESSAGE_ALL);

		messagePanelMenuFinalOnlyItem = new JRadioButtonMenuItem("Final Messages Only");
		messagePanelMenuFinalOnlyItem.getAccessibleContext().setAccessibleDescription("Final Messages Only");
		messagePanelMenuFinalOnlyItem.addActionListener(this);
		messagePanelMenuFinalOnlyItem.setActionCommand(OptionEvent.MESSAGE_FINAL_ONLY);

		messagePanelMenuOffItem = new JRadioButtonMenuItem("Messages Off");
		messagePanelMenuOffItem.getAccessibleContext().setAccessibleDescription("Messages Off");
		messagePanelMenuOffItem.addActionListener(this);
		messagePanelMenuOffItem.setActionCommand(OptionEvent.MESSAGE_OFF);


		messagePanelGroup.add(messagePanelMenuAllItem);
		messagePanelGroup.add(messagePanelMenuFinalOnlyItem);
		messagePanelGroup.add(messagePanelMenuOffItem);

		messagePanelMenu.add(messagePanelMenuAllItem);
		messagePanelMenu.add(messagePanelMenuFinalOnlyItem);
		messagePanelMenu.add(messagePanelMenuOffItem);



		nodePanelMenuCheckItem = new JCheckBoxMenuItem("Node Panel");
		nodePanelMenuCheckItem.setMnemonic(KeyEvent.VK_N);
		nodePanelMenuCheckItem.setSelected(true);
		nodePanelMenuCheckItem.getAccessibleContext().setAccessibleDescription("Node Panel");
		nodePanelMenuCheckItem.addItemListener(this);

		//Build the display type menu.
		displayTypeMenu = new JMenu("Display Type");
		displayTypeMenu.setMnemonic(KeyEvent.VK_M);
		displayTypeMenu.getAccessibleContext().setAccessibleDescription("Display Type");

		displayTypeGroup = new ButtonGroup();

		displayMenuBinaryItem = new JRadioButtonMenuItem("Binary");
		displayMenuBinaryItem.setSelected(true);
		displayMenuBinaryItem.getAccessibleContext().setAccessibleDescription("Binary Display");
		displayMenuBinaryItem.addActionListener(this);
		displayMenuBinaryItem.setActionCommand(OptionEvent.BINARY_DISPLAY);

		displayMenuSectionalItem = new JRadioButtonMenuItem("Sectional");
		displayMenuSectionalItem.getAccessibleContext().setAccessibleDescription("Sectional Display");
		displayMenuSectionalItem.addActionListener(this);
		displayMenuSectionalItem.setActionCommand(OptionEvent.SECTIONAL_DISPLAY);


		displayTypeGroup.add(displayMenuBinaryItem);
		displayTypeGroup.add(displayMenuSectionalItem);

		displayTypeMenu.add(displayMenuBinaryItem);
		displayTypeMenu.add(displayMenuSectionalItem);



		//Build the animation menu.
		viewTypeMenu = new JMenu("Screen Type");
		viewTypeMenu.setMnemonic(KeyEvent.VK_V);
		viewTypeMenu.getAccessibleContext().setAccessibleDescription("View Type");

		viewTypeGroup = new ButtonGroup();

		viewTypeMenuTabbedItem = new JRadioButtonMenuItem("Tabbed Pane");
		viewTypeMenuTabbedItem.setSelected(true);
		viewTypeMenuTabbedItem.getAccessibleContext().setAccessibleDescription("Tabbed Pane Display");
		viewTypeMenuTabbedItem.addActionListener(this);
		viewTypeMenuTabbedItem.setActionCommand(OptionEvent.TABBED_PANE);

		viewTypeMenuWindowsItem = new JRadioButtonMenuItem("Windows");
		viewTypeMenuWindowsItem.getAccessibleContext().setAccessibleDescription("Window Display");
		viewTypeMenuWindowsItem.addActionListener(this);
		viewTypeMenuWindowsItem.setActionCommand(OptionEvent.WINDOWS);


		viewTypeGroup.add(viewTypeMenuTabbedItem);
		viewTypeGroup.add(viewTypeMenuWindowsItem);

		viewTypeMenu.add(viewTypeMenuTabbedItem);
		viewTypeMenu.add(viewTypeMenuWindowsItem);



		viewMenu.add(subtreeCountCheckItem);
		viewMenu.add(animationMenuCheckItem);
		viewMenu.add(toolBarPanelCheckItem);
		viewMenu.add(nodePanelMenuCheckItem);
		viewMenu.addSeparator();
		viewMenu.add(messagePanelMenu);
		viewMenu.addSeparator();
		viewMenu.add(displayTypeMenu);
		viewMenu.add(viewTypeMenu);


	}


	/**
	 * Constructs the menu for the given CenterJFrame. This menu calls upon the optionPane to give it commands.
	 */
	public void constructMenu() {

		//Create the menu bar.
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);


		constructFileMenu();

		constructViewMenu();

		constructToolMenu();

		constructHelpMenu();

		menuBar.add(fileMenu);
		menuBar.add(viewMenu);
		menuBar.add(toolMenu);
		menuBar.add(helpMenu);



    }


	/**
	 * Implements ItemListener.
	 */
	public void itemStateChanged(ItemEvent e) {
        if (e.getItem() == subtreeCountCheckItem) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                optionAction(OptionEvent.INTEGER_FIELDS_ON);
            }
            else {
                optionAction(OptionEvent.INTEGER_FIELDS_OFF);
            }
        }
		if (e.getItem() == animationMenuCheckItem) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				optionPane.setAnimationPanelShow(true);
				optionPane.redrawControlPanel();
				optionAction(OptionEvent.ANIMATION_ON);
			}
			else {
				optionPane.setAnimationPanelShow(false);
				optionPane.redrawControlPanel();
				optionAction(OptionEvent.ANIMATION_OFF);
			}
		}
		if (e.getItem() == toolBarPanelCheckItem) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				getContentPane().add(optionPane, BorderLayout.NORTH);
				getContentPane().validate();
				getContentPane().repaint();
			}
			else {
				getContentPane().remove(optionPane);
				getContentPane().validate();
				getContentPane().repaint();
			}
		}
		if (e.getItem() == nodePanelMenuCheckItem) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				optionPane.setDisplayPanelShow(true);
				optionPane.redrawControlPanel();
			}
			else {
				optionPane.setDisplayPanelShow(false);
				optionPane.redrawControlPanel();
			}
		}

 	}


	/**
	 * If an action is pereformed through one of the buttons.
	 *
	 * @param e ActionEvent that contains information about the action performed.
	 */
	public void actionPerformed(ActionEvent e) {
		// Command of action
		String command = e.getActionCommand();

		if (command.equals(OptionEvent.EXIT)) {
			System.exit(0);
		}
		else if (command.equals(OptionEvent.INTEGER) || command.equals(OptionEvent.DOUBLE) || command.equals(OptionEvent.CHARACTER) ) {
			optionAction(OptionEvent.INPUT_CHANGE_ALL, command);
		}
		else if (command.equals(OptionEvent.MESSAGE_OFF)) {
			optionPane.setMessagePanelShow(false);
			optionPane.redrawControlPanel();
			optionAction(command, null, optionPane);
		}
		else if (command.equals(OptionEvent.MESSAGE_FINAL_ONLY) || command.equals(OptionEvent.MESSAGE_ALL)) {
			optionPane.setMessagePanelShow(true);
			optionPane.redrawControlPanel();
			optionAction(command, null, optionPane);
		}
		else if (command.equals(OptionEvent.ANIMATION_OFF)) {
			optionPane.setAnimationPanelShow(false);
			optionPane.redrawControlPanel();
			optionAction(command);
		}
		else if (command.equals(OptionEvent.ANIMATION_ON)) {
			optionPane.setAnimationPanelShow(true);
			optionPane.redrawControlPanel();
			optionAction(command);
		}
		else if (command.equals(OptionEvent.NODE_DISPLAY_OFF)) {
			optionPane.setDisplayPanelShow(false);
			optionPane.redrawControlPanel();
		}
		else if (command.equals(OptionEvent.NODE_DISPLAY_ON)) {
			optionPane.setDisplayPanelShow(true);
			optionPane.redrawControlPanel();
		}
		else if (command.equals(OptionEvent.TOOL_BAR_OFF)) {
			getContentPane().remove(optionPane);
			getContentPane().validate();
			getContentPane().repaint();
		}
		else if (command.equals(OptionEvent.TOOL_BAR_ON)) {
			getContentPane().add(optionPane, BorderLayout.NORTH);
			getContentPane().validate();
			getContentPane().repaint();
		}
		else if (command.equals(OptionEvent.COLOR_OPTIONS)) {
			optionAction(command, this);
		}
		else {
			optionAction(command);
		}
	}


	/////////////////////////////////
	// Implements TreeMessageListener
	/////////////////////////////////

 	/**
	 * Invoked when an Tree Message action occurs.
	 */
	public void treeMessageEventPerformed(TreeMessageEvent e) {
		if (e.getMessage().equals(TreeMessageEvent.KEY_TYPE_CHANGE)) {
			if (e.getMessageObject().equals(OptionEvent.INTEGER)) {
				integerTypeRadioItem.setSelected(true);
			}
			else if (e.getMessageObject().equals(OptionEvent.CHARACTER)) {
				characterTypeRadioItem.setSelected(true);
			}
			else if (e.getMessageObject().equals(OptionEvent.DOUBLE)) {
				doubleTypeRadioItem.setSelected(true);
			}
		}
	}


	/**
	 * Gets the icon from the resource (JAR file).
	 */
	public Image getImage(String imageName) {

   		// Get current classloader
   		URL url = this.getClass().getResource(imageName);


   		Image img=Toolkit.getDefaultToolkit().getImage(url);

		return img;

	}


	///////////////////////////////
	// Same methods as OptionJPanel
	///////////////////////////////

	/**
	 * Adds the OptionListener as listening to this current panel. It simply adds to listener to
	 * the list of listeners.
	 *
	 * @param optionListener the listener added to listen to the options
	 */
    public void addOptionListener(OptionListener optionListener) {
		listeners.add(optionListener);
	}

	/**
	 * Removes the OptionListener as listening to this current panel. It simply removes to listener to
	 * the list of listeners.
	 *
	 * @param optionListener the listener removed to listen to the options
	 */
    public void removeOptionListener(OptionListener optionListener) {
		listeners.remove(optionListener);
	}



	/**
	 * Passes an optionEvent. If the optionListener parameter is null, the method sends the optionEvent to all listeners. Otherwise, it only
	 * sends the message to the specified optionListener.
	 *
	 * @param optionEvent the event passed on.
	 * @param optionListener the specific listener to the event or null if all listeners should be
	 * called.
	 */
	public void optionAction(OptionEvent optionEvent, OptionListener optionListener) {

		// If listener is null.
		if (optionListener == null) {
			// Goes through entire list.
			ListIterator list = listeners.listIterator(0);

			while (list.hasNext()) {
				((OptionListener)list.next()).optionEventPerformed(optionEvent);
			}
		}
		else {
			optionListener.optionEventPerformed(optionEvent);
		}
	}

	/**
	 * Passes an optionEvent.
	 *
	 * @param optionEvent the event passed on.
	 */
	public void optionAction(OptionEvent optionEvent) {
		optionAction(optionEvent, null);
	}


	/**
	 * Creates an optionEvent based upon the id, event type, and object. If the optionListener
	 * parameter is null, the method sends the optionEvent to all listeners. Otherwise, it only
	 * sends the message to the specified optionListener.
	 *
	 * @param evt the String event passed in the optionEvent.
	 * @param object the object accompanying the String event type.
	 * @param optionListener the specific listener to the event or null if all listeners should be
	 * called.
	 */
	public void optionAction(String evt, Object object, OptionListener optionListener) {
		OptionEvent optionEvent = new OptionEvent(this, id, evt, object);

		optionAction(optionEvent, optionListener);

	}


	/**
	 * Creates an optionEvent based upon the id, event type, and object. All listeners are called.
	 *
	 * @param evt the String event passed in the optionEvent.
	 * @param object the object accompanying the String event type.
	 */
	public void optionAction(String evt, Object object) {
		optionAction(evt, object, null);
	}

	/**
	 * Creates an optionEvent based upon the id and event type. All listeners are called.
	 *
	 * @param evt the String event passed in the optionEvent.
	 */
	public void optionAction(String evt) {
		optionAction(evt, null, null);
	}

}
