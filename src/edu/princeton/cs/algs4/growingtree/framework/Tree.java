package edu.princeton.cs.algs4.growingtree.framework;
/*
 * @(#)Tree.java
 *
 * Last Modified: 9/15/01
 */

import java.awt.*;
import java.util.*;


/**
 * A rooted Tree, where every node is pointed to by just one other node, which is called its parent.
 * <p>A Tree represents a group of objects, known as its elements, each with its corresponding key.
 * The key maps the value of each node and is used as a comparator. The Tree collection is similiar to a Map,
 * except that duplicate keys are allowed. <p>
 *
 *
 * @author Corey Sanders
 * @version 1.1 9/15/01
 */
public interface Tree<P extends NodeProperties>  {

	/**
	 * Returns the number of objects in the current Tree.
	 *
	 * @return the number of objects in the current Tree.
	 */
	public int size();

	/**
	 * Returns true if the current <code>Tree</code> is empty.
	 *
	 * @return true if the <code>Tree</code> is empty.
	 */
	public boolean isEmpty();

	/**
	 * Returns the value of the current <code>Tree</code>.
	 *
	 * @return the value of the current <code>Tree</code>.
	 */
	public Object getValue();

	/**
	 * Returns the key of the current <code>Tree</code>.
	 *
	 * @return the key of the current <code>Tree</code>.
	 */
	public KeyType getKey();


	/**
     * Gets the the level of the <code>Tree</code>.
     * The level is the integer count of how far down in the tree the current node is.
     *
     * @return integer count of the level of the current <code>Tree</code>.
     */
	public int getLevel();


	/**
     * Returns the parent of the current <code>Tree</code>.
	 *
	 * @return <code>Tree</code> that is the parent and null if the tree is the head.
	 */
	public Tree<P> getParentTree();

	/**
     * Returns the children of the current <code>Tree</code>.
	 *
	 * @return <code>Tree</code> array that is the children and null if the tree is an exterior node.
	 */
	public Tree[] getChildren();


}
